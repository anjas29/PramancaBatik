<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaporanBarang extends Model
{
    protected $table = 'laporan_barang';

    public function barang(){
    	return $this->belongsTo('AppBarang');
    }
}
