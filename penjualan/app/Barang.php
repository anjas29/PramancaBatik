<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barang';

    protected $fillable = array('nama', 'kategori_id', 'harga', 'stok');

    public function detail_pesanan(){
    	return $this->hasMany('App\Detail_Pesanan','barang_id');
    }

    public function kategori(){
    	return $this->belongsTo('App\Kategori','kategori_id');
    }
}
