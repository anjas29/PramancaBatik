<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    protected $table = 'pesanan';

    protected $fillable = array('pelanggan_id', 'kode_pesanan', 'customer_id', 'potongan_harga', 'total_pembayaran', 'tanggal_pesanan', 'verifikasi');
    
    public $timestamps = false;

    public function beban(){
    	return $this->hasOne('App\Beban');
    }

    public function detail_pesanan(){
    	return $this->hasMany('App\Detail_Pesanan');
    }

    public function pembayaran(){
        return $this->hasOne('App\Pembayaran');
    }
    
    public function getTotalBayarAttribute(){
        return $this->attributes['total_pembayaran'] + $this->hasOne('App\Beban')->first()->harga_beban;
    }

    public function pelanggan(){
    	return $this->belongsTo('App\Pelanggan');
    }
    
    public function customer(){
        return $this->belongsTo('App\CustomerService');
    }
}
