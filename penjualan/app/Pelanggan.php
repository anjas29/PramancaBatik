<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Pelanggan extends Authenticatable
{
    protected $table = 'pelanggan';

    protected $fillable = array('alamat', 'no_telepon', 'user_id');

    public function user(){
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function pesanan(){
    	return $this->hasMany('App\Pesanan', 'pesanan_id');
    }
}
