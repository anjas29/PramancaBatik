<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_Pesanan extends Model
{
    protected $table = 'detail_pesanan';

    protected $fillable = array('pesanan_id', 'detail_barang_id', 'jumlah');

    public $timestamps = false;

    public function barang(){
    	return $this->belongsTo('App\Barang', 'barang_id');
    }

    public function pesanan(){
    	return $this->belongsTo('App\Pesanan', 'pesanan_id');
    }
}
