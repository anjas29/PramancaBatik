<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_Pembayaran extends Model
{
    protected $table = 'detail_pembayaran';

    protected $fillable = array('pembayaran_id', 'pembayaran', 'keterangan', 'tanggal_pembayaran');

    public $timestamps = false;

    public function pembayaran_detail(){
    	return $this->belongsTo('App\Pembayaran', 'pembayaran_id');
    }
}
