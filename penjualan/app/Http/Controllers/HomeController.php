<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\User;
use App\Pelanggan;
use App\Barang;
use App\Kategori;
use App\Pesanan;
use App\Detail_Pesanan;
use App\Pembayaran;
use App\Detail_Pembayaran;
use App\Beban;

use Carbon\Carbon;
use Auth;
use Input;
use Hash;
use Cart;
use Session;
use Validator;
use DB;

class HomeController extends Controller
{
    

    public function __construct()
    {
        $sites = array('getRegisterPelanggan', 
                        'registerPelanggan',
                        'getIndex', 
                        'getCategory',
                        'category',
                        'postLogin',
                        'specificCategory',
                        'getCart', 'postAddCart','postDeleteItemCart', 'postUpdateCart',
                        'getLoginCashier', 'postLoginCashier', 'getGuest', 'postGuest',
                        'postVerifPesanan', 'postVerifikasi',
                        'getPayment1','getPayment2','getPayment3',
                        'getCheckPesanan', 'postCheckPesanan',
                        'postPayment1', 'postPayment2', 'postPayment3', 'getPaymentSuccess',
                        'getLogout',
                        'detailBarang');

        $this->middleware('auth:pelanggan', ['except' => $sites]);
    }

    
    public function index(){
        return view('home');
    }

    public function postLogin(){
        $auth = auth('pelanggan');

        $credential = [
            'email' => Input::get('email'),
            'password' => Input::get('password')
        ];

        if($auth->attempt($credential)){
            return redirect('/');
        }else{
            Session::flash('loginFailed', 'Login Gagal!');
            return redirect('/login');
        }
    }

    public function getLogout(){
        auth('pelanggan')->logout();
        Cart::destroy();
        return redirect('/');
    }

    public function getIndex(){
        $category = Kategori::all();

        $latestItem = Barang::limit(9)->orderBy('created_at','DESC')->get();

        $data = Barang::limit(20)->get();


        return view('edited')->withCategory($category)
                             ->withLatest($latestItem)
                             ->withData($data);
    }

    public function getCategory($one){
        $data = Barang::all();

        return view('category')->withData($data);
    }

    public function category(){
        $category_name = "Semua";
        $count = Barang::count();
        $limit = 12;
        $kategori = Kategori::all();

        if(!is_null(Input::get('limit'))){
            $limit = Input::get('limit');
        }

        if(Input::get('sort') == 'nama'){
            $data = Barang::orderBy('nama', 'ASC')->paginate($limit);
        }else if(Input::get('sort') == 'harga'){
            $data = Barang::orderBy('harga', 'ASC')->paginate($limit);
        }else{
            $data = Barang::orderBy('created_at', 'DSC')->paginate($limit);
        }
        
        if(!is_null(Input::get('limit'))){
            $data->appends(['limit'=>$limit])->links();
        }

        return view('category')
                    ->withData($data)
                    ->withCategory($kategori)
                    ->withSelect($category_name)
                    ->withCount($count);
    }

    public function specificCategory($category){
        $limit = 12;
        if(!is_null(Input::get('limit'))){
            $limit = Input::get('limit');
        }

        $category_name = Kategori::where('nama', $category)->firstOrFail()->nama;
        $kategori = Kategori::all();


        $data = Barang::whereHas('kategori', function($q) use ($category)
                {
                    $q->where('nama', '=', $category);
                });


        $count = $data->count();
        $data = $data->paginate($limit);
        
        if(!is_null(Input::get('limit'))){
            $data->appends(['limit'=>$limit])->links();
        }

        return view('category')
                    ->withData($data)
                    ->withCategory($kategori)
                    ->withSelect($category_name)
                    ->withCount($count);
    }

    public function detailBarang($id){
        $kategori = Kategori::all();
        $category_name = "Semua";
        $data = Barang::findOrFail($id);
        $category_name = $data->kategori->nama;

        return view('detail')
                    ->withData($data)
                    ->withCategory($kategori)
                    ->withSelect($category_name);
    }

    public function getRegisterPelanggan(){
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: b91f866b918a08f9bebe3dc201dc92ed"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = null;
        if ($err) {
          return abort('404');
        } else {
          $result = json_decode($response);
        }
        $data = $result->rajaongkir->results;
        
        return view('register')->withData($data);
    }
    public function registerPelanggan(){
        $emailIsExist = Pelanggan::where('email', '=', Input::get('email'))->exists();

        if($emailIsExist){
            return redirect('/register');
        }else{
            $kota = explode('-',Input::get('kota'));

            $user = new Pelanggan;
            $user->nama = Input::get('nama');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            $user->alamat = Input::get('alamat');
            $user->kota = $kota[1];
            $user->kota_id = $kota[0];
            $user->kode_pos = Input::get('kode_pos');
            $user->tempat_lahir = Input::get('tempat_lahir');
            $user->tanggal_lahir = Input::get('tanggal_lahir');
            $user->no_telepon = Input::get('telepon');
            $user->save();
        }

        return view('loginSuccess');
    }

    public function getCart(){
        
        $cart = Cart::content();
        $data = array(
                'count' => Cart::count(),
                'total' => Cart::total(),
                'details' => $cart
            );

        return view('cart')->withData($data);
    }

    public function postAddCart(){
        $id = Input::get('id');
        $qty = Input::get('jumlah');

        $barang = Barang::findOrFail($id);
        $harga = $barang->harga;

        if($qty <= 0){
            Session::flash('messageError','Barang tidak valid');
            return redirect('/cart');
        }

        if($qty >= 10)
            $harga = $barang->harga_grosir;

        Cart::add(array(
                'id' => $id,
                'name' => $barang->nama,
                'qty' => $qty,
                'price' => $harga,
                'options'=> array('foto' => $barang->foto, 'berat' => $barang->berat)
            ));
        Session::flash('message', 'Barang berhasil ditambahkan');
        return redirect('/cart');
    }

    public function postDeleteItemCart(){
        $rowId = Input::get('rowid');

        Cart::remove($rowId);
        Session::flash('message', 'Barang berhasil dihapus');
    }

    public function postUpdateCart(){
        if(!isset($rowid) || !isset($qty)){
            Session::flash('messageError','Barang tidak valid');
            return redirect('/cart');
        }
        $rowid = Input::get('rowid');
        $qty = Input::get('qty');

        foreach ($rowid as $i => $r) {
            if($qty[$i] <= 0){
                Session::flash('messageError', 'Minimal pembelian barang 1 item');
                return redirect('/cart');        
            }
            Cart::update($r, ['qty' => $qty[$i]]);
        }
        Session::flash('message', 'Jumlah barang berhasil diperbarui');
        return redirect('/cart');
    }

    public function getLoginCashier(){

        return view('loginCashier');
    }

    public function postLoginCashier(){
        $auth = auth('pelanggan');

        $credential = [
            'email' => Input::get('email'),
            'password' => Input::get('password')
        ];

        if($auth->attempt($credential)){
            return redirect('/payment/step1');
        }else{  
            return redirect('/loginCashier');
        }
    }

    public function getGuest(){
        return view('dataTamu');
    }

    public function postGuest(){
        Session::put('temp_nama', Input::get('nama'));
        Session::put('temp_no_telepon', Input::get('no_telepon'));
        Session::put('guest', 'Success');
        return redirect('/payment/step1');
    }

    public function getPayment1(){
        if(!auth('pelanggan')->check() && !Session::has('guest')){
            return redirect('/loginCashier');
        }
        if(Cart::count()>0){
            $kota_id = null;
            $alamat = null;
            $kode_pos = null;
            if(auth('pelanggan')->check()){
                $user = auth('pelanggan')->user();    
                $kota_id = $user->kota_id;
                $alamat = $user->alamat;
                $kode_pos = $user->kode_pos;
                Session::put('temp_nama', $user->nama);
                Session::put('temp_no_telepon', $user->no_telepon);
            }            

            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                "key: b91f866b918a08f9bebe3dc201dc92ed"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $kota = null;
            if ($err) {
              return abort('500');
            } else {
              $kota = json_decode($response);
              $kota = $kota->rajaongkir->results;
            }

            $data = array(
                        'kota_id'=>$kota_id,
                        'alamat'=>$alamat,
                        'kode_pos'=>$kode_pos,
                        'beban' => 0,
                        'kota' => $kota
                    );

            return view('payment1')->withData($data);
        }else{
            Session::flash('messageError','Barang tidak valid');
            return redirect('/cart');
        }
        
    }
    public function postPayment1(){
        if(Cart::count()>0){
            $kota = explode('-',Input::get('kota'));
            $curl = curl_init();
            $origin = 501;
            $destination = $kota[0];
            $weight = 0;

            foreach (Cart::content() as $c) {
                $weight = $weight + ($c->qty * $c->options->berat);
            }

            if($weight < 1000){
                $weight = 1000;
            }else if(($weight % 1000) < 300){
                $weight = floor($weight/1000)*1000;
            }else{
                $weight = ceil($weight/1000)*1000;
            }

            curl_setopt_array($curl, array(
              CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => 'origin='.$origin.'&destination='.$destination.'&weight='.$weight.'&courier=jne',
              CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: b91f866b918a08f9bebe3dc201dc92ed"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $result = null;

            if ($err) {
              return redirect('/payment/step1');
            } else {
              $result = json_decode($response, true);
            }

            $beban = 300000;

            if(count($result['rajaongkir']['results'][0]['costs']) != 0){
                $beban = $result['rajaongkir']['results'][0]['costs'][1]['cost'][0]['value'];
            }
            $kode_unik = sprintf('%03d', rand(1,999));

            Session::put('temp_kota', $kota[1]);
            Session::put('temp_kota_id', $kota[0]);
            Session::put('temp_alamat', Input::get('alamat'));
            Session::put('temp_kode_pos', Input::get('kode_pos'));
            Session::put('temp_kode_unik', $kode_unik);
            Session::put('temp_beban', $beban);
            Session::put('payment1', 'Success');
            
            return redirect('/payment/step2');
        }else{
            Session::flash('messageError', 'Barang tidak valid');
            return redirect('/cart');
        }
    }

    public function getPayment2(){
        if(Session::has('payment1')){

            return view('payment2');
        }else if(Cart::count()>0){
            return redirect('/payment/step1');
        }else{
            Session::flash('messageError', 'Barang tidak valid');
            return redirect('/cart');
        }
        
    }

    public function postPayment2(){
        if(Session::has('payment1')){
            if(Input::get('metode_pembayaran') == 'kredit'){
                Session::put('metode_pembayaran', 'kredit');
            }else{
                Session::put('metode_pembayaran', 'tunai');
            }

            Session::put('payment2', 'Success');

            return redirect('/payment/step3');
        }else if(Cart::count()>0){
            return redirect('/payment/step1');
        }else{
            Session::flash('messageError', 'Barang tidak valid');
            return redirect('/cart');
        }
    }

    public function getPayment3(){
        if(Session::has('payment2')){
            $cart = Cart::content();
            $data = array(
                    'count' => Cart::count(),
                    'total' => Cart::total(),
                    'details' => $cart,
                    'tax' => Session::get('temp_beban'),
                    'beban' => Session::get('temp_beban'),
                    'kode_unik' => Session::get('temp_kode_unik'),
                    'user'=> auth('pelanggan')->user()
                );

            return view('payment3')->withData($data);    
        }else{
            Session::flash('messageError', 'Pembelian tidak valid');
            return redirect('/cart');
        }
    }

    public function postPayment3(){
        if(Session::has('payment2')){
            $user = auth('pelanggan')->user();
            $total = Cart::total();

            $pesanan = new Pesanan;
            $pesanan->nama_pelanggan = Session::get('temp_nama');
            $pesanan->no_telepon = Session::get('temp_no_telepon');
            if(auth('pelanggan')->check()){
                $pesanan->pelanggan_id = auth('pelanggan')->user()->id;
                $pesanan->kode_pesanan = 'PO' . sprintf('%010d',sprintf("%03d", auth('pelanggan')->user()->id) .Carbon::now()->format('dm') . sprintf("%03d", rand(1,999)));
            }else{
                $pesanan->pelanggan_id = null;
                $pesanan->kode_pesanan = 'PO' . sprintf('%010d',sprintf("%03d", 0) .Carbon::now()->format('dm') . sprintf("%03d", rand(1,999)));
            }
            
            $pesanan->potongan_harga = 0;
            $pesanan->total_pembayaran = $total;
            $pesanan->tanggal_pesanan = Carbon::now()->format('Y-m-d H:i:s');
            $pesanan->kota = Session::get('temp_kota');
            $pesanan->kota_id = Session::get('temp_kota_id');
            $pesanan->kode_pos = Session::get('temp_kode_pos');
            $pesanan->alamat = Session::get('temp_alamat');
            $pesanan->verifikasi = 'Konfirmasi Pembayaran';

            if(Session::get('metode_pembayaran') == 'tunai'){
                $pesanan->jenis_transaksi = 'TON';
            }else{
                $pesanan->jenis_transaksi = 'KON';
            }

            $code = null;

            do{
                $code = str_random(15).str_random(3).str_random(3). Carbon::now()->format('m').str_random(5) . Carbon::now()->format('d');
            }while(Pesanan::where('kode_pesanan_2', '=', $code)->exists());

            $pesanan->kode_pesanan_2 = $code;
            $pesanan->save();
            Session::put('temp_kode', $code);
            $berat = 0;
            foreach (Cart::content() as $item) {
                $barang = Barang::where('id', $item->id)->first();
                $barang->stok = $barang->stok - $item->qty;
                $barang->save();
                
                $detail_pesanan = new Detail_Pesanan;
                $detail_pesanan->pesanan_id = $pesanan->id;
                $detail_pesanan->barang_id = $item->id;
                $detail_pesanan->jumlah = $item->qty;
                $detail_pesanan->harga = $item->price;
                $detail_pesanan->save();
                $berat = $berat + $item->options->berat;
            }

            if($berat < 1000){
                $berat = 1000;
            }else if(($berat % 1000) < 300){
                $berat = floor($berat/1000)*1000;
            }else{
                $berat = ceil($berat/1000)*1000;
            }

            $beban = new Beban;
            $beban->pesanan_id = $pesanan->id;
            $beban->harga_beban = Session::get('temp_beban')+Session::get('temp_kode_unik');
            $beban->berat = $berat;
            $beban->jumlah_barang = Cart::count();
            $beban->save();

            $pembayaran = new Pembayaran;
            $pembayaran->kode_pembayaran = 'P' . sprintf('%010d',sprintf("%03d", 0) .Carbon::now()->format('dm') . sprintf("%031d", rand(1,999)));
            $pembayaran->pesanan_id = $pesanan->id;
            $pembayaran->metode_pembayaran = Session::get('metode_pembayaran');
            $pembayaran->sisa_pembayaran = $pesanan->total_pembayaran + $beban->harga_beban;

            if(Session::get('metode_pembayaran') == 'kredit'){
                $pembayaran->status_pembayaran = 'Kredit Belum Lunas';
                $pembayaran->uang_muka = 0;
                $pembayaran->cicilan_perbulan = $pesanan->total_pembayaran/2;
            }else{
                $pembayaran->uang_muka = 0;
                $pembayaran->cicilan_perbulan = 0;
                $pembayaran->status_pembayaran = 'Tunai Belum Lunas';
            }

            $pembayaran->save();
            Cart::destroy();
            Session::forget('temp_beban');
            Session::forget('temp_alamat');
            Session::forget('temp_kota');
            Session::forget('temp_kota_id');
            Session::forget('temp_kode_pos');
            Session::forget('payment1');
            Session::forget('payment2');
            Session::forget('temp_alamat');
            Session::forget('temp_no_telepon');
            Session::forget('guest');
            Session::forget('metode_pembayaran');
            Session::forget('temp_kode_unik');

            $total = $pesanan->total_pembayaran + $beban->harga_beban;
            Session::put('total',$total);

            Session::flash('message', 'Terima kasih, Proses pembelian selesai');

            return redirect('/payment/success');
        }else if(Session::has('payment1')){

            return redirect('/payment/step2');
        }else if(Cart::count()>0){

            return redirect('/payment/step1');
        }else{
            Session::flash('messageError', 'Barang belanja tidak valid');
            return redirect('/cart');
        }
    }

    public function getPaymentSuccess(){
        
        if(Session::has('total')){
            $total = Session::get('total');
            $kode = Session::get('temp_kode');
            Session::forget('temp_kode');
            Session::forget('total');
            return view('payment_method')->withTotal($total)->withKode($kode);
        }else{
            return redirect('order');
        }
    }

    public function postVerifPesanan(){

        $file = array('image' => Input::file('bukti_pembayaran'));
        $rules = array('image' => 'required');

        $validator = Validator::make($file, $rules);
        
        if($validator->fails()) {

                Session::flash('messageError', 'Upload Bukti Pembayaran Gagal');
                return redirect('/order');
        } else {
            if(Input::file('bukti_pembayaran')->isValid()){
                $user_id = '';
                if(auth('pelanggan')->check())
                    $user_id = auth('pelanggan')->user()->id;
                else
                    $user_id = sprintf('%02d', rand(1,99));
                
                $path = storage_path().'/app/buktiPembayaran/';
                $extension = Input::file('bukti_pembayaran')->getClientOriginalExtension();

                $fileName = $user_id . '_' .Carbon::now()->format('YmdHis').'.'.$extension;

                if(strcasecmp($extension, 'jpg') != 0 && strcasecmp($extension, 'jpeg') != 0 && strcasecmp($extension, 'png') != 0){
                    Session::flash('messageError', 'Gambar Profil Tidak Valid');
                    return redirect('/order');
                }

                Input::file('bukti_pembayaran')->move($path, $fileName);

                $id = Input::get('id');

                $pesanan = Pesanan::where('id', $id)->first();

                if($pesanan->verifikasi == 'Konfirmasi Pembayaran'){
                    $pembayaran = Pembayaran::with('pesanan.beban')->whereHas('pesanan', function($q) use ($id)
                                    {
                                        $q->where('id', '=', $id);
                                    })->first();

                     $detailPembayaran = new Detail_Pembayaran;
                     $detailPembayaran->pembayaran_id = $pembayaran->id;
                     if($pesanan->jenis_transaksi == 'TON'){
                        $detailPembayaran->pembayaran = $pembayaran->pesanan->total_pembayaran;
                        $detailPembayaran->keterangan = 'Tunai';
                     }else{
                        $detailPembayaran->pembayaran = $pembayaran->pesanan->total_pembayaran/2;
                        $detailPembayaran->keterangan = 'Uang Muka';
                     }
                     
                     $detailPembayaran->bukti_pembayaran = $fileName;
                     $detailPembayaran->tanggal_pembayaran = Carbon::now()->format('Y-m-d H:i:s');
                     $detailPembayaran->verifikasi = 0;
                     $detailPembayaran->save();
                     
                     $pembayaran->save();
                }else if($pesanan->verifikasi == 'Lunas Uang Muka'){
                    $pembayaran = Pembayaran::whereHas('pesanan', function($q) use ($id)
                                    {
                                        $q->where('id', '=', $id);
                                    })->first();

                     $detailPembayaran = new Detail_Pembayaran;
                     $detailPembayaran->pembayaran_id = $pembayaran->id;
                     $detailPembayaran->pembayaran = floor(($pembayaran->sisa_pembayaran - $pembayaran->pesanan->beban->harga_beban)/10)*10;
                     $detailPembayaran->keterangan = 'Pelunasan Kredit';
                     
                     $detailPembayaran->bukti_pembayaran = $fileName;
                     $detailPembayaran->tanggal_pembayaran = Carbon::now()->format('Y-m-d H:i:s');
                     $detailPembayaran->verifikasi = 0;
                     $detailPembayaran->save();
                     
                     $pembayaran->save();
                }else{
                    Session::flash('messageError', 'Upload Bukti Pembayaran Gagal');
                    return redirect('/order');
                }

                $pesanan->save();

                Session::flash('message', 'Upload Bukti Pembayaran Berhasil');
                return redirect('/order');

            }else{
                Session::flash('messageError', 'Upload Bukti Pembayaran Gagal');
                return redirect('/order');
            }
        }
    }

    public function getCheckPesanan(){
        return view('pesanan');
    }
    public function postCheckPesanan(){
        $data = Pesanan::with('detail_pesanan.barang')
                        ->with('beban')
                        ->with('pelanggan')
                        ->where('kode_pesanan_2', Input::get('kode_pesanan'))
                        ->first();
        if(is_null($data)){
            Session::flash('messageError', 'Pesanan tidak ditemukan!');
            return view('pesanan');
        }
        return view('pesanan')->withData($data)->withKode(Input::get('kode_pesanan'));
    }
    public function getOrders(){
        $user_id = auth('pelanggan')->user()->id;

        $data = Pesanan::with('pembayaran')->where('pelanggan_id', $user_id)
                    ->where(function($query){
                            $query->where('jenis_transaksi', 'TON')->orWhere('jenis_transaksi','KON');
                    })->orderBy('id', 'DESC')->get();

        return view('orders')->withData($data);
    }

    public function getDetailOrder($id){
        $user_id = auth('pelanggan')->user()->id;

        
        $data = Pesanan::with('detail_pesanan.barang')
                        ->with('beban')
                        ->with('pelanggan')
                        ->where('id', $id)
                        ->first();

        if(is_null($data) || $data->pelanggan_id != $user_id){
            return abort('404');
        }

        $checkData = Pesanan::where('id', $id)
                        ->with(array('pembayaran.detail_pembayaran'=>function($query){
                            $query->where('verifikasi', 0);
                        }))->first();

        $check = false;
        if($checkData->pembayaran->detail_pembayaran->count() == 0)
            $check = true;    
        
            

        return view('order')->withData($data)->withCheck($check);
    }

    public function getTagihan(){
        $user_id = auth('pelanggan')->user()->id;

        $data = Pembayaran::whereHas('pesanan', function($q) use ($user_id)
                {
                    $q->where('pelanggan_id', '=', $user_id);
                })->where('status_pembayaran','Lunas Uang Muka')->get();
        
        return view('tagihan')->withData($data);
    }

    public function getDetailTagihan($id){
        $user_id = auth('pelanggan')->user()->id;

        
        $data = Pembayaran::with('pesanan.detail_pesanan.barang')
                        ->with('pesanan.beban')
                        ->with('pesanan.pelanggan')
                        ->where('id', $id)
                        ->first();
        if(is_null($data) || $data->pesanan->pelanggan_id != $user_id){
            return abort('404');
        }
        
        return view('detail_tagihan')->withData($data);
    }

    public function getProfil(){
        $data = auth('pelanggan')->user();
        
        return view('profile')->withData($data);
    }

    public function getSettings(){
        $data = auth('pelanggan')->user();
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: b91f866b918a08f9bebe3dc201dc92ed"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $result = null;
        if ($err) {
          return abort('404');
        } else {
          $result = json_decode($response);
        }
        $kota = $result->rajaongkir->results;

        return view('setting')->withData($data)->withKota($kota);
    }

    public function postSettings(Request $request){

        $file = array('image' => Input::file('file'));
        $rules = array('image' => 'required');
        $rules2 = array('image' => 'max:1000');
        $validator = Validator::make($file, $rules);
        $validator2 = Validator::make($file, $rules2);

        if($validator->fails()) {
                $kota = explode('-',Input::get('kota'));

                $id = auth('pelanggan')->user()->id;
                $data = Pelanggan::where('id', $id)->first();

                $data->nama = Input::get('nama');
                $data->tempat_lahir = Input::get('tempat_lahir');
                $data->tanggal_lahir = Input::get('tanggal_lahir');
                $data->kota = $kota[1];
                $data->kota_id = $kota[0];
                $data->kode_pos = Input::get('kode_pos');
                $data->alamat = Input::get('alamat');
                $data->no_telepon = Input::get('no_telepon');
                $data->save();
        } else {
            if($validator2->fails()) {
                return "MAX FILE ";
            }
            if(Input::file('file')->isValid()){
                $kota = explode('-',Input::get('kota'));
                $id = auth('pelanggan')->user()->id;
                $data = Pelanggan::where('id', $id)->first();

                $path = storage_path().'/app/user/';
                $extension = Input::file('file')->getClientOriginalExtension();

                $fileName = $data->foto;
                if($fileName == 'default.jpg')
                    $fileName = $data->id .Carbon::now()->format('YmdHis').'.'.$extension;

                if(strcasecmp($extension, 'jpg') != 0 && strcasecmp($extension, 'jpeg') != 0 && strcasecmp($extension, 'png') != 0){
                    Session::flash('messageError', 'Gambar Profil Tidak Valid');
                    return redirect('/settings');
                }
                
                Input::file('file')->move($path, $fileName);

                $data->nama = Input::get('nama');
                $data->tempat_lahir = Input::get('tempat_lahir');
                $data->tanggal_lahir = Input::get('tanggal_lahir');
                $data->kota = $kota[1];
                $data->kota_id = $kota[0];
                $data->kode_pos = Input::get('kode_pos');
                $data->alamat = Input::get('alamat');
                $data->no_telepon = Input::get('no_telepon');
                $data->foto = $fileName;
                $data->save();

            }else{
                return abort('500');
            }
        }

        Session::flash('message', 'Profil telah diperbarui');

        return redirect('/profil');
    }

    public function postSettingPassword(){
        $user = auth('pelanggan')->user();

        $validate_user = DB::table('pelanggan')
                            ->where('id', $user->id)
                            ->first();
        if ($validate_user && Hash::check(Input::get('old_password'), $validate_user->password)) {
          $user->password = Hash::make(Input::get('new_password'));
          $user->save();

          Session::flash('message', 'Password telah diperbarui');

          return redirect('/profil');
        }else{
            Session::flash('messageError', 'Password lama tidak sesuai');
            return redirect('/settings');
        }
    }

    public function postVerifikasi(){

        $id = Input::get('rowid');
        $data = Pesanan::with('pelanggan')->where('id', $id)->first();
        
        if(auth('pelanggan')->user()->id == $data->pelanggan->id){
            $data->verifikasi = 'Diterima';
            $data->save();
            Session::flash('message', 'Status diperbarui');
        }else{
            return redirect('/orders');
        }
    }
}