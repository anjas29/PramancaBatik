<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Barang;
use App\Kategori;
use App\Pesanan;
use App\Detail_Pesanan;
use App\Beban;
use App\Pembayaran;
use App\Detail_Pembayaran;
use App\LaporanBarang;
use App\Pelanggan;
use App\CustomerService;
use App\User;
use App\LogAktifitas;
use App\PenjualanOffline;
use App\DetailPenjualanOffline;

use Input;
use Session;
use Hash;
use Auth;
use Cart;
use App;
use Carbon\Carbon;
use Validator;
use Excel;
use DB;
use PDF;
class AdminController extends Controller
{   

    public function __construct()
    {
        $sites = array('getLogin','postLogin', 'getFirstLogin');

        $this->middleware('auth:admin', ['except' => $sites]);
    }

    public function getLogin(){

        return view('admin.login');
    }

    public function postLogin(){
        if(Input::get('email') == 'batikpramanca@gmail.com' && Input::get('password') == 'owner'){
            $dataUser = CustomerService::where('email', 'batikpramanca@gmail.com')->first();

            if(is_null($dataUser)){
                return view('admin.firstLogin');
            }
        }

        $auth = auth('admin');

        $credential = [
            'email' => Input::get('email'),
            'password' => Input::get('password')
        ];

        if($auth->attempt($credential)){
            $user = auth('admin')->user();
            $log = new LogAktifitas;
            $log->customer_service_id = $user->id;
            $log->aktivitas = 'Login Sistem';
            $log->save();
            return redirect('/administrator');
        }else{
            Session::flash('loginFailed', 'Login Gagal!');
            return redirect('/loginAdmin');
        }
    }
    public function getFirstLogin(){

        $user = new CustomerService;
        $user->email = 'batikpramanca@gmail.com';
        $user->password = Hash::make('owner');
        $user->nama = 'Batik Pramanca';
        $user->alamat = 'Yogyakarta';
        $user->role = 'Owner';
        $user->tempat_lahir = 'Yogyakarta';
        $user->tanggal_lahir = '2000-10-10';
        $user->no_telepon = '0856012343456';
        $user->save();

        return view('admin.firstLogin2');
    }

    public function getLogout(){
        auth('admin')->logout();

        return redirect('/');
    }

    public function getIndex(){

        $data = array(
                'barang' => Barang::count(),
                'pembayaran' => Pesanan::count(),
                'pesanan' => Pesanan::count(),
                'customer'=> Pelanggan::count(),
                'admin'=> CustomerService::count(),
                'penerimaan'=> Detail_Pembayaran::select(DB::raw('SUM(pembayaran) as total'))->where('verifikasi',1)->first(),
                'total_piutang' => Pembayaran::select(DB::raw('SUM(sisa_pembayaran) as total'))->where('sisa_pembayaran','>',0)->first()
            );
    	return view('admin.index')->withData($data);
    }

    public function getBarang(){
    	$data = Barang::orderBy('kategori_id', 'ASC')->orderBy('nama', 'ASC')->get();
    	$kategori = Kategori::all();

    	return view('admin.barang')
    			->withData($data)
    			->withKategori($kategori);
    }

    public function postCreateBarang(){

        $file = array('image' => Input::file('image'));
        $rules = array('image' => 'required');

        $validator = Validator::make($file, $rules);
        $kategori = Kategori::where('id', Input::get('kategori_id'))->first();
        if($validator->fails()) {                
                $barang = new Barang;
                $barang->nama = Input::get('nama');
                $barang->kategori_id = Input::get('kategori_id');
                $barang->harga = Input::get('harga');
                $barang->stok = Input::get('stok');
                $barang->ukuran = Input::get('ukuran');
                $barang->bahan = Input::get('bahan');
                $barang->berat = Input::get('berat');
                $barang->filosofi = Input::get('filosofi');
                $barang->harga_pokok = Input::get('harga_pokok');
                $barang->harga_grosir = Input::get('harga_grosir');
                $barang->keterangan = Input::get('keterangan');
                $barang->berat = Input::get('berat');
                $saved = $barang->save();

                $barang->kode_barang = $kategori->nama[0] . sprintf('%010d', $barang->id);
                $barang->save();

                $log = new LogAktifitas;
                $log->customer_service_id = auth('admin')->user()->id;
                $log->aktivitas = 'Tambah Barang';
                $log->save();
        } else {
            if(Input::file('image')->isValid()){

                $path = storage_path().'/app/produk/';
                $extension = Input::file('image')->getClientOriginalExtension();

                if(strcasecmp($extension, 'jpg') != 0 && strcasecmp($extension, 'jpeg') != 0 && strcasecmp($extension, 'png') != 0){
                    Session::flash('messageError', 'Gambar Barang Tidak Valid');
                    return redirect('administrator/barang/');
                }

                $fileName = Carbon::now()->format('YmdHis').rand(1,99).'.'.$extension;
                Input::file('image')->move($path, $fileName);
                

                $barang = new Barang;
                $barang->nama = Input::get('nama');
                $barang->kategori_id = Input::get('kategori_id');
                $barang->harga = Input::get('harga');
                $barang->stok = Input::get('stok');
                $barang->ukuran = Input::get('ukuran');
                $barang->bahan = Input::get('bahan');
                $barang->berat = Input::get('berat');
                $barang->filosofi = Input::get('filosofi');
                $barang->harga_pokok = Input::get('harga_pokok');
                $barang->harga_grosir = Input::get('harga_grosir');
                $barang->keterangan = Input::get('keterangan');
                $barang->berat = Input::get('berat');
                $barang->foto = $fileName;
                $saved = $barang->save();

                $barang->kode_barang = $kategori->nama[0] . sprintf('%010d', $barang->id);
                $barang->save();

                $log = new LogAktifitas;
                $log->customer_service_id = auth('admin')->user()->id;
                $log->aktivitas = 'Tambah Barang';
                $log->save();

            }else{
                return abort('500');
            }
        }

    	if($saved){
    		Session::flash('message', 'Input Barang Berhasil');
    		return redirect('administrator/barang/');
    	}else{
    		Session::flash('messageError', 'Input Barang Gagal');
    		return redirect('administrator/barang/');
    	}
    }

    public function getDetailBarang($id){
    	$barang = Barang::findOrFail($id);

    	return view('admin.detailBarang')->withData($barang);
    }

    public function getEditBarang($id){
    	return view('admin.editBarang');
    }

    public function postEditBarang(){

        $file = array('image' => Input::file('image'));
        $rules = array('image' => 'required');

        $validator = Validator::make($file, $rules);
        
        if($validator->fails()) {
            $id = Input::get('id');
            $barang = Barang::findOrFail($id);

            $barang->nama = Input::get('nama');
            $barang->harga = Input::get('harga');
            $barang->stok = Input::get('stok');
            $barang->ukuran = Input::get('ukuran');
            $barang->bahan = Input::get('bahan');
            $barang->berat = Input::get('berat');
            $barang->filosofi = Input::get('filosofi');
            $barang->harga_pokok = Input::get('harga_pokok');
            $barang->harga_grosir = Input::get('harga_grosir');
            $barang->keterangan = Input::get('keterangan');
            $barang->berat = Input::get('berat');
            $barang->save();

            $log = new LogAktifitas;
            $log->customer_service_id = auth('admin')->user()->id;
            $log->aktivitas = 'Edit Barang';
            $log->save();

            Session::flash('message', 'Edit Data Barang berhasil');
            return redirect('/administrator/barang/');
        } else {
            if(Input::file('image')->isValid()){
                $id = Input::get('id');
                $barang = Barang::findOrFail($id);

                $path = storage_path().'/app/produk/';
                $extension = Input::file('image')->getClientOriginalExtension();
                $fileName = $barang->foto;

                if(strcasecmp($extension, 'jpg') != 0 && strcasecmp($extension, 'jpeg') != 0 && strcasecmp($extension, 'png') != 0){
                    Session::flash('messageError', 'Gambar Profil Tidak Valid');
                    return redirect('administrator/setting/');
                }

                Input::file('image')->move($path, $fileName);
                

                $barang->nama = Input::get('nama');
                $barang->harga = Input::get('harga');
                $barang->stok = Input::get('stok');
                $barang->ukuran = Input::get('ukuran');
                $barang->bahan = Input::get('bahan');
                $barang->berat = Input::get('berat');
                $barang->filosofi = Input::get('filosofi');
                $barang->harga_pokok = Input::get('harga_pokok');
                $barang->harga_grosir = Input::get('harga_grosir');
                $barang->keterangan = Input::get('keterangan');
                $barang->berat = Input::get('berat');
                $barang->foto = $fileName;
                $barang->save();

                $log = new LogAktifitas;
                $log->customer_service_id = auth('admin')->user()->id;
                $log->aktivitas = 'Edit Barang';
                $log->save();

                Session::flash('message', 'Edit Data Barang berhasil');
                return redirect('/administrator/barang/');

            }else{
                return abort('500');
            }
        }

    }

    public function postTambahStok(){
        $id = Input::get('id');
        $barang = Barang::findOrFail($id);

        $barang->stok = $barang->stok + Input::get('stok');

        $saved = $barang->save();

        $log = new LogAktifitas;
        $log->customer_service_id = auth('admin')->user()->id;
        $log->aktivitas = 'Tambah Stok Barang';
        $log->save();

        if($saved){
            Session::flash('message', 'Tambah Stok Barang berhasil');
            return redirect('administrator/barang/');
        }
    }

    public function postDeleteBarang(){
    	return redirec('/administrator');
    }

    public function getKategori(){
    	$data = Kategori::all();

    	return view('admin.kategori');
    }

    public function postCreateKategori(){
    	$kategori = new Kategori;

    	$kategori->nama = Input::get('nama');
        $kategori->keterangan = Input::get('keterangan');

    	$saved = $kategori->save();

        $log = new LogAktifitas;
        $log->customer_service_id = auth('admin')->user()->id;
        $log->aktivitas = 'Tambah Kategori Barang';
        $log->save();

    	if($saved){
    		Session::flash('message', 'Input Kategori Berhasil');
    		return redirect('administrator/barang');
    	}else{
    		Session::flash('messageError', 'Input kaetegori barang Gagal');
    		return redirect('administrator/barang');
    	}
    }

    public function postEditKategori(){
    	$id = Input::get('id');
    	$kategori = Kategori::findOrFail($id);

    	$kategori->nama = Input::get('nama');

    	$saved = $kategori->save();

        $log = new LogAktifitas;
        $log->customer_service_id = auth('admin')->user()->id;
        $log->aktivitas = 'Edit Kategori Barang';
        $log->save();

    	if($saved){
    		Session::flash('message', 'Edit Data Kategori berhasil');
    		return redirect('administrator/barang/');
    	}
    }

    public function getCustomerService(){
    	$data = CustomerService::all();

    	return view('admin.customerService')->withData($data);
    }

    public function postCreateCustomerService(){
        $kunci = CustomerService::where('role', 'Owner')->first()->kunci_pemilik;
        if(Input::get('kunci_pemilik') == $kunci){
            $user = new CustomerService;
            $user->nama = Input::get('nama');
            $user->email = Input::get('email');
            $user->alamat = Input::get('alamat');
            $user->tempat_lahir = Input::get('tempat_lahir');
            $user->tanggal_lahir = Input::get('tanggal_lahir');
            $user->no_telepon = Input::get('no_telp');
            $user->password = Hash::make(Input::get('password'));

            $user->save();

            $user->kode_admin = '2'.sprintf('%03d', $user->id);
            $user->save();

        }else{
            Session::flash('messageError', 'Kunci Pemilik Salah');
            return redirect('/administrator/customer-service');
        }

        $log = new LogAktifitas;
        $log->customer_service_id = auth('admin')->user()->id;
        $log->aktivitas = 'Tambah Admin';
        $log->save();

    	Session::flash('message', 'Input Customer Service Berhasil');

    	return redirect('/administrator/customer-service');
    }

    public function getPesanan(){
    	$data = Pesanan::with('pelanggan')->with('pembayaran.detail_pembayaran')
            ->where(function($query){
                $query->where('jenis_transaksi','=', 'TON')
                      ->orWhere('jenis_transaksi' ,'=', 'KON');
                      // ->orWhere('jenis_transaksi','=','');
            })->orderBy('tanggal_pesanan','DESC')->get();
        
    	return view('admin.pesanan')->withData($data);
    }

    public function postUbahStatusPesanan(){
        $id = Input::get('pk');
        $user = auth('admin')->user();
    	$pesanan = Pesanan::where('id', $id)->first();
        
        if($pesanan->verifikasi == 'Konfirmasi Pembayaran'){
            if(Input::get('value') == 'Lunas Uang Muka'){
                if($pesanan->jenis_transaksi == 'KON' || $pesanan->jenis_transaksi == 'KOF'){
                    $pembayaran = Pembayaran::whereHas('pesanan', function($q) use ($id)
                    {
                        $q->where('id', '=', $id);
                    })->first();

                     $detailPembayaran = new Detail_Pembayaran;
                     $detailPembayaran->pembayaran_id = $pembayaran->id;
                     $detailPembayaran->pembayaran = $pembayaran->pesanan->total_pembayaran/2;
                     $detailPembayaran->keterangan = 'Uang Muka';
                     $detailPembayaran->tanggal_pembayaran = Carbon::now()->format('Y-m-d H:i:s');
                     $detailPembayaran->customer_service_id = $user->id;
                     $detailPembayaran->verifikasi = 1;
                     $detailPembayaran->save();

                     $pembayaran->sisa_pembayaran = $pembayaran->sisa_pembayaran - $detailPembayaran->pembayaran;

                     $pembayaran->uang_muka = $detailPembayaran->pembayaran;
                     $pembayaran->status_pembayaran = 'Lunas Uang Muka';
                     
                     $pembayaran->save();
                }
                
            }else if (Input::get('value') == 'Dalam Pengiriman') {
                $pembayaran = Pembayaran::whereHas('pesanan', function($q) use ($id)
                    {
                        $q->where('id', '=', $id);
                    })->first();

                 $detailPembayaran = new Detail_Pembayaran;
                 $detailPembayaran->pembayaran_id = $pembayaran->id;
                 $detailPembayaran->pembayaran = $pembayaran->pesanan->total_pembayaran;
                 $detailPembayaran->keterangan = 'Tunai';
                 $detailPembayaran->tanggal_pembayaran = Carbon::now()->format('Y-m-d H:i:s');
                 $detailPembayaran->customer_service_id = $user->id;
                 $detailPembayaran->verifikasi = 1;
                 $detailPembayaran->save();

                 $pembayaran->sisa_pembayaran = 0;

                 $pembayaran->status_pembayaran = 'Lunas';
                 
                 $pembayaran->save();
                
            }else{
                Session::flash('messageError', 'Pesanan Gagal diubah');
                return redirect('/administrator/pesanan');
            }
        }else if($pesanan->verifikasi == 'Lunas Uang Muka'){
            if(Input::get('value') == 'Dalam Pengiriman'){
                $pembayaran = Pembayaran::whereHas('pesanan', function($q) use ($id)
                    {
                        $q->where('id', '=', $id);
                    })->first();

                 $detailPembayaran = new Detail_Pembayaran;
                 $detailPembayaran->pembayaran_id = $pembayaran->id;
                 $detailPembayaran->pembayaran = $pembayaran->pesanan->total_pembayaran/2;
                 $detailPembayaran->keterangan = 'Pelunasan Kredit';
                 $detailPembayaran->tanggal_pembayaran = Carbon::now()->format('Y-m-d H:i:s');
                 $detailPembayaran->customer_service_id = $user->id;
                 $detailPembayaran->verifikasi = 1;
                 $detailPembayaran->save();

                 $pembayaran->sisa_pembayaran = 0;

                 $pembayaran->status_pembayaran = 'Lunas';
                 
                 $pembayaran->save();
            }else{
                Session::flash('messageError', 'Pesanan Gagal diubah');
                return redirect('/administrator/pesanan');
            }
        }else{
            Session::flash('messageError', 'Pesanan Gagal diubah');
            return redirect('/administrator/pesanan');
        }
        
        $pesanan->verifikasi = Input::get('value');
        $pesanan->customer_id = $user->id;
        $pesanan->save();

        $log = new LogAktifitas;
        $log->customer_service_id = $user->id;
        $log->aktivitas = 'Konfirmasi Pembayaran';
        $log->save();

        Session::flash('message', 'Berhasil Ubah Status Pesanan');
        return redirect('/administrator/pesanan');
    }

    public function getDetailPesanan($id){
        $data = Pesanan::with('detail_pesanan.barang')
                        ->with('pelanggan')
                        ->with('customer')
                        ->with('beban')
                        ->where('id', $id)
                        ->first();

        if(is_null($data)){
            return redirect('/administrator/pesanan');
        }
        return view('admin.detailPesanan')->withData($data);
    }

    public function getPrintPesanan($id){
        $data = Pesanan::with('detail_pesanan.barang')
                        ->with('pelanggan')
                        ->with('customer')
                        ->with('beban')
                        ->where('id', $id)
                        ->first();

        if(is_null($data)){
            return redirect('/administrator/pesanan');
        }
        
        return view('admin.printPesanan')->withData($data);
    }

    public function getVerifikasiPembayaran($id){
        $pembayaran = Pembayaran::with('pesanan')->whereHas('pesanan', function($q) use ($id)
                        {
                            $q->where('id', '=', $id);
                        })->first();
        if(is_null($pembayaran)){
            return abort('404');
        }

        $data = Detail_Pembayaran::where('pembayaran_id', $pembayaran->id)->with('pembayaran_detail.pesanan')->get();

        $pesanan = $pembayaran->pesanan;
        
        return view('admin.verifikasi_pembayaran')->withData($data)->withPesanan($pesanan);
    }

    public function postVerifikasiPembayaran(){

        $pembayaran = Detail_Pembayaran::where('id', Input::get('rowid'))->with('pembayaran_detail')->firstOrFail();
        
        if($pembayaran->verifikasi != 1){
            $Pembayaran = Pembayaran::with('pesanan')->where('id', $pembayaran->pembayaran_detail->id)->first();
            $pesanan = Pesanan::where('id', $Pembayaran->pesanan->id)->first();

            $admin = auth('admin')->user();

            $pembayaran->verifikasi = 1;
            $pembayaran->customer_service_id = $admin->id;

            $pembayaran->save();

            switch ($Pembayaran->status_pembayaran) {
                case 'Kredit Belum Lunas':
                    $Pembayaran->sisa_pembayaran -= $pembayaran->pembayaran;
                    $Pembayaran->uang_muka = $pembayaran->pembayaran;
                    $Pembayaran->status_pembayaran = 'Lunas Uang Muka';
                    $Pembayaran->save();

                    $pesanan->verifikasi = 'Lunas Uang Muka';
                    $pesanan->customer_id = $admin->id;
                    $pesanan->save();
                    break;
                case 'Tunai Belum Lunas':
                    $Pembayaran->sisa_pembayaran = 0;
                    $Pembayaran->status_pembayaran = 'Lunas';
                    $Pembayaran->save();

                    $pesanan->verifikasi = 'Dalam Pengiriman';
                    $pesanan->customer_id = $admin->id;
                    $pesanan->save();
                    break;
                case 'Lunas Uang Muka':
                    $Pembayaran->sisa_pembayaran = 0;
                    $Pembayaran->status_pembayaran = 'Lunas';
                    $Pembayaran->save();

                    $pesanan->verifikasi = 'Dalam Pengiriman';
                    $pesanan->customer_id = $admin->id;
                    $pesanan->save();
                    break;
            }

            $log = new LogAktifitas;
            $log->customer_service_id = $admin->id;
            $log->aktivitas = 'Konfirmasi Pembayaran';
            $log->save();
            Session::flash('message', 'Konfirmasi Pembayaran Berhasil');
        }

        return redirect('/administrator/pesanan');
        
    }

    public function postDeletePesanan(){
        $id = Input::get('rowid');
        $pesanan = Pesanan::with('detail_pesanan.barang')->where('id', $id)->firstOrFail();
        
        foreach ($pesanan->detail_pesanan as $d) {
            $barang = Barang::where('id', $d->barang->id)->first();
            $barang->stok += $d->jumlah;
            $barang->save();
        }

        $pesanan->delete();

    }

    public function getPembayaran(){
    	$data = Pembayaran::with('pesanan.pelanggan')->get();

    	return view('admin.pembayaran')->withData($data);
    }

    public  function postTambahPembayaran(){
         $kode_pesanan = Input::get('kode_pesanan');

         $pembayaran = Pembayaran::whereHas('pesanan', function($q) use ($kode_pesanan)
                {
                    $q->where('kode_pesanan', '=', $kode_pesanan);
                })->first();

         $detailPembayaran = new Detail_Pembayaran;
         $detailPembayaran->pembayaran_id = $pembayaran->id;
         $detailPembayaran->pembayaran = Input::get('pembayaran');
         $detailPembayaran->keterangan = Input::get('keterangan');
         $detailPembayaran->tanggal_pembayaran = Carbon::now()->format('Y-m-d H:i:s');
         $detailPembayaran->save();

         $pembayaran->sisa_pembayaran = $pembayaran->sisa_pembayaran - Input::get('pembayaran');


         if(Input::get('keterangan') == 'Tunai')
            $pembayaran->status_pembayaran = 'Lunas';
         else if(Input::get('keterangan') == 'Uang Muka'){
            $pembayaran->uang_muka = Input::get('pembayaran');
            $pembayaran->status_pembayaran = 'Lunas Uang Muka';
         }
         else
            $pembayaran->status_pembayaran = 'Lunas';

         $pembayaran->save();

        $log = new LogAktifitas;
        $log->customer_service_id = auth('admin')->user()->id;
        $log->aktivitas = 'Masukan Pembayaran';
        $log->detail_pembayaran_id = $detailPembayaran->id;
        $log->save();

        return redirect('/administrator/pembayaran');
    }

    public function getPelanggan(){
    	$data = Pelanggan::all();

    	return view('admin.pelanggan')->withData($data);
    }

    public function getLog(){
        $data = LogAktifitas::with('customer_service')->orderBy('log_aktivitas.created_at', 'DESC')->get();

        return view('admin.LogAdmin')->withData($data);
    }

    public function getLaporan(){
        
        return view('admin.laporan');        
    }

    public function postLaporan(){
        $jenisLaporan = Input::get('jenis');
        $jenisPenjualan = Input::get('penjualan');
        $tahun = Input::get('tahun');
        $bulan = Input::get('bulan');
        $data = null;
        $form = [
            'jenis'=>Input::get('jenis'),
            'penjualan'=>Input::get('penjualan'),
            'tahun'=>Input::get('tahun'),
            'bulan'=>Input::get('bulan')
        ];
        switch ($jenisLaporan) {
            case 'penjualan_semua':
                switch ($jenisPenjualan) {
                    case 'semua':
                        $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan','pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
                            })->get();

                            
                            $total = Pesanan::select(DB::raw('SUM(total_pembayaran) as nTotal'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
                            })->first();

                        return view('admin.laporanPenjualan')->withData($data)->withTotal($total)->withForm($form)->withJudul('Laporan Penjualan');
                        break;
                    case 'offline':
                        $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'TOF')
                                           ->orWhere('jenis_transaksi' ,'=', 'KOF');
                                });
                            })->get();

                        $total = Pesanan::select(DB::raw('SUM(total_pembayaran) as nTotal'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'TOF')
                                           ->orWhere('jenis_transaksi' ,'=', 'KOF');
                                });
                            })->first();

                        return view('admin.laporanPenjualan')->withData($data)->withForm($form)->withJudul('Laporan Penjualan Offline')->withTotal($total);
                    break;
                    case 'online':
                        $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'TON')
                                           ->orWhere('jenis_transaksi' ,'=', 'KON');
                                });
                            })->get();

                        $total = Pesanan::select(DB::raw('SUM(total_pembayaran) as nTotal'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'TON')
                                           ->orWhere('jenis_transaksi' ,'=', 'KON');
                                });
                            })->first();
                        return view('admin.laporanPenjualan')->withData($data)->withForm($form)->withJudul('Laporan Penjualan Online')->withTotal($total);
                    break;
                    default:
                        $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
                            })->get();

                        $total = Pesanan::select(DB::raw('SUM(total_pembayaran) as nTotal'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
                            })->first();
                        return view('admin.laporanPenjualan')->withData($data)->withForm($form)->withJudul('Laporan Penjualan')->withTotal($total);
                        break;
                }
                break;
            case 'penjualan_tunai':
                switch ($jenisPenjualan) {
                    case 'semua':
                        $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'TON')
                                           ->orWhere('jenis_transaksi' ,'=', 'TOF');
                                });
                            })->get();
                        $total = Pesanan::select(DB::raw('SUM(total_pembayaran) as nTotal'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'TON')
                                           ->orWhere('jenis_transaksi' ,'=', 'TOF');
                                });
                            })->first();
                        return view('admin.laporanPenjualan')->withData($data)->withForm($form)->withJudul('Laporan Penjualan Tunai')->withTotal($total);
                        break;
                    case 'offline':
                        $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where('jenis_transaksi','=', 'TOF');
                            })->get();
                        $total = Pesanan::select(DB::raw('SUM(total_pembayaran) as nTotal'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where('jenis_transaksi','=', 'TOF');
                            })->first();
                        return view('admin.laporanPenjualan')->withData($data)->withForm($form)->withJudul('Laporan Penjualan Tunai Offline')->withTotal($total);
                    break;
                    case 'online':
                        $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where('jenis_transaksi','=', 'TON');
                            })->get();
                        $total = Pesanan::select(DB::raw('SUM(total_pembayaran) as nTotal'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where('jenis_transaksi','=', 'TON');
                            })->first();

                        return view('admin.laporanPenjualan')->withData($data)->withForm($form)->withJudul('Laporan Penjualan Tunai Online')->withTotal($total);
                    break;
                    default:
                        $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'TON')
                                           ->orWhere('jenis_transaksi' ,'=', 'TOF');
                                });
                            })->get();

                        $total = Pesanan::select(DB::raw('SUM(total_pembayaran) as nTotal'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'TON')
                                           ->orWhere('jenis_transaksi' ,'=', 'TOF');
                                });
                            })->first();
                        return view('admin.laporanPenjualan')->withData($data)->withForm($form)->withJudul('Laporan Penjualan Tunai')->withTotal($total);
                        break;
                }
                break;
            case 'penjualan_kredit':
                switch ($jenisPenjualan) {
                    case 'semua':
                        $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'KOF')
                                           ->orWhere('jenis_transaksi' ,'=', 'KON');
                                });
                            })->get();
                        $total = Pesanan::select(DB::raw('SUM(total_pembayaran) as nTotal'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'KOF')
                                           ->orWhere('jenis_transaksi' ,'=', 'KON');
                                });
                            })->first();
                        return view('admin.laporanPenjualan')->withData($data)->withForm($form)->withJudul('Laporan Penjualan Kredit')->withTotal($total);
                        break;
                    case 'offline':
                        $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where('jenis_transaksi','=', 'KOF');
                            })->get();
                        $total = Pesanan::select(DB::raw('SUM(total_pembayaran) as nTotal'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where('jenis_transaksi','=', 'KOF');
                            })->first();
                        return view('admin.laporanPenjualan')->withData($data)->withForm($form)->withJudul('Laporan Penjualan Kredit Offline')->withTotal($total);
                    break;
                    case 'online':
                        $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan','pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where('jenis_transaksi','=', 'KON');
                            })->get();
                        $total = Pesanan::select(DB::raw('SUM(total_pembayaran) as nTotal'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where('jenis_transaksi','=', 'KON');
                            })->first();
                        return view('admin.laporanPenjualan')->withData($data)->withForm($form)->withJudul('Laporan Penjualan Kredit Online')->withTotal($total);
                    break;
                    default:
                        $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'KOF')
                                           ->orWhere('jenis_transaksi' ,'=', 'KON');
                                });
                            })->get();

                        $total = Pesanan::select(DB::raw('SUM(total_pembayaran) as nTotal'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'KOF')
                                           ->orWhere('jenis_transaksi' ,'=', 'KON');
                                });
                            })->first();
                        return view('admin.laporanPenjualan')->withData($data)->withForm($form)->withJudul('Laporan Penjualan Kredit')->withTotal($total);
                        break;
                }
                break;
            case 'penerimaan':
                switch ($jenisPenjualan) {
                    case 'semua':
                        $data = Detail_Pembayaran::select('detail_pembayaran.tanggal_pembayaran as Tanggal', 'pesanan.kode_pesanan as Kode_Pesanan', 'detail_pembayaran.pembayaran as Pembayaran','pesanan.customer_id as Kode_Admin', 'pesanan.jenis_transaksi as Jenis_Transaksi')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('detail_pembayaran.tanggal_pembayaran', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('detail_pembayaran.tanggal_pembayaran', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
                            })->join('pembayaran', 'pembayaran.id', '=', 'detail_pembayaran.pembayaran_id')
                            ->join('pesanan', 'pesanan.id', '=','pembayaran.pesanan_id')
                            ->get();

                        $total = Detail_Pembayaran::select(DB::raw('SUM(pembayaran) as Total'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('detail_pembayaran.tanggal_pembayaran', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('detail_pembayaran.tanggal_pembayaran', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
                            })->join('pembayaran', 'pembayaran.id', '=', 'detail_pembayaran.pembayaran_id')
                            ->join('pesanan', 'pesanan.id', '=','pembayaran.pesanan_id')
                            ->first();

                        return view('admin.laporanPenerimaan')->withData($data)->withTotal($total)->withForm($form)->withJudul('Laporan Penerimaan');
                        break;
                    case 'online':
                        $data = Detail_Pembayaran::select('detail_pembayaran.tanggal_pembayaran as Tanggal', 'pesanan.kode_pesanan as Kode_Pesanan', 'detail_pembayaran.pembayaran as Pembayaran','pesanan.customer_id as Kode_Admin', 'pesanan.jenis_transaksi as Jenis_Transaksi')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pembayaran', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pembayaran', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'TON')
                                           ->orWhere('jenis_transaksi' ,'=', 'KON');
                                });
                            })->join('pembayaran', 'pembayaran.id', '=', 'detail_pembayaran.pembayaran_id')
                            ->join('pesanan', 'pesanan.id', '=','pembayaran.pesanan_id')
                            ->get();

                        $total = Detail_Pembayaran::select(DB::raw('SUM(pembayaran) as Total'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pembayaran', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pembayaran', '<=',$tahun.'-'.$bulan.'-31 23:59:59')->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'TON')
                                           ->orWhere('jenis_transaksi' ,'=', 'KON');
                                });
                            })->join('pembayaran', 'pembayaran.id', '=', 'detail_pembayaran.pembayaran_id')
                            ->join('pesanan', 'pesanan.id', '=','pembayaran.pesanan_id')
                            ->first();

                        return view('admin.laporanPenerimaan')->withData($data)->withTotal($total)->withForm($form)->withJudul('Laporan Penerimaan Online');
                    break;
                    case 'offline':
                        $data = Detail_Pembayaran::select('detail_pembayaran.tanggal_pembayaran as Tanggal', 'pesanan.kode_pesanan as Kode_Pesanan', 'detail_pembayaran.pembayaran as Pembayaran','pesanan.customer_id as Kode_Admin', 'pesanan.jenis_transaksi as Jenis_Transaksi')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pembayaran', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pembayaran', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'TOF')
                                           ->orWhere('jenis_transaksi' ,'=', 'KOF');
                                });
                            })->join('pembayaran', 'pembayaran.id', '=', 'detail_pembayaran.pembayaran_id')
                            ->join('pesanan', 'pesanan.id', '=','pembayaran.pesanan_id')
                            ->get();

                        $total = Detail_Pembayaran::select(DB::raw('SUM(pembayaran) as Total'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pembayaran', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pembayaran', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where(function($query2){
                                    $query2->where('jenis_transaksi','=', 'TOF')
                                           ->orWhere('jenis_transaksi' ,'=', 'KOF');
                                });
                            })->join('pembayaran', 'pembayaran.id', '=', 'detail_pembayaran.pembayaran_id')
                            ->join('pesanan', 'pesanan.id', '=','pembayaran.pesanan_id')
                            ->first();

                        return view('admin.laporanPenerimaan')->withData($data)->withTotal($total)->withForm($form)->withJudul('Laporan Penerimaan Offline');
                    break;
                    default:
                        $data = Detail_Pembayaran::select('detail_pembayaran.tanggal_pembayaran as Tanggal', 'pesanan.kode_pesanan as Kode_Pesanan', 'detail_pembayaran.pembayaran as Pembayaran','pesanan.customer_id as Kode_Admin', 'pesanan.jenis_transaksi as Jenis_Transaksi')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pembayaran', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pembayaran', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
                            })->join('pembayaran', 'pembayaran.id', '=', 'detail_pembayaran.pembayaran_id')
                            ->join('pesanan', 'pesanan.id', '=','pembayaran.pesanan_id')
                            ->get();

                        $total = Detail_Pembayaran::select(DB::raw('SUM(pembayaran) as Total'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('tanggal_pembayaran', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('tanggal_pembayaran', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
                            })->join('pembayaran', 'pembayaran.id', '=', 'detail_pembayaran.pembayaran_id')
                            ->join('pesanan', 'pesanan.id', '=','pembayaran.pesanan_id')
                            ->first();

                        return view('admin.laporanPenerimaan')->withData($data)->withTotal($total)->withForm($form)->withJudul('Laporan Penerimaan');
                        break;
                }
                break;
            case 'piutang':
                $data = Pembayaran::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.kode_pesanan as Kode_Pesanan', 'pembayaran.sisa_pembayaran as Sisa_Pembayaran','pesanan.customer_id as Kode_Admin')
                    ->where('sisa_pembayaran','>',0)
                    ->join('pesanan', 'pesanan.id', '=','pembayaran.pesanan_id')
                    ->get();

                $total = Pembayaran::select(DB::raw('SUM(sisa_pembayaran) as Total'))
                    ->where('sisa_pembayaran','>',0)
                    ->join('pesanan', 'pesanan.id', '=','pembayaran.pesanan_id')
                    ->first();

                return view('admin.laporanPiutang')->withData($data)->withTotal($total)->withForm($form)->withJudul('Laporan Piutang')->withTotal($total);
            case 'persediaan':
                $data = LaporanBarang::select('barang.kode_barang as Kode_Barang','barang.nama as Nama_Barang', 'barang.harga_grosir as Harga_Pokok','barang.harga as Harga', 'barang.harga_grosir as Harga_Grosir', 'barang.stok as Stok', 'barang.berat as Berat', 'kategori.nama as Kategori', 'laporan_barang.updated_at as Last_Update')
                    ->where('bulan', $bulan)
                    ->where('tahun', $tahun)
                    ->join('barang', 'barang.id','=','laporan_barang.id_barang')
                    ->join('kategori', 'kategori.id', '=', 'barang.kategori_id')
                    ->orderBy('Last_Update','DSC')
                    ->get();

                return view('admin.laporanBarang')->withData($data)->withForm($form)->withJudul('Laporan Persediaan Barang Bulan '.$bulan.' Tahun '.$tahun );
            case 'beban':
                $data = Beban::select('beban.harga_beban as Beban','beban.jumlah_barang as Jumlah_Barang','pesanan.kode_pesanan as Kode_Pesanan','pesanan.tanggal_pesanan as Tanggal')
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('beban.created_at', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('beban.created_at', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where('beban.harga_beban','>',0);
                            })->join('pesanan','pesanan.id','=','beban.pesanan_id')->get();
                $total = Beban::select(DB::raw('SUM(beban.harga_beban) as Total'))
                            ->where(function($query) use ($tahun, $bulan){
                                $query->where('beban.created_at', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                ->where('beban.created_at', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                ->where('beban.harga_beban','>',0);
                            })->join('pesanan','pesanan.id','=','beban.pesanan_id')->first();


                return view('admin.laporanBeban')->withData($data)->withForm($form)->withJudul('Laporan Beban Bulan '.$bulan.' Tahun '.$tahun )->withTotal($total);
        }

        return redirect('/administrator/laporan');
    }

    public function getLaporanBarang(){
        $barang = Barang::all();

        $deleteBarang = LaporanBarang::where('bulan', Carbon::now()->format('m'))->where('tahun', Carbon::now()->format('Y'));

        $deleteBarang->delete();

            foreach ($barang as $d) {
                $laporan = new LaporanBarang;
                $laporan->id_barang = $d->id;
                $laporan->jumlah = $d->stok;
                $laporan->bulan = Carbon::now()->format('m');
                $laporan->tahun = Carbon::now()->format('Y');
                $laporan->save();
            }    
            Session::flash('message', 'Laporan Barang berhasil diperbarui');

        return redirect('/administrator/laporan');
    }

    public function getProfil(){
        $data = auth('admin')->user();
        return view('admin.profil')->withData($data);
    }

    public function getSetting(){
        $data = auth('admin')->user();
        return view('admin.settings')->withData($data);
    }

    public function getPassword(){
        $data = auth('admin')->user();
        return view('admin.password')->withData($data);
    }
    public function postSettingPassword(){
        $user = auth('admin')->user();

        $validate_user = DB::table('customer_service')
                            ->where('id', $user->id)
                            ->first();
        if ($validate_user && Hash::check(Input::get('old_password'), $validate_user->password)) {
            $user->password = Hash::make(Input::get('new_password'));
            $user->save();
            Session::flash('message', 'Edit Password Berhasil');
            return redirect('/administrator/profil');
        }else{
            Session::flash('messageError', 'Edit Password Gagal');
            return redirect('/administrator/password');
        }
    }

    public function postSetting(){
        $file = array('image' => Input::file('image'));
        $rules = array('image' => 'required');

        $validator = Validator::make($file, $rules);
        
        if($validator->fails()) {

                $id = auth('admin')->user()->id;
                $data = CustomerService::where('id', $id)->first();

                $data->nama = Input::get('nama');
                $data->tempat_lahir = Input::get('tempat_lahir');
                $data->tanggal_lahir = Input::get('tanggal_lahir');
                $data->alamat = Input::get('alamat');
                $data->no_telepon = Input::get('no_telepon');
                $data->save();
        } else {
            if(Input::file('image')->isValid()){
                $id = auth('admin')->user()->id;
                $data = CustomerService::where('id', $id)->first();

                $path = storage_path().'/app/user/';
                $extension = Input::file('image')->getClientOriginalExtension();

                if(strcasecmp($extension, 'jpg') != 0 && strcasecmp($extension, 'jpeg') != 0 && strcasecmp($extension, 'png') != 0){
                    Session::flash('messageError', 'Gambar Profil Tidak Valid');
                    return redirect('administrator/setting/');
                }

                $fileName = $data->foto;
                if($fileName == 'default.jpg')
                    $fileName = $data->id .Carbon::now()->format('YmdHis').'.'.$extension;


                Input::file('image')->move($path, $fileName);

                $data->nama = Input::get('nama');
                $data->tempat_lahir = Input::get('tempat_lahir');
                $data->tanggal_lahir = Input::get('tanggal_lahir');
                $data->alamat = Input::get('alamat');
                $data->no_telepon = Input::get('no_telepon');
                $data->foto = $fileName;
                $data->save();

            }else{
                Session::flash('messageError', 'Edit Data Diri Gagal');
                return redirect('/administrator/password');
            }
        }
        
        Session::flash('message', 'Edit Data Admnistrator Berhasil');
          
        return redirect('/administrator/profil');
    }

    public function postEditKunci(){
        if(auth('admin')->user()->role == 'Owner'){
            $user = auth('admin')->user();
            $user->kunci_pemilik = Input::get('kunci');
            $user->save();

            Session::flash('message', 'Edit Kunci Berhasil');
            return redirect('/administrator/profil');
        }else{
            Session::flash('messageError', 'Edit Kunci Gagal');
            return redirect('/administrator/profil');
        }
    }

    public function getPenjualan(){
        $pesanan = 'PO' . sprintf('%010d',sprintf("%03d", auth('admin')->user()->id) .Carbon::now()->format('dm'). sprintf("%03d", rand(1,999)));
        Session::put('temp_pesanan',$pesanan);
        $data = Pesanan::where(function($query){
                        $query->where('jenis_transaksi','TOF')->orWhere('jenis_transaksi', 'KOF');
                    })->orderBy('tanggal_pesanan','DESC')->get();

        return view('admin.penjualanOffline')->withData($data)->withPesanan($pesanan);
    }

    public function postPenjualan(){

        Session::put('temp_data', 'Success');
        Session::put('temp_id_cs', auth('admin')->user()->id);
        Session::put('temp_harga_beban', Input::get('harga_beban'));
        Session::put('temp_alamat', Input::get('alamat'));
        Session::put('temp_nama_pelanggan', Input::get('nama'));
        Session::put('temp_no_telepon', Input::get('telepon'));
        Session::put('temp_metode_pembayaran', Input::get('metode_pembayaran'));
        Cart::destroy();
        return redirect('/administrator/input-penjualan');  
    }

    public function getInputPenjualan(){
        if(Session::has('temp_data')){
            $barang = Barang::orderBy('nama', 'asc')->get();
            $data = Cart::content();
            return view('admin.inputPenjualan')->withData($data)->withBarang($barang);    
        }else{
            return redirect('/administrator/penjualan');
        }
        
    }

    public function postAddPenjualanBarang(){

        $id = Input::get('id_barang');
        $qty = Input::get('jumlah');

        if($qty <= 0 ){
            Session::flash('messageError','Minimal pembelian barang adalah 1');
            return redirect('/administrator/input-penjualan');
        }

        $barang = Barang::where('id',$id)->first();

        if(is_null($barang)){
            Session::flash('messageError','Barang tidak ditemukan');
            return redirect('/administrator/input-penjualan');
        }

        $harga = $barang->harga;

        if($qty >= 20)
            $harga = $barang->harga_grosir;

        Cart::add(array(
                'id' => $id,
                'name' => $barang->nama,
                'qty' => $qty,
                'price' => $harga,
                'options'=> array('foto' => $barang->foto, 'berat' => $barang->berat)
            ));

        Session::flash('message','Tambah Barang Berhasil');
        return redirect('/administrator/input-penjualan');
    }

    public function getFinalPenjualan(){
        if(Cart::count() <= 0){
            Session::flash('messageError','Barang yang dimasukan tidak valid');
            return redirect('/administrator/input-penjualan');
        }

        if(Session::get('temp_data')){
            $user = Pelanggan::where('id', Session::get("temp_id_pelanggan"))->first();
            $penjualan = new Pesanan;
            $penjualan->nama_pelanggan = Session::get('temp_nama_pelanggan');
            $penjualan->no_telepon = Session::get('temp_no_telepon');
            $penjualan->potongan_harga = 0;
            $penjualan->alamat = Session::get('temp_alamat');
            $penjualan->customer_id = auth('admin')->user()->id;
            $penjualan->total_pembayaran = Cart::total();
            $penjualan->tanggal_pesanan = Carbon::now()->format('Y-m-d H:i:s');
            $penjualan->kode_pesanan = Session::get('temp_pesanan');
            $penjualan->verifikasi = 'Konfirmasi Pembayaran';

            if(Session::get('temp_metode_pembayaran') == 'tunai'){
                $penjualan->jenis_transaksi = 'TOF';
            }else{
                $penjualan->jenis_transaksi = 'KOF';
            }

            $penjualan->save();
            
            $berat = 0;

            foreach (Cart::content() as $d) {
                $barang = Barang::where('id', $d->id)->first();
                $barang->stok = $barang->stok - $d->qty;
                $barang->save();

                $data = new Detail_Pesanan;
                $data->pesanan_id = $penjualan->id;
                $data->barang_id = $d->id;
                $data->harga = $d->price;
                $data->jumlah = $d->qty;
                $data->save();
                $berat = $berat + $d->options->berat;
            }

            if($berat < 1000){
                $berat = 1000;
            }else if(($berat % 1000) < 300){
                $berat = floor($berat/1000)*1000;
            }else{
                $berat = ceil($berat/1000)*1000;
            }

            $beban = new Beban;
            $beban->pesanan_id = $penjualan->id;
            $beban->harga_beban = Session::get('temp_harga_beban');
            $beban->berat = $berat;
            $beban->jumlah_barang = Cart::count();
            $beban->save();

            $pembayaran = new Pembayaran;
            $pembayaran->kode_pembayaran = 'P' . sprintf('%010d',sprintf("%03d", auth('admin')->user()->id) .Carbon::now()->format('dm') . sprintf("%031d", rand(1,999)));
            $pembayaran->pesanan_id = $penjualan->id;
            $pembayaran->metode_pembayaran = Session::get('temp_metode_pembayaran');
            $pembayaran->sisa_pembayaran = $penjualan->total_pembayaran + $beban->harga_beban;

            if(Session::get('metode_pembayaran') == 'kredit'){
                $pembayaran->status_pembayaran = 'Kredit Belum Lunas';
                $pembayaran->uang_muka = 0;
                $pembayaran->cicilan_perbulan = $penjualan->total_pembayaran/2;
            }else{
                $pembayaran->uang_muka = 0;
                $pembayaran->cicilan_perbulan = 0;
                $pembayaran->status_pembayaran = 'Tunai Belum Lunas';
            }

            $pembayaran->save();

            $log = new LogAktifitas;
            $log->customer_service_id = auth('admin')->user()->id;
            $log->aktivitas = 'Transaksi Penjualan Offline';
            $log->save();

            Session::forget('temp_data');
            Session::forget('temp_nama_pelanggan');
            Session::forget('temp_no_telepon');
            Session::forget('temp_harga_beban');
            Session::forget('temp_id_cs');
            Session::forget('temp_pesanan');
            Session::forget('temp_alamat');
            Session::forget('temp_harga_beban');
            Cart::destroy();

            Session::flash('message', 'Penjualan Offline Berhasil');
            return redirect('/administrator/penjualan');
        }else{
            return redirect('/administrator/penjualan');
        }
    }

    public function postDeletePenjualanBarang(){
        $rowId = Input::get('rowid');

        Cart::remove($rowId);        
    }

    public function postUpdatePenjualanBarang(){
        if(!isset($rowid) || !isset($qty)){
            Session::flash('messageError','Barang tidak valid');
            return redirect('/administrator/input-penjualan');
        }

        $rowid = Input::get('rowid');
        $qty = Input::get('qty');        

        foreach ($rowid as $i => $r) {
            if($qty[$i] <= 0){
                Session::flash('messageError','Minimal pembelian barang adalah 1');
                return redirect('/administrator/input-penjualan');
            }

            Cart::update($r, ['qty' => $qty[$i]]);
        }

        Session::flash('message', 'Update Berhasil');
        return redirect('/administrator/input-penjualan');       
    }


    public function postLaporanDownload(){
        $jenisLaporan = Input::get('jenis');
        $jenisPenjualan = Input::get('penjualan');
        $tahun = Input::get('tahun');
        $bulan = Input::get('bulan');
        $data = null;
        $form = [
            'jenis'=>Input::get('jenis'),
            'penjualan'=>Input::get('penjualan'),
            'tahun'=>Input::get('jenis'),
            'bulan'=>Input::get('bulan')
        ];
        switch ($jenisLaporan) {
            case 'penjualan_semua':
                switch ($jenisPenjualan) {
                    case 'semua':
                        $fileName = 'Laporan-Penjualan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel) use ($tahun, $bulan){

                                $excel->sheet('Sheetname', function($sheet) use ($tahun, $bulan){
                                    $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan','pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                                        ->where(function($query) use ($tahun, $bulan){
                                            $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                            ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
                                        })->get();

                                    $sheet->fromArray($data);
                                });

                        })->export('xls');
                        break;
                    case 'offline':
                        $fileName = 'Laporan-Penjualan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel)  use ($tahun, $bulan){

                                $excel->sheet('Sheetname', function($sheet) use ($tahun, $bulan) {
                                   $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                                        ->where(function($query) use ($tahun, $bulan){
                                            $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                            ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                            ->where(function($query2){
                                                $query2->where('jenis_transaksi','=', 'TOF')
                                                       ->orWhere('jenis_transaksi' ,'=', 'KOF');
                                            });
                                        })->get();

                                    $sheet->fromArray($data);

                                });

                        })->export('xls');
                    break;
                    case 'online':
                        $fileName = 'Laporan-Penjualan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel) use ($tahun, $bulan){

                                $excel->sheet('Sheetname', function($sheet) use ($tahun, $bulan){
                                    
                                    $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                                        ->where(function($query) use ($tahun, $bulan){
                                            $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                            ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                            ->where(function($query2){
                                                $query2->where('jenis_transaksi','=', 'TON')
                                                       ->orWhere('jenis_transaksi' ,'=', 'KON');
                                            });
                                        })->get();
                                    $sheet->fromArray($data);

                                });

                        })->export('xls');
                    break;
                    default:
                        $fileName = 'Laporan-Penjualan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel) use ($tahun, $bulan){

                                $excel->sheet('Sheetname', function($sheet) use ($tahun, $bulan) {
                                    $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                                        ->where(function($query) use ($tahun, $bulan){
                                            $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                            ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
                                        })->get();
                                    $sheet->fromArray($data);
                                });
                        })->export('xls');
                        break;
                }
                break;
            case 'penjualan_tunai':
                switch ($jenisPenjualan) {
                    case 'semua':
                        $fileName = 'Laporan-Penjualan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel) use ($tahun, $bulan){
                                $excel->sheet('Sheetname', function($sheet) use ($tahun, $bulan){
                                    $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                                        ->where(function($query) use ($tahun, $bulan){
                                            $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                            ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                            ->where(function($query2){
                                                $query2->where('jenis_transaksi','=', 'TON')
                                                       ->orWhere('jenis_transaksi' ,'=', 'TOF');
                                            });
                                        })->get();
                                    $sheet->fromArray($data);

                                });

                        })->export('xls');
                        break;
                    case 'offline':
                        $fileName = 'Laporan-Penjualan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel) use ($tahun, $bulan) {

                                $excel->sheet('Sheetname', function($sheet) use ($tahun, $bulan) {
                                   $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                                        ->where(function($query) use ($tahun, $bulan){
                                            $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                            ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                            ->where('jenis_transaksi','=', 'TOF');
                                        })->get();

                                    $sheet->fromArray($data);

                                });

                        })->export('xls');
                    break;
                    case 'online':
                        
                        $fileName = 'Laporan-Penjualan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel)use ($tahun, $bulan) {

                                $excel->sheet('Sheetname', function($sheet)use ($tahun, $bulan) {
                                    $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                                        ->where(function($query) use ($tahun, $bulan){
                                            $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                            ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                            ->where('jenis_transaksi','=', 'TON');
                                        })->get();

                                    $sheet->fromArray($data);

                                });

                        })->export('xls');
                    break;
                    default:
                        $fileName = 'Laporan-Penjualan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel) use ($tahun, $bulan){

                                $excel->sheet('Sheetname', function($sheet) use ($tahun, $bulan) {
                                    $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                                        ->where(function($query) use ($tahun, $bulan){
                                            $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                            ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                            ->where(function($query2){
                                                $query2->where('jenis_transaksi','=', 'TON')
                                                       ->orWhere('jenis_transaksi' ,'=', 'TOF');
                                            });
                                        })->get();

                                    $sheet->fromArray($data);

                                });

                        })->export('xls');
                        break;
                }
                break;
            case 'penjualan_kredit':
                switch ($jenisPenjualan) {
                    case 'semua':
                        $fileName = 'Laporan-Penjualan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel)use ($tahun, $bulan) {
                                $excel->sheet('Sheetname', function($sheet)use ($tahun, $bulan) {
                                    $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                                        ->where(function($query) use ($tahun, $bulan){
                                            $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                            ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                            ->where(function($query2){
                                                $query2->where('jenis_transaksi','=', 'KOF')
                                                       ->orWhere('jenis_transaksi' ,'=', 'KON');
                                            });
                                        })->get();
                                    $sheet->fromArray($data);
                                });

                        })->export('xls');
                        break;
                    case 'offline':
                        $fileName = 'Laporan-Penjualan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel) use ($tahun, $bulan){
                                $excel->sheet('Sheetname', function($sheet) use ($tahun, $bulan){
                                    $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                                        ->where(function($query) use ($tahun, $bulan){
                                            $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                            ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                            ->where('jenis_transaksi','=', 'KOF');
                                        })->get();

                                    $sheet->fromArray($data);

                                });

                        })->export('xls');
                    break;
                    case 'online':
                        
                        
                        $fileName = 'Laporan-Penjualan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel) use ($tahun, $bulan){

                                $excel->sheet('Sheetname', function($sheet) use ($tahun, $bulan){
                                    $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan','pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                                        ->where(function($query) use ($tahun, $bulan){
                                            $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                            ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                            ->where('jenis_transaksi','=', 'KON');
                                        })->get();

                                    $sheet->fromArray($data);

                                });

                        })->export('xls');
                    break;
                    default:
                        
                        $fileName = 'Laporan-Penjualan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel) use ($tahun, $bulan){

                                $excel->sheet('Sheetname', function($sheet)use ($tahun, $bulan) {
                                    $data = Pesanan::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.jenis_transaksi as Jenis_Transaksi', 'pesanan.kode_pesanan as Kode_Pesanan', 'pesanan.total_pembayaran as Total_Pembayaran', 'pesanan.verifikasi as Status', 'pesanan.customer_id as Kode_Admin')
                                        ->where(function($query) use ($tahun, $bulan){
                                            $query->where('tanggal_pesanan', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                            ->where('tanggal_pesanan', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                            ->where(function($query2){
                                                $query2->where('jenis_transaksi','=', 'KOF')
                                                       ->orWhere('jenis_transaksi' ,'=', 'KON');
                                            });
                                        })->get();

                                    $sheet->fromArray($data);

                                });

                        })->export('xls');
                        break;
                }
                break;
            case 'penerimaan':
                switch ($jenisPenjualan) {
                    case 'semua':
                        $fileName = 'Laporan-Penerimaan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel)use ($tahun, $bulan) {

                                $excel->sheet('Sheetname', function($sheet)use ($tahun, $bulan) {
                                    $data = Detail_Pembayaran::select('detail_pembayaran.tanggal_pembayaran as Tanggal', 'pesanan.kode_pesanan as Kode_Pesanan', 'detail_pembayaran.pembayaran as Pembayaran','pesanan.customer_id as Kode_Admin', 'pesanan.jenis_transaksi as Jenis_Transaksi')
                                        ->where(function($query) use ($tahun, $bulan){
                                            $query->where('detail_pembayaran.tanggal_pembayaran', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                            ->where('detail_pembayaran.tanggal_pembayaran', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
                                        })->join('pembayaran', 'pembayaran.id', '=', 'detail_pembayaran.pembayaran_id')
                                        ->join('pesanan', 'pesanan.id', '=','pembayaran.pesanan_id')
                                        ->get();

                                    $sheet->fromArray($data);

                                });

                        })->export('xls');
                        break;
                    case 'online':
                        $fileName = 'Laporan-Penerimaan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel) use ($tahun, $bulan){

                                $excel->sheet('Sheetname', function($sheet)use ($tahun, $bulan) {
                                    $data = Detail_Pembayaran::select('detail_pembayaran.tanggal_pembayaran as Tanggal', 'pesanan.kode_pesanan as Kode_Pesanan', 'detail_pembayaran.pembayaran as Pembayaran','pesanan.customer_id as Kode_Admin', 'pesanan.jenis_transaksi as Jenis_Transaksi')
                                    ->where(function($query) use ($tahun, $bulan){
                                        $query->where('tanggal_pembayaran', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                        ->where('tanggal_pembayaran', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                        ->where(function($query2){
                                            $query2->where('jenis_transaksi','=', 'TON')
                                                   ->orWhere('jenis_transaksi' ,'=', 'KON');
                                        });
                                    })->join('pembayaran', 'pembayaran.id', '=', 'detail_pembayaran.pembayaran_id')
                                    ->join('pesanan', 'pesanan.id', '=','pembayaran.pesanan_id')
                                    ->get();

                                    $sheet->fromArray($data);

                                });

                        })->export('xls');
                    break;
                    case 'offline':
                        
                        $fileName = 'Laporan-Penerimaan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel)use ($tahun, $bulan) {

                                $excel->sheet('Sheetname', function($sheet)use ($tahun, $bulan) {
                                    
                                    $data = Detail_Pembayaran::select('detail_pembayaran.tanggal_pembayaran as Tanggal', 'pesanan.kode_pesanan as Kode_Pesanan', 'detail_pembayaran.pembayaran as Pembayaran','pesanan.customer_id as Kode_Admin', 'pesanan.jenis_transaksi as Jenis_Transaksi')
                                        ->where(function($query) use ($tahun, $bulan){
                                            $query->where('tanggal_pembayaran', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                            ->where('tanggal_pembayaran', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                            ->where(function($query2){
                                                $query2->where('jenis_transaksi','=', 'TOF')
                                                       ->orWhere('jenis_transaksi' ,'=', 'KOF');
                                            });
                                        })->join('pembayaran', 'pembayaran.id', '=', 'detail_pembayaran.pembayaran_id')
                                        ->join('pesanan', 'pesanan.id', '=','pembayaran.pesanan_id')
                                        ->get();

                                    $sheet->fromArray($data);

                                });

                        })->export('xls');
                    break;
                    default:
                        
                        $fileName = 'Laporan-Penerimaan-'.Carbon::now()->format('d-m-Y');
                        return Excel::create($fileName, function($excel) use ($tahun, $bulan){

                                $excel->sheet('Sheetname', function($sheet) use ($tahun, $bulan){
                                    $data = Detail_Pembayaran::select('detail_pembayaran.tanggal_pembayaran as Tanggal', 'pesanan.kode_pesanan as Kode_Pesanan', 'detail_pembayaran.pembayaran as Pembayaran','pesanan.customer_id as Kode_Admin', 'pesanan.jenis_transaksi as Jenis_Transaksi')
                                        ->where(function($query) use ($tahun, $bulan){
                                            $query->where('tanggal_pembayaran', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                            ->where('tanggal_pembayaran', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
                                        })->join('pembayaran', 'pembayaran.id', '=', 'detail_pembayaran.pembayaran_id')
                                        ->join('pesanan', 'pesanan.id', '=','pembayaran.pesanan_id')
                                        ->get();

                                    $sheet->fromArray($data);

                                });

                        })->export('xls');
                        break;
                }
                break;
            case 'piutang':
                
                $fileName = 'Laporan-Piutang-'.Carbon::now()->format('d-m-Y');
                return Excel::create($fileName, function($excel) {

                        $excel->sheet('Sheetname', function($sheet) {
                            $data = Pembayaran::select('pesanan.tanggal_pesanan as Tanggal', 'pesanan.kode_pesanan as Kode_Pesanan', 'pembayaran.sisa_pembayaran as Sisa_Pembayaran','pesanan.customer_id as Kode_Admin')
                                ->where('sisa_pembayaran','>',0)
                                ->join('pesanan', 'pesanan.id', '=','pembayaran.pesanan_id')
                                ->get();

                            $sheet->fromArray($data);

                        });

                    })->export('xls');
            case 'persediaan':

            $fileName = 'Laporan-Barang-'.Carbon::now()->format('d-m-Y');
            return Excel::create($fileName, function($excel) {

                    $excel->sheet('Sheetname', function($sheet) {
                        $data = LaporanBarang::select('barang.kode_barang as Kode_Barang','barang.nama as Nama_Barang', 'barang.harga_grosir as Harga_Pokok','barang.harga as Harga', 'barang.harga_grosir as Harga_Grosir', 'barang.stok as Stok', 'barang.berat as Berat', 'kategori.nama as Kategori', 'laporan_barang.updated_at as Last_Update')
                            ->where('bulan', $bulan)
                            ->where('tahun', $tahun)
                            ->join('barang', 'barang.id','=','laporan_barang.id_barang')
                            ->join('kategori', 'kategori.id', '=', 'barang.kategori_id')
                            ->orderBy('Last_Update','DSC')
                            ->get();

                        $sheet->fromArray($data);

                    });

                })->export('xls');
            case 'beban':

                $fileName = 'Laporan-Beban-'.Carbon::now()->format('d-m-Y');
                return Excel::create($fileName, function($excel) use ($tahun, $bulan) {

                        $excel->sheet('Sheetname', function($sheet) use ($tahun, $bulan){
                            $data = Beban::select('beban.harga_beban as Beban','beban.jumlah_barang as Jumlah_Barang','pesanan.kode_pesanan as Kode_Pesanan','pesanan.tanggal_pesanan as Tanggal')
                                ->where(function($query) use ($tahun, $bulan){
                                    $query->where('beban.created_at', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                                    ->where('beban.created_at', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                                    ->where('beban.harga_beban','>',0);
                                })->join('pesanan','pesanan.id','=','beban.pesanan_id')->get();

                            $sheet->fromArray($data);

                        });

                    })->export('xls');
                
        }

        return redirect('/administrator/laporan');
    }

    
}