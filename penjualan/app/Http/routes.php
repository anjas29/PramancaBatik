<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@getIndex');


//For Auth
Route::get('/login', 'Auth\AuthController@getLogin');
Route::post('/login', 'HomeController@postLogin');

Route::get('/register', 'HomeController@getRegisterPelanggan');
Route::post('/register', 'HomeController@registerPelanggan');

Route::get('/logout', 'HomeController@getLogout');

Route::get('/loginAdmin', 'AdminController@getLogin');
Route::post('/loginAdmin', 'AdminController@postLogin');
Route::get('/logoutAdmin', 'AdminController@getLogout');
Route::get('/firstLogin', 'AdminController@getFirstLogin');

Route::group(array('prefix'=>'/'), function(){
    Route::get('index', 'HomeController@getIndex');

    Route::get('category', 'HomeController@category');
    Route::get('category/{category}', 'HomeController@specificCategory');
    Route::get('detail/{id}', "HomeController@detailBarang");

    Route::get('cart', 'HomeController@getCart');
    Route::post('cart/add', 'HomeController@postAddCart');
    Route::post('cart/update', 'HomeController@postUpdateCart');
    Route::post('cart/delete', 'HomeController@postDeleteItemCart');

    Route::get('loginCashier', 'HomeController@getLoginCashier');
    Route::post('loginCashier', 'HomeController@postLoginCashier');
    Route::get('guest', 'HomeController@getGuest');
    Route::post('guest', 'HomeController@postGuest');

    Route::get('checkPesanan', 'HomeController@getCheckPesanan');
    Route::post('checkPesanan', 'HomeController@postCheckPesanan');

    Route::get('payment/step1', 'HomeController@getPayment1');
    Route::post('payment/step1', 'HomeController@postPayment1');

    Route::get('payment/step2', 'HomeController@getPayment2');
    Route::post('payment/step2', 'HomeController@postPayment2');

    Route::get('payment/step3', 'HomeController@getPayment3');
    Route::post('payment/step3', 'HomeController@postPayment3');
    Route::get('payment/success', 'HomeController@getPaymentSuccess');

    Route::get('order', 'HomeController@getOrders');
    Route::get('order/{id}', 'HomeController@getDetailOrder');
    Route::get('profil', 'HomeController@getProfil');
    Route::post('verifikasi', 'HomeController@postVerifikasi');

    Route::get('tagihan', 'HomeController@getTagihan');
    Route::get('tagihan/{id}', 'HomeController@getDetailTagihan');

    Route::get('settings', 'HomeController@getSettings');
    Route::post('settings', 'HomeController@postSettings');
    Route::post('settingPassword', 'HomeController@postSettingPassword');

    Route::post('/verifPesanan', 'HomeController@postVerifPesanan');
     
});

Route::group(array('prefix'=>'/administrator'), function(){
    Route::post('ubahStatusPesanan', 'AdminController@postUbahStatusPesanan');
    Route::get('detailPesanan/{id}', 'AdminController@getDetailPesanan');
    Route::get('cetakDetailPesanan/{id}', 'AdminController@getCetakDetailPesanan');
});

Route::post('/register-pelanggan', 'HomeController@registerPelanggan');

Route::group(array('middleware'=>'role:Pelanggan'),function(){
    Route::controller('pelanggan', 'PelangganController');
});

Route::group(array('middleware'=>'auth:admin'),function(){
    Route::controller('administrator','AdminController');
});

Route::get('/img/{filename}', function ($filename)
{
    $path = storage_path() . '/app/img/' . $filename;

    if(!File::exists($path)) return abort('400');

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});
Route::get('/images/{filename}', function ($filename)
{
    $path = storage_path() . '/app/img/' . $filename;

    if(!File::exists($path)) return abort('400');

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/imgProduct/{filename}', function ($filename)
{
    $path = storage_path() . '/app/produk/' . $filename;

    if(!File::exists($path)) return abort('400');

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/imgUser/{filename}', function ($filename)
{
    $path = storage_path() . '/app/user/' . $filename;

    if(!File::exists($path)) return abort('404');

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/imgPembayaran/{filename}', function ($filename)
{
    $path = storage_path() . '/app/buktiPembayaran/' . $filename;

    if(!File::exists($path)) return abort('400');

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

