<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $table = 'pembayaran';

    protected $fillable = array('pesanan_id', 'cicilan_perbulan', 'sisa_pembayaran', 'tanggal_jatuh_tempo', 'status_pembayaran');
    
    public $timestamps = false;

    public function detail_pembayaran(){
    	return $this->hasMany('App\Detail_Pembayaran', 'pembayaran_id');
    }

    public function getNotifAttribute(){
    	return $this->hasMany('App\Detail_Pembayaran', 'pembayaran_id')->where('verifikasi',0)->count();
    }

    public function pesanan(){
    	return $this->belongsTo('App\Pesanan', 'pesanan_id');
    }


}


