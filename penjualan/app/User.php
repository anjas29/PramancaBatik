<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Role;


class User extends Authenticatable
{
    
    protected $table = 'users';

    protected $fillable = [
        'nama', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles(){
        return $this->belongsToMany('App\Role');
    }

    public function assignRole($role){
        if(is_string($role)){
            $role = Role::where('name', $role)->first();
        }

        return $this->roles()->attach($role);
    }

    public function revokeRole($role){
        if(is_string($role)){
            $role = Role::where('name', $role)->first();
        }

        return $this->roles()->detach($role);
    }

    public function hasRole($name){
        foreach ($this->roles as $role) {
            if($role->name === $name) return true;
    }

        return false;
    }

    public function pelanggan(){
        return $this->hasOne('App\Pelanggan');
    }
}
