<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogAktifitas extends Model
{
    protected $table = 'log_aktivitas';

    protected $fillable = array('customer_service_id', 'aktivitas');

    public function customer_service(){
    	return $this->belongsTo('App\CustomerService', 'customer_service_id');
    }
}
