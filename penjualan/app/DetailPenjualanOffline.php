<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPenjualanOffline extends Model
{
    protected $table = 'detail_penjualan_offline';

    public $timestamps = false;

    public function penjualan(){
    	return $this->belongsTo('App\PenjualanOffline');
    }
}
