<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class CustomerService extends Authenticatable
{
	 protected $table = 'customer_service';

	 protected $fillable = array('alamat', 'no_telepon', 'user_id');

	 public function log_aktivitas(){
	 	return $this->hasMany('App\LogAktifitas', 'customer_service_id');
	 }

	 public function pesanan(){
	 	return $this->hasMany('App\Pesanan', 'customer_id');
	 }

	 public function user(){
	 	return $this->belongsTo('App\User');
	 }

}
