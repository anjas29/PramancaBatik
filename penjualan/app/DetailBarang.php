<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailBarang extends Model
{
	protected $table = 'detail_barang';

	protected $fillable = array('barang_id', 'size', 'stok');

	public function barang(){
		return $this->belongsTo('App\Barang','barang_id');
	}

	public function detail_pesanan(){
		return $this->hasMany('App\Detail_Pesanan', 'detail_barang_id');
	}
}
