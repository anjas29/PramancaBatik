<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembayaranOffline extends Model
{
 	protected $table = 'pembayaran_pffline';

 	public function pesanan(){
 		return $this->belongsTo('App\PenjualanOffline', 'pesanan_id');
 	}

 	public function detail_pembayaran(){
 		return $this->hasMany('App\Detail_Pembayaran', 'pembayaran_id');
 	}
}
