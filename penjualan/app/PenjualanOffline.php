<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenjualanOffline extends Model
{
    protected $table = 'penjualan_offline';

    public $timestamps = false;

    public function detail(){
    	return $this->hasMany('App\DetailPenjualanOffline');
    }
    
    public function pelanggan(){
    	return $this->belongsTo('App\User');
    }

    public function beban(){
    	return $this->hasOne('App\Beban','pesanan_id');
    }
}
