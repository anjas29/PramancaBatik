<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beban extends Model
{
    protected $table = 'beban';

    protected $fillable = array('pesanan_id', 'berat', 'jumlah_barang');

    public function pesanan(){
    	return $this->belongsTo('App\Pesanan', 'pesanan_id');
    }
}
