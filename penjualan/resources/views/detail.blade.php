@extends('layouts.header')
@section('style')
    <style type="text/css">
        .thumbSmall {
          position: relative;
          width: 100%;
          height: 200px;
          overflow: hidden;
        }
        .thumbSmall img {
          position: absolute;
          left: 50%;
          top: 50%;
          height: 100%;
          width: auto;
          -webkit-transform: translate(-50%,-50%);
              -ms-transform: translate(-50%,-50%);
                  transform: translate(-50%,-50%);
        }
        .thumbSmall img.portrait {
          width: 100%;
          height: auto;
        }
    </style>
@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="/index">Beranda</a>
                </li>
                <li><a href="/category/{{ $select }}">{{ $select }}</a></li>
                <li>{{ $data->nama }}</li>
            </ul>
        </div>

        <div class="col-md-3">
            <!-- *** MENUS AND FILTERS ***
_________________________________________________________ -->
            <div class="panel panel-default sidebar-menu">

                <div class="panel-heading">
                    <h3 class="panel-title">Kategori</h3>
                </div>

                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked category-menu">
                        @foreach($category as $c)
                            @if (strcasecmp($c->nama,$select) == 0)
                                <li class="active">
                                    <a href="/category/{{ $c->nama }}">{{ $c->nama }}  <span class="badge pull-right">{{ $c->barang->count() }}</span></a>
                                </li>
                            @else
                                <li>
                                    <a href="/category/{{ $c->nama }}">{{ $c->nama }}  <span class="badge pull-right">{{ $c->barang->count() }}</span></a>
                                </li>
                            @endif
                        @endforeach
                    </ul>

                </div>
            </div>
            <!-- *** MENUS AND FILTERS END *** -->
        </div>

        <div class="col-md-9">
            <div class="row" id="productMain">
                <div class="col-sm-6">
                    <div id="mainImage">
                        <img src="/imgProduct/{{ $data->foto }}" alt="" class="img-responsive">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box">
                        <h1 class="text-center">{{ $data->nama }}
                            <p class="price">Rp {{ number_format($data->harga, 0, ',','.') }}</p>
                        </h1>
                        

                        <p class="text-center buttons">
                            <button class='btn btn-primary' data-toggle='modal' data-target='#keranjang'><i class="fa fa-shopping-cart"></i> Masukan Keranjang Belanja</button> 
                            <a href="/category/{{ $select }}" class="btn btn-default"><i class="fa fa-heart"></i> Lihat Lainnya</a>
                        </p>
                    </div>

                    <div class="row" id="thumbs">
                        <div class="col-xs-4">
                            <a href="/imgProduct/{{ $data->foto }}" class="thumb">
                                <img src="/imgProduct/{{ $data->foto }}" alt="" class="img-responsive">
                            </a>
                        </div>
                    </div>
                </div>

            </div>


            <div class="box" id="details">
                <p>
                    <h4>Keterangan</h4>
                    @if($data->keterangan == '')    
                        <p>Tidak ada keterangan</p>
                    @else
                        <p>{{$data->keterangan}}</p>
                    @endif
                    <h4>Filosofi</h4>
                    <p>{{$data->filosofi}}</p>
                    <h4>Bahan</h4>
                    <p>{{$data->bahan}}</p>
                    <h4>Ukuran</h4>
                    <p>{{$data->ukuran}}</p>
                    <hr>
                    <div class="social">
                        <h4>Show it to your friends</h4>
                        <p>
                            <a href="#" class="external facebook" data-animate-hover="pulse"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="external gplus" data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="external twitter" data-animate-hover="pulse"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="external instagram" data-animate-hover="pulse"><i class="fa fa-instagram"></i></a>
                        </p>
                    </div>
                </p>
            </div> 
        </div>
    <!-- /.container -->
</div>
<div class="modal fade" id="keranjang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <form method="post" action="/cart/add" class="form-horizontal">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Belanja</h4>
        </div>
        <div class="modal-body">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <div class="thumbSmall">
                    <a href="#">
                        <img src="/imgProduct/{{ $data->foto }}" alt="" class="img-responsive">
                    </a>
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <div class="form-group">
                    <label for="nmprdk" class="col-sm-2 control-label">Nama</label>    
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name='nama' disabled value='{{ $data->nama }}'>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nmprdk" class="col-sm-2 control-label">Harga</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name='harga' disabled value='{{ $data->harga }}'>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nmprdk" class="col-sm-2 control-label">Jumlah</label>    
                    <div class="col-sm-3">
                      <input type="number" class="form-control" name='jumlah' value='1'>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" value="{{ $data->id }}" name="id">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
@endsection
