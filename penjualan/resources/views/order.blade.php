@extends('layouts.header')
@section('style')

@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="/index">Beranda</a>
                </li>
                <li><a href="/order">Pesanan</a>
                </li>
                <li>{{ $data->kode_pesanan }}</li>
            </ul>

        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU ***
_________________________________________________________ -->
            <div class="panel panel-default sidebar-menu">

                <div class="panel-heading">
                    <h3 class="panel-title">Pelanggan </h3>
                </div>

                <div class="panel-body">

                    <ul class="nav nav-pills nav-stacked">
                        <li class="active">
                            <a href="/order"><i class="fa fa-list"></i> Pesanan</a>
                        </li>
                        <li>
                            <a href="/tagihan"><i class="fa fa-heart"></i> Tagihan</a>
                        </li>
                        <li>
                            <a href="/profil"><i class="fa fa-user"></i> Profil</a>
                        </li>
                        <li>
                            <a href="/logout"><i class="fa fa-sign-out"></i> Logout</a>
                        </li>
                    </ul>
                </div>

            </div>
            <!-- /.col-md-3 -->

            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-9" id="customer-order">
            <div class="box">
                <h2>
                    {{$data->kode_pesanan}}
                    @if($data->verifikasi == 'Dalam Pengiriman')
                        <a id='verifikasi' class="btn btn-success btn-sm pull-right" data-kode='{{$data->kode_pesanan}}' data-id='{{$data->id}}'>Pesanan sudah diterima?</a>
                    @elseif($data->verifikasi == 'Diterima')
                        <span class="label btn-primary btn-sm pull-right">Diterima</span>
                    @elseif($check)
                        <button type="button" data-target='#inputKonfirmasi' data-toggle='modal' class="btn btn-danger btn-sm pull-right">Konfirmasi Pembayaran</button> 
                    @else
                        <span class="label label-info btn-sm pull-right">Menunggu Konfirmasi</span>
                    @endif
                </h2>

                <p class="text-muted">
                    Pesanan <strong>{{$data->kode_pesanan}}</strong> pada tanggal <strong>{{$data->tanggal_pesanan}}</strong>.
                    <br> Kode : <strong>{{$data->kode_pesanan_2}}</strong>
                    <br> Status :  <strong style='color:#00796B;'>{{$data->verifikasi}}</strong>
                </p>
                <div class="row ">
                    <div class="col-md-6">
                        <h2>Penerima</h2>
                            Nama <strong>{{$data->pelanggan->nama}}</strong>
                            <br>Email <strong>{{$data->pelanggan->email}}</strong>
                            <br>Tujuan <strong>{{$data->alamat}}</strong>
                            </p>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th colspan="2">Barang</th>
                                <th>Jumlah</th>
                                <th>Harga Satuan</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data->detail_pesanan as $d)
                            <tr>
                                <td>
                                    <a href="/detail/{{$d->barang->id}}">
                                        <img src="/imgProduct/{{ $d->barang->foto }}" alt="Foto Gambar">
                                    </a>
                                </td>
                                <td><a href="/detail/{{$d->barang->id}}">{{ $d->barang->nama }}</a>
                                </td>
                                <td>{{ $d->jumlah }}</td>
                                <td>Rp {{ number_format($d->harga, 0, ',','.') }}</td>
                                <?php $subtotal = $d->harga * $d->jumlah ?>
                                <td>Rp {{ number_format($subtotal, 0, ',','.') }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4" class="text-right">Total Belanja</th>
                                <td>Rp {{ number_format($data->total_pembayaran, 0, ',','.') }}</td>
                            </tr>
                            <tr>
                                <th colspan="4" class="text-right">Biaya Ekspedisi</th>
                                <td>Rp {{ number_format($data->beban->harga_beban, 0, ',','.') }}</td>
                            </tr>
                            <tr>
                                <th colspan="4" class="text-right">Total</th>
                                <?php $total = $data->beban->harga_beban + $data->total_pembayaran;?>
                                <td>Rp {{ number_format($total, 0, ',','.') }}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
        </div>

    </div>
    <!-- /.container -->
    <div class="modal fade" id="inputKonfirmasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <form method="post" action="/verifPesanan" class="form-horizontal" enctype="multipart/form-data">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Konfirmasi Pembayaran</h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label class="col-sm-3 control-label">Bukti Pembayaran</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="file" name="bukti_pembayaran" class="form-control">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" value="{{ $data->id }}" name="id">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Lanjutkan</button>
        </div>
        </div>
        </form>
      </div>
    </div>
</div>
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
@if($data->verifikasi == 'Dalam Pengiriman')
<script type="text/javascript">
    $('#verifikasi').click(function() {
        var rowid = $(this).data('id');
        var kode = $(this).data('kode');

        bootbox.confirm("Apakah pesanan <b>"+kode+"</b> sudah diterima?", function(result) {
            if (result) {
                toastr.options.timeOut = 0;
                toastr.options.extendedTimeOut = 0;
                toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Sedang menghapus...');
                toastr.options.timeOut = 5000;
                toastr.options.extendedTimeOut = 1000;
                $.post("/verifikasi", {rowid: rowid})
                .done(function(result) {
                    toastr.clear();
                    window.location.replace("/order");
                })
                .fail(function(result) {
                    toastr.clear();
                    toastr.error('Kesalahan server! Silahkan reload halaman dan coba lagi');
                });
            };
        }); 
    });
</script>
@endif
@stop