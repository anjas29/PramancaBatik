@extends('layouts.header')
@section('style')
	<meta name="_token" content="{{ csrf_token() }}">
@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="/index">Home</a>
                </li>
                <li>Registrasi</li>
            </ul>

        </div>

        <div class="row" id="error-page">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="box">

                    <h4 class="text-muted">Pramanca Batik</h4>
                    <h2 >Registrasi berhasil</h2>

                    <p class="text-center">Anda telah <strong>resmi terdaftar</strong> sebagai pelanggan.<br>Silahkan lanjutkan proses <a href="/login"><strong>Log In</strong></a> untuk mempermudah penggunaan layanan jual beli di Pramanca Batik.</p>

                    <p class="buttons"><a href="/login" class="btn btn-primary"><i class="fa fa-sign-in"></i> Log In</a>
                        </p>
                </div>
            </div>
        </div>
    <!-- /.container -->
</div>
<!-- /#content -->
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
@stop