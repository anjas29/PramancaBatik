@extends('layouts.header')
@section('style')
	<meta name="_token" content="{{ csrf_token() }}">
@stop
@section('content')
<?php $total = Cart::total(); ?>
<div id="content">
    <div class="container">

        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="/index">Beranda</a>
                </li>
                <li>Pembayaran - Metode</li>
            </ul>
        </div>

        <div class="col-md-9" id="checkout">

            <div class="box">
                <form method="post" action="/payment/step2">
                    <h1>Pembayaran</h1>
                    <ul class="nav nav-pills nav-justified">
                        <li><a href="/payment/step1"><i class="fa fa-map-marker"></i><br>Alamat</a>
                        </li>
                        <li class="active"><a href="/payment/step2"><i class="fa fa-truck"></i><br>Cara Pembayaran</a>
                        </li>
                        <li class="disabled"><a href="#"><i class="fa fa-money"></i><br>Ringkasan</a>
                        </li>
                    </ul>

                    <div class="content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="box payment-method">

                                    <h4>Tunai</h4>

                                    <p>Pembayaran Tunai</p>

                                    <div class="box-footer text-center">

                                        <input type="radio" name="metode_pembayaran" value="tunai">
                                    </div>
                                </div>
                            </div>
                            @if($total >= 2000000 AND auth('pelanggan')->check())
                            <div class="col-sm-6">
                                <div class="box payment-method">

                                    <h4>Kredit</h4>

                                    <p>Pembayaran Kredit dengan 50% Uang muka. Hanya berlaku untuk pembelian diatas Rp 2.000.00,-</p>

                                    <div class="box-footer text-center">

                                        <input type="radio" name="metode_pembayaran" value="kredit">
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="box-footer">
                        <div class="pull-left">
                            <a href="/payment1" class="btn btn-default"><i class="fa fa-chevron-left"></i>Edit Alamat</a>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary">Selanjutnya<i class="fa fa-chevron-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box -->


        </div>
        <!-- /.col-md-9 -->

        <div class="col-md-3">
            <div class="box" id="order-summary">
                <div class="box-header">
                    <h3>Ringkasan</h3>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>Total Belanja</td>
                                <th>{{ $total }}</th>
                            </tr>
                            <tr>
                                <td>Biaya Ekspedisi</td>
                                <th>{{ Session::get('temp_beban') }}</th>
                            </tr>
                            <tr>
                                <td>Biaya Unik</td>
                                <th>{{ Session::get('temp_kode_unik')}}</th>
                            </tr>
                            <tr class="total">
                                <td>Total</td>
                                <th>{{ $total + Session::get('temp_beban') + Session::get('temp_kode_unik') }}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>

        </div>
        <!-- /.col-md-3 -->

    </div>
</div>
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
@stop