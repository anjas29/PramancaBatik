@extends('layouts.header')
@section('style')

@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href='/index'>Beranda</a>
                </li>
                <li>Pesanan</li>
            </ul>

        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU ***
_________________________________________________________ -->
            <div class="panel panel-default sidebar-menu">

                <div class="panel-heading">
                    <h3 class="panel-title">Pelanggan</h3>
                </div>

                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active">
                            <a href="/order"><i class="fa fa-list"></i> Pesanan</a>
                        </li>
                        <li>
                            <a href="/tagihan"><i class="fa fa-heart"></i> Tagihan</a>
                        </li>
                        <li>
                            <a href="/profil"><i class="fa fa-user"></i> Profil</a>
                        </li>
                        <li>
                            <a href="/logout"><i class="fa fa-sign-out"></i> Logout</a>
                        </li>
                    </ul>
                </div>

            </div>
            <!-- /.col-md-3 -->

            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-9" id="customer-orders">
            <div class="box">
                <h1>Pesanan</h1>

                <p class="text-muted">Jika terjadi kesalahan pemesanan <a href="#">contact us</a>, Pesanan akan dilayani 24/7.</p>

                <hr>
                @if(count($data) != 0)
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Kode Pesanan</th>
                                <th>Jenis Transaksi</th>
                                <th>Tanggal</th>
                                <th>Total</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $d)
                            <tr>
                                <th>{{ $d->kode_pesanan }}</th>
                                <td>{{ $d->jenis_transaksi }}</td>
                                <td>{{ $d->tanggal_pesanan }}</td>
                                <td>Rp {{ number_format($d->totalBayar, 0, ',','.') }}</td>
                                <td>
                                    @if($d->verifikasi == 'Diterima')
                                        <span class="label label-success">Diterima</span>
                                    @elseif($d->verifikasi == 'Konfirmasi Pembayaran')
                                        <span class="label label-warning">Konfirmasi Pembayaran</span>
                                    @elseif($d->verifikasi == 'Lunas Uang Muka')
                                        <span class="label label-info">Lunas Uang Muka</span>
                                    @elseif($d->verifikasi == 'Dalam Pengiriman')
                                        <span class="label label-primary">Dalam Pengiriman</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="/order/{{ $d->id }}" class="btn btn-primary btn-sm">Lihat</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                    <h3>Tidak ada Pesanan</h3>
                @endif
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
@stop