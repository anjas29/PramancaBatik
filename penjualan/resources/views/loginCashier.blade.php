@extends('layouts.header')
@section('style')
	<meta name="_token" content="{{ csrf_token() }}">
@stop
@section('content')
<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <ul class="breadcrumb">
                    <li><a href="/index">Home</a>
                    </li>
                    <li>Log in</li>
                </ul>

            </div>
        </div>
        <div class="row box">
            <div class="col-md-6">
                <h3>Login</h3>

                <form action="/loginCashier" method="post">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in"></i> Log in</button>
                    </div>
                </form>
                <br>
                <p class="lead" align="center">Belum punya akun? <a href="/register">Daftar disini</a></p>
            </div>
            <div class="col-md-6">
                <h3>Tamu</h3>
                <div class="text-center">
                    <p>
                        Lanjutkan pembeliaan sebagai tamu. Anda tetap wajib mengisi identitas dan data diri lainnya.
                    </p>
                    <a href="/guest" class="btn btn-primary">Lanjutkan</a>
                </div>
            </div>
        </div>
        
    </div>
    <!-- /.container -->
</div>
<!-- /#content -->
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
@stop