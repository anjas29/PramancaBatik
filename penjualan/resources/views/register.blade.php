@extends('layouts.header')
@section('style')
	<meta name="_token" content="{{ csrf_token() }}">
@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="/index">Home</a>
                </li>
                <li>Register</li>
            </ul>

        </div>

        <div class="col-md-6 col-md-offset-3">
            <div class="box">
                <h1>Register</h1>
                <form action="/register" method="post" id='formRegister' data-toggle='validator'>
                <div class="form-group">
                        <label >Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" data-error='Data harus diisi' required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label >No Telepon</label>
                        <input type="text" class="form-control" name="telepon" data-error='Data harus diisi' required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label >Tempat Lahir</label>
                        <input type="text" class="form-control" name="tempat_lahir" data-error='Data harus diisi' required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label >Tanggal Lahir</label>
                        <input type="date" class="form-control" name="tanggal_lahir"  data-error='Data harus diisi' required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label >Kota</label>
                        <select name="kota" class="form-control" required>
                            @foreach($data as $d)
                            <option value="{{$d->city_id}}-{{$d->city_name}}">{{$d->city_name}}</option>
                            @endforeach
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label >Kode Pos</label>
                        <input type="text" class="form-control" name="kode_pos" data-error='Data harus diisi' required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea name="alamat" class="form-control"  data-error='Data harus diisi' required></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" data-error='Email tidak valid' required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id='password' name="password" data-minlength='8' required>
                        <div class="help-block with-errors">Minimal 8 huruf</div>
                    </div>
                    <div class="form-group">
                        <label for="password">Retype Password</label>
                        <input type="password" class="form-control" id='confirmPassword'name="confirmPassword" data-match="#password">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-user-md"></i> Register</button>
                    </div>
                </form>
                <br>
                <p class="lead" align="center">Sudah punya akun? <a href="/login">Login disini</a></p>
            </div>
        </div>


    </div>
    <!-- /.container -->
</div>
<!-- /#content -->
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
<script src="{{ asset('/js/validator.js') }}"></script>
@stop