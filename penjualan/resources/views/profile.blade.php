@extends('layouts.header')
@section('style')

@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li>
                    <a href="/index">Beranda</a>
                </li>
                <li>Profil</li>
            </ul>
        </div>
        <div class="col-md-3">
            <!-- *** CUSTOMER MENU ***
_________________________________________________________ -->
            <div class="panel panel-default sidebar-menu">

                <div class="panel-heading">
                    <h3 class="panel-title">Customer</h3>
                </div>

                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked">
                        <li>
                            <a href="/order"><i class="fa fa-list"></i> Pesanan</a>
                        </li>
                        <li>
                            <a href="/tagihan"><i class="fa fa-heart"></i> Tagihan</a>
                        </li>
                        <li class="active">
                            <a href="/profil"><i class="fa fa-user"></i> Profil</a>
                        </li>
                        <li>
                            <a href="/logout"><i class="fa fa-sign-out"></i> Logout</a>
                        </li>
                    </ul>
                </div>

            </div>
            <!-- /.col-md-3 -->

            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-9">
            <div class="box">
                <h1>
                    Profil
                    <a href="/settings" class="btn btn-sm btn-info pull-right">Edit Profil <i class="fa fa-plus"></i></a>
                </h1>

                <p class="lead">Data diri pelanggan</p>  
                <div class="row">
                    <div class='col-sm-4'>
                        <div>
                            @if(is_null($data->foto))
                            <img src="/imgUser/default.jpg" alt="" class="img-responsive">
                            @else
                            <img src="/imgUser/{{ $data->foto }}" alt="" class="img-responsive">
                            @endif
                        </div>
                    </div>
                    <div class='col-sm-8'>
                        <div class="form-group">
                            <strong>Nama</strong>
                            <br>{{ $data->nama }}
                        </div>
                        <div class="form-group">
                            <strong>Email</strong>
                            <br>{{ $data->email }}
                        </div>
                        <div class="form-group">
                            <strong>Kode Pos</strong>
                            <br>{{ $data->kode_pos }}
                        </div>
                        <div class="form-group">
                            <strong>Alamat</strong>
                            <br>{{ $data->alamat }}
                        </div>
                        <div class="form-group">
                            <strong>Telepon</strong>
                            <br>{{ $data->no_telepon }}
                        </div>
                        <div class="form-group">
                            <strong>Tempat Lahir</strong>
                            <br>{{ $data->tempat_lahir }}
                        </div>
                        <div class="form-group">
                            <strong>Tanggal Lahir</strong>
                            <br>{{ $data->tanggal_lahir }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
<!-- /#content -->
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
@stop