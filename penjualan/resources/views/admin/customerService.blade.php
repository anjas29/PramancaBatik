@extends('layouts.adminLayout')
@section('header')
  {{ Html::style('css/dataTables.bootstrap.css') }}
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar Admnistrator
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Administrator</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-info'>
            <div class="box-header">
              <i class="fa fa-list"></i>
              <h3 class="box-title">Daftar Administrator</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#inputCs" id="input-cs">Input Administrator <i class="fa fa-plus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Administrator</th>
                    <th>Nama Administrator</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>No. Telp</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->kode_admin }}</td>
                    <td>{{ $d->nama }}</td>
                    <td>{{ $d->email }}</td>
                    <td>{{ $d->alamat }}</td>
                    <td>{{ $d->no_telepon }}</td>
                  </tr>
                  <?php $c++; ?>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!--Input Barang Modal-->
  <div class="modal fade" id="inputCs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      
      <form action="/administrator/create-customer-service" method="post" class='form-horizontal' role='form' data-toggle='validator'>
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Input Customer Service</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Nama</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="input_nama" name='nama' required>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Email</label>
            <div class="col-sm-9">
              <input type="email" class="form-control" id="input_email" name='email' required>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Tempat Lahir</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="input_email" name='tempat_lahir' required>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Tanggal Lahir</label>
            <div class="col-sm-9">
              <input type="date" class="form-control" id="input_email" name='tanggal_lahir' required>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Alamat</label>
            <div class="col-sm-9">
              <textarea class="form-control " name="alamat" required></textarea>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">No Telp</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="input_no_telp" name='no_telp'required>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Password</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" id="input_password" name='password' data-minlength='6' required>
              <div class="help-block with-errors">Minimal 6 karakter</div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Kunci Pemilik</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" id="input_key" name='kunci_pemilik' required>
              <div class="help-block with-errors"></div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </div>
        </form>
        
      </div>
    </div>
  </div>
@stop
@section('js')
  <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
<script>
  $(document).ready(function() {
    $('.dataTable').dataTable();
  
  });
  $('#input-cs').click(function() {
      $('#input_nama').val("");
      $('#input_email').val("");
      $('#input_alamat').val("");
      $('#input_no_telp').val("");
      $('#input_password').val("");
      $('#input_key').val("");
    });
</script>
@stop