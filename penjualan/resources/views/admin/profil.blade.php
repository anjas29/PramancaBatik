@extends('layouts.adminLayout')
@section('header')
  {{ Html::style('css/dataTables.bootstrap.css') }}
  {{ Html::style('/css/bootstrap-editable.css') }}
@stop
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profil  {{$data->role}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Profil</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">

          <!-- Profile Image -->
          <div class="box box-primary">
                <div class="row">
                  <div class="col-md-10 col-md-offset-1">
                    <br>  
                      <a href="/administrator/setting" class="btn btn-sm btn-info pull-right">Edit Profil <i class="fa fa-plus"></i></a>
                  </div>
                  @if(auth('admin')->user()->role == 'Owner')
                  <div class="col-md-10 col-md-offset-1">
                    <br>  
                      <div class="box-tools pull-right">
                        <button class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editKunci" id="edit-kunci">Edit Kunci <i class="fa fa-key"></i></button>
                    </div>
                  </div>
                  @endif
                </div>
                <div class="row">
                    <div class='col-sm-4'>
                        <div class="col-md-10 col-md-offet-1">
                            @if(is_null($data->foto))
                            <img src="/imgUser/default.jpg" alt="" class="img-responsive">
                            @else
                            <img src="/imgUser/{{ $data->foto }}" alt="" class="img-responsive">
                            @endif
                        </div>
                    </div>
                    <div class='col-sm-8'>
                        <div class="form-group">
                            <strong>Nama</strong>
                            <br>{{ $data->nama }}
                        </div>
                        <div class="form-group">
                            <strong>Email</strong>
                            <br>{{ $data->email }}
                        </div>
                        <div class="form-group">
                            <strong>Alamat</strong>
                            <br>{{ $data->alamat }}
                        </div>
                        <div class="form-group">
                            <strong>Telepon</strong>
                            <br>{{ $data->no_telepon }}
                        </div>
                        <div class="form-group">
                            <strong>Tempat, tanggal lahir</strong>
                            <br>{{ $data->tempat_lahir }}, {{ $data->tanggal_lahir }}
                        </div>
                    </div>
                </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  @if(auth('admin')->user()->role == 'Owner')
  <div class="modal fade" id="editKunci" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      {!! Form::open(array('url' => '/administrator/edit-kunci', 'class' => 'form-horizontal', 'role' => 'form')) !!}
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit Kunci Pemilik</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Kunci Saat ini</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" disabled="" value="{{ $data->kunci_pemilik }}">
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Ubah Kunci</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="edit_kategori_nama" name='kunci'>
            </div>
          </div>
        </div>
        <div class="modal-footer">
        <input type='hidden' name='id' id='edit_kategori_id'>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
  @endif
@stop
@section('js')
<script src="{{ asset('/js/bootstrap-editable.min.js') }}"></script>
@stop