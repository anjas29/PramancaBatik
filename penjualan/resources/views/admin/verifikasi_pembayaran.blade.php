@extends('layouts.adminLayout')
@section('header')
  {{ Html::style('css/dataTables.bootstrap.css') }}
  {{ Html::style('/css/bootstrap-editable.css') }}
  <link href="//cdn.rawgit.com/noelboss/featherlight/1.5.0/release/featherlight.min.css" type="text/css" rel="stylesheet" />
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pesanan
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Detail Pembayaran</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-info'>
            <div class="box-header">
              <i class="fa fa-list"></i>
              <h3 class="box-title">Daftar Pembayaran Pesanan {{$pesanan->kode_pesanan}}</h3>
            </div>
            <div class="box-body">
              @if(count($data) > 0)
              <table class="table table-striped table-bordered" id='dataTable'>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tanggal Pembayaran</th>
                    <th>Keterangan</th>
                    <th>Pembayaran</th>
                    <th>Status Konfirmasi</th>
                    <th>Operasi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->tanggal_pembayaran }}</td>
                    <td>{{ $d->keterangan }}</td>
                    <td>Rp {{ number_format($d->pembayaran, 0, ',','.') }}</td>
                    <td>
                    @if($d->verifikasi == 1)
                          <strong style='color:#303F9F;'>Terkonfirmasi</strong>
                    @else
                          <strong style='color:#607D8B'>Belum Terkonfirmasi</strong>
                    @endif
                    </td>
                    <td>
                      <div class="btn-group">
                        @if($d->verifikasi != 1)
                          <a type='button' class='verifikasi btn btn-sm btn-success' data-id="{{$d->pembayaran_detail->pesanan->id}}" data-rowid="{{ $d->id }}"><i class="fa fa-check"></i></a>
                        @else
                          <a type='button' class='btn btn-sm'><i class="fa fa-check"></i></a>
                        @endif
                      </div>
                      <div class="btn-group">
                          <a href="#" data-featherlight="/imgPembayaran/{{$d->bukti_pembayaran}}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a>
                      </div>
                    </td>
                  </tr>
                  <?php $c++; ?>
                  @endforeach
                </tbody>
              </table>
              @else
                <div class="col-sm-12">
                  <h4>Tidak ada transaksi</h4>
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop
@section('js')
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('/js/bootstrap-editable.min.js') }}"></script>
<script src="//cdn.rawgit.com/noelboss/featherlight/1.5.0/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
  $('.verifikasi').click(function() {
    var rowid = $(this).data('rowid');
    var id = $(this).data('id');

    bootbox.confirm("Konfirmasi Pembayaran ini?", function(result) {
      if (result) {
        toastr.options.timeOut = 0;
        toastr.options.extendedTimeOut = 0;
        toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Sedang memproses...');
        toastr.options.timeOut = 5000;
        toastr.options.extendedTimeOut = 1000;
        $.post("/administrator/verifikasi-pembayaran", {rowid: rowid})
        .done(function(result) {
          window.location.replace("/administrator/verifikasi-pembayaran/"+id);
        })
        .fail(function(result) {
          toastr.clear();
          toastr.error('Kesalahan server! Silahkan reload halaman dan coba lagi');
        });
      };
    }); 
  });
</script>
@stop