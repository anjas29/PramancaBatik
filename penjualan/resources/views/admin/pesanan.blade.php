@extends('layouts.adminLayout')
@section('header')
  {{ Html::style('/css/dataTables.bootstrap.css') }}
  {{ Html::style('/css/bootstrap-editable.css') }}
  <style type="text/css">
    .badge-notify{
       background:red;
       position:absolute;
       top: -10px;
       left: 20px;
    }
  </style>
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pesanan
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pesanan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-info'>
            <div class="box-header">
              <i class="fa fa-list"></i>
              <h3 class="box-title">Daftar Pesanan</h3>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered table-condensed" id='dataTable'>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Kode Pesanan</th>
                    <th>Jenis Transaksi</th>
                    <th>Nama Pelanggan</th>
                    <th>Total Pembayaran</th>
                    <th>Status</th>
                    <th>Operasi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c++ }}</td>
                    <td>{{ $d->tanggal_pesanan }}</td>
                    <td>{{ $d->kode_pesanan }}</td>
                    <td>{{ $d->jenis_transaksi }}</td>
                    <td>{{ $d->nama_pelanggan }}</td>
                    <td>Rp {{ number_format($d->total_pembayaran, 0, ',','.') }}</td>
                    <td>
                    @if($d->verifikasi == 'Konfirmasi Pembayaran')
                          <strong style='color:#303F9F;'>Konfirmasi Pembayaran</strong>
                    @elseif($d->verifikasi == 'Dalam Pengiriman')
                          <strong style='color:#303F9F;'>Dalam Pengiriman</strong>
                    @elseif($d->verifikasi == 'Lunas Uang Muka')
                          <strong style='color:#303F9F;'>Lunas Uang Muka</strong>
                    @else
                          <strong style='color:#009688;'>Diterima</strong>
                    @endif
                    </td>
                    <td>
                      <div class="btn-group">
                        <a class="btn btn-xs btn-primary" href="/administrator/detailPesanan/{{ $d->id }}"><i class="fa fa-eye"></i></a>
                      </div>
                      <div class="btn-group">
                        <a class="btn btn-xs btn-success" href="/administrator/verifikasi-pembayaran/{{ $d->id }}"><i class="fa fa-edit"></i></a>
                        @if($d->pembayaran->notif > 0)
                          <span class="badge badge-xs badge-notify" style="z-index:99;">{{$d->pembayaran->notif}}</span>
                        @endif
                      </div>
                      <div class="btn-group">
                        <a class="delete btn btn-xs btn-danger" data-rowid="{{$d->id}}" data-kode="{{$d->kode_pesanan}}"><i class="fa fa-times"></i></a>
                      </div>
                   </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop
@section('js')
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('/js/bootstrap-editable.min.js') }}"></script>
<script>
  $(document).ready(function() {

    $('#dataTable').dataTable({
      "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('.edit').editable();
      }
    });
  });

  $('.delete').click(function() {
      var rowid = $(this).data('rowid');
      var kodePesanan = $(this).data('kode');

      bootbox.confirm("Hapus Pesanan <strong>"+kodePesanan+" </strong> ini?", function(result) {
        if (result) {
          toastr.options.timeOut = 0;
          toastr.options.extendedTimeOut = 0;
          toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Sedang menghapus...');
          toastr.options.timeOut = 5000;
          toastr.options.extendedTimeOut = 1000;
          $.post("/administrator/delete-pesanan", {rowid: rowid})
          .done(function(result) {
            window.location.replace("/administrator/pesanan/");
          })
          .fail(function(result) {
            toastr.clear();
            toastr.error('Kesalahan server! Silahkan reload halaman dan coba lagi');
          });
        };
      }); 
    });
</script>
@stop