@extends('layouts.adminLayout')
@section('header')
  {{ Html::style('css/dataTables.bootstrap.css') }}
  {{ Html::style('/css/bootstrap-editable.css') }}
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan Penjualan
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Laporan Penjualan</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-info'>
            <div class="box-header">
              <i class="fa fa-list"></i>
              <h3 class="box-title">{{$judul}}</h3>
              <form action="/administrator/laporan-download" method="POST">
                <input  type="hidden" value="{{$form['jenis']}}" name="jenis">
                <input  type="hidden" value="{{$form['penjualan']}}" name="penjualan">
                <input  type="hidden" value="{{$form['tahun']}}" name="tahun">
                <input  type="hidden" value="{{$form['bulan']}}" name="bulan">
                <button type='submit' class="btn btn-sm btn-info pull-right">Download Laporan</i></button>
              </form>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered" id='dataTable'>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Pesanan</th>
                    <th>Nama Pelanggan</th>
                    <th>Total Pembayaran</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->kode_pesanan }}</td>
                    <td>{{ $d->pelanggan->nama }}</td>
                    <td>{{ $d->total_pembayaran }}</td>
                    <td>
                    @if($d->verifikasi == 'Konfirmasi Pembayaran')
                          <a href="#" class="edit" data-type="select" data-pk="{{ $d->id }}" data-url="{{ URL::to('/administrator/ubahStatusPesanan') }}" data-source='{"Konfirmasi Pembayaran": "Konfirmasi Pembayaran", "Dalam Pengiriman": "Dalam Pengiriman", "Lunas Uang Muka" : "Lunas Uang Muka"}' data-title="Ubah Status">Konfirmasi Pembayaran</a>
                    @elseif($d->verifikasi == 'Dalam Pengiriman')
                          <a href="#" class="edit" data-type="select" data-pk="{{ $d->id }}" data-url="{{ URL::to('/administrator/ubahStatusPesanan') }}" data-source='{"Konfirmasi Pembayaran": "Konfirmasi Pembayaran", "Dalam Pengiriman": "Dalam Pengiriman", "Lunas Uang Muka" : "Lunas Uang Muka"}' data-title="Ubah Status">Dalam Pengiriman</a>
                    @elseif($d->verifikasi == 'Lunas Uang Muka')
                          <a href="#" class="edit" data-type="select" data-pk="{{ $d->id }}" data-url="{{ URL::to('/administrator/ubahStatusPesanan') }}" data-source='{"Konfirmasi Pembayaran": "Konfirmasi Pembayaran", "Dalam Pengiriman": "Dalam Pengiriman", "Lunas Uang Muka" : "Lunas Uang Muka"}' data-title="Ubah Status">Lunas Uang Muka</a>
                    @else
                          <a>Diterima</a>
                    @endif
                    </td>
                  </tr>
                  <?php $c++; ?>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop
@section('js')
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('/js/bootstrap-editable.min.js') }}"></script>
<script>
 
</script>
@stop