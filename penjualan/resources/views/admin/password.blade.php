@extends('layouts.adminLayout')
@section('header')
  {{ Html::style('css/dataTables.bootstrap.css') }}
  {{ Html::style('/css/bootstrap-editable.css') }}
@stop
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profil  {{$data->role}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/administrator/profil">Profil</a></li>
        <li class="active"> Setting</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">

          <!-- Profile Image -->
          <div class="box box-primary">
          <br>
                    <div class="row">
                        <div class='col-sm-4'>
                            <div class="col-md-10 col-md-offset-1">
                                @if(is_null($data->foto))
                                <img src="/imgUser/default.jpg" alt="" class="img-responsive">
                                @else
                                <img src="/imgUser/{{ $data->foto }}" alt="" class="img-responsive">
                                @endif
                                <hr>
                                <a href="/administrator/setting" class="btn btn-sm btn-success col-md-12">Ganti Data Diri</a>

                            </div>
                        </div>
                        <div class='col-sm-5 col-sm-offset-1'>
                              <p class="lead">Password</p>
                                  <form action="/administrator/setting-password" method="post" data-toggle='validator'>
                                        <div class="form-group">
                                            <label for="password_old">Password Lama</label>
                                            <input type="password" class="form-control" id="password_old" name="old_password" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password_1">Password Baru</label>
                                            <input type="password" class="form-control" id="password_1" name="new_password" data-minlength='8' required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password_2">Ulangi Password Baru</label>
                                            <input type="password" class="form-control" id="password_2" name="renew_password" data-match="#password_1" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                      
                                      <div class="col-sm-12 text-center">
                                          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                                          <p></p>
                                      </div>
                                      <!-- /.row -->
                                  </form>    
                        </div>
                    </div>
                <form>
          <!-- /.box -->
                
              </div>
          <br>
        </div>
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
@stop
@section('js')

<script src="{{ asset('/js/bootstrap-editable.min.js') }}"></script>

<script src="{{ asset('/js/validator.js') }}"></script>
@stop