@extends('layouts.adminLayout')
@section('header')
  {{ Html::style('/css/dataTables.bootstrap.css') }}
  {{ Html::style('css/bootstrap-editable.css') }}
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penjualan Offline
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Penjualan Offline</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-info'>
            <div class="box-header">
              <i class="fa fa-list"></i>
              <h3 class="box-title">Daftar Penjualan Offline</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#inputCs" id="input-cs">Input Penjualan <i class="fa fa-plus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable table-condensed">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Kode Penjualan</th>
                    <th>Jenis Transaksi</th>
                    <th>Nama Pelanggan</th>
                    <th>Total Pembayaran</th>
                    <th>Status</th>
                    <th>Operasi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->tanggal_pesanan}}</td>
                    <td>{{ $d->kode_pesanan }}</td>
                    <td>{{ $d->jenis_transaksi }}</td>
                    <td>{{ $d->nama_pelanggan }}</td>
                    <td>Rp {{ number_format($d->total_pembayaran, 0, ',','.') }}</td>
                    <td>
                    @if($d->verifikasi == 'Konfirmasi Pembayaran')
                      @if($d->jenis_transaksi == 'TOF')
                        <a href="#" class="edit" data-type="select" data-pk="{{ $d->id }}" data-url="{{ URL::to('/administrator/ubahStatusPesanan') }}" data-source='{"Dalam Pengiriman": "Lunas"}' data-title="Ubah Status"><strong style='color:#303F9F;'>Konfirmasi Pembayaran</strong></a>
                      @else
                        <a href="#" class="edit" data-type="select" data-pk="{{ $d->id }}" data-url="{{ URL::to('/administrator/ubahStatusPesanan') }}" data-source='{"Lunas Uang Muka" : "Uang Muka", "Dalam Pengiriman": "Lunas"}' data-title="Ubah Status"><strong style='color:#303F9F;'>Konfirmasi Pembayaran</strong></a>
                      @endif
                          
                    @elseif($d->verifikasi == 'Dalam Pengiriman')
                          <strong style='color:#303F9F;'>Dalam Pengiriman</strong>
                    @elseif($d->verifikasi == 'Lunas Uang Muka')
                          <a href="#" class="edit" data-type="select" data-pk="{{ $d->id }}" data-url="{{ URL::to('/administrator/ubahStatusPesanan') }}" data-source='{"Lunas Uang Muka" : "Uang Muka", "Dalam Pengiriman": "Lunas"}' data-title="Ubah Status"><strong style='color:#303F9F;'>Lunas Uang Muka</strong></a>
                    @else
                          <strong style='color:#009688;'>Diterima</strong>
                    @endif
                    </td>
                    <td>
                        <div class="btn-group">
                            <a class="btn btn-xs btn-primary" href="/administrator/detailPesanan/{{ $d->id }}"><i class="fa fa-eye"></i></a>
                        </div>
                        <div class="btn-group">
                        <a class="delete btn btn-xs btn-danger" data-rowid="{{$d->id}}" data-kode="{{$d->kode_pesanan}}"><i class="fa fa-times"></i></a>
                      </div>
                    </td>
                  </tr>
                  <?php $c++; ?>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!--Input Barang Modal-->
  <div class="modal fade" id="inputCs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <form action="/administrator/penjualan" method="post" data-toggle='validator' class="form-horizontal">
        <div class="modal-content" >
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Input Penjualan Offline</h4>
          </div>
          <div class="modal-body">
          <div class="form-group">
              <label for="" class="col-sm-3 control-label">No Pesanan</label>
              <div class="col-sm-9">
                <label class="control-label">{{$pesanan}}</label>
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-3 control-label">Nama Pelanggan</label>
              <div class="col-sm-9">
                <input type="text" name="nama" value="" class="form-control" required data-error="Data harus diisi">
                <div class="help-block with-errors"></div>
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-3 control-label">No Telepon Pelanggan</label>
              <div class="col-sm-9">
                <input type="text" name="telepon" value="" class="form-control" required data-error="Data harus diisi">
                <div class="help-block with-errors"></div>
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-3 control-label">Alamat</label>
              <div class="col-sm-9">
                <textarea name="alamat" value="" class="form-control" required data-error="Data harus diisi"></textarea>
                <div class="help-block with-errors"></div>
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-3 control-label">Jenis Pembayaran</label>
              <div class="col-sm-9">
                <select id='pelanggan' name="metode_pembayaran" class="form-control">
                  <option value="tunai">Tunai</option>
                  <option value="kredit">Kredit</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-3 control-label">Harga Beban</label>
              <div class="col-sm-9">
                <input type="text" name="harga_beban" class="form-control">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
@stop
@section('js')
  <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('/js/bootstrap-editable.min.js') }}"></script>
  <script src="{{ asset('/js/validator.js') }}"></script>
  <script>
    $(document).ready(function() {
      $('.dataTable').dataTable({
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          $('.edit').editable();
        }
      });
      $('.delete').click(function() {
        var rowid = $(this).data('rowid');
        var kodePesanan = $(this).data('kode');

        bootbox.confirm("Hapus Pesanan <strong>"+kodePesanan+" </strong> ini?", function(result) {
          if (result) {
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Sedang menghapus...');
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 1000;
            $.post("/administrator/delete-pesanan", {rowid: rowid})
            .done(function(result) {
              window.location.replace("/administrator/penjualan/");
            })
            .fail(function(result) {
              toastr.clear();
              toastr.error('Kesalahan server! Silahkan reload halaman dan coba lagi');
            });
          };
        }); 
      });
    });
  </script>
@stop