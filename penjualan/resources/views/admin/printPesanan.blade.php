<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Batik Pramanca</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('/admin/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/admin/dist/css/AdminLTE.min.css') }}">
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
    <section class="content">
      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <h3>Batik Pramanca <span class="logo-mini pull-right"><b><img src="/images/bp-logo.png"></b></span></h3>
          <div class='box box-info'>
            <div class="box-body">
                  <h3>Pesanan {{$data->kode_pesanan}}
                      @if($data->verifikasi == 'Diterima')
                          <span class="label label-success pull-right">Diterima</span>
                      @elseif($data->verifikasi == 'Konfirmasi Pembayaran')
                          <span class="label label-warning pull-right">Konfirmasi Pembayaran</span>
                      @elseif($data->verifikasi == 'Lunas Uang Muka')
                          <span class="label label-warning pull-right">Lunas Uang Muka</span>
                      @elseif($data->verifikasi == 'Dalam Pengiriman')
                          <div class="label label-info pull-right">Dalam Pengiriman</div>
                      @endif
                   </h3>
                   <hr class="primary">
                   <h4>Detail Pelanggan</h4>
                  <table class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <td>{{$data->nama_pelanggan}}</td>
                      </tr>
                      @if(!is_null($data->pelanggan))
                      <tr>
                        <th>Email</th>
                        <td>{{$data->pelanggan->email}}</td>
                      </tr>
                      @endif
                      <tr>
                        <th>No Telepon</th>
                        <td>{{$data->no_telepon}}</td>
                      </tr>
                      @if(!is_null($data->alamat))
                      <tr>
                        <th>Tujuan</th>
                        <td>{{$data->alamat}}</td>
                      </tr>
                      @endif
                    </thead>
                  </table>
                <h4>Detail Barang </h4>
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>
                    <th>Harga Satuan</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; ?>
                  @foreach($data->detail_pesanan as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->barang->nama }}</td>
                    <td>{{ $d->jumlah }}</td>
                    <td>Rp {{ number_format($d->harga, 0, ',','.') }}</td>
                    <?php $subtotal = $d->harga * $d->jumlah; ?>
                    <td>{{ $subtotal }}</td>
                  </tr>
                  <?php $c++; ?>
                  @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="4" class="text-right">Harga total</th>
                        <th>Rp {{ number_format($data->total_pembayaran, 0, ',','.') }}</th>
                    </tr>
                    <tr>
                        <th colspan="4" class="text-right">Beban</th>
                        <th>Rp {{ number_format($data->beban->harga_beban, 0, ',','.') }}</th>
                    </tr>
                    <tr>
                        <th colspan="4" class="text-right">Total</th>
                        <?php $total = $data->total_pembayaran + $data->beban->harga_beban; ?>
                        <th>Rp {{ number_format($total, 0, ',','.') }}</th>
                    </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    
  <script src="{{ asset('/admin/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
  
  <script src="{{ asset('/admin/dist/js/demo.js') }}"></script>
  <script type="text/javascript">
      $(document).ready(function() {
        window.print();
        setTimeout(function(){window.close();},1);
        window.onfocus=function(){ window.close();}
      });
  </script>
</body>
</html>
