@extends('layouts.adminLayout')
@section('header')
  {{ Html::style('css/dataTables.bootstrap.css') }}
  {{ Html::style('/css/bootstrap-editable.css') }}
@stop
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profil  {{$data->role}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/administrator/profil">Profil</a></li>
        <li class="active"> Setting</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">

          <!-- Profile Image -->
          <div class="box box-primary">
          <br>
                <form action="/administrator/setting" method="post" class="form-horizontal" data-toggle='validator' enctype="multipart/form-data" role='form'>
                    <div class="row">
                        <div class='col-sm-4'>
                            <div class="col-md-10 col-md-offset-1">
                                <label>Foto </label><br>
                                @if(is_null($data->foto))
                                <img src="/imgUser/default.jpg" alt="" class="img-responsive">
                                @else
                                <img src="/imgUser/{{ $data->foto }}" alt="" class="img-responsive">
                                @endif
                                <hr>
                                <input type="file" class="form-control" id="image" name="image">
                                <hr>
                                <a href="/administrator/password" class="btn btn-sm btn-primary col-md-12">Ganti Password</a>
                                <hr>
                            </div>
                        </div>
                        <div class='col-sm-6 col-sm-offset-1 col-xs-10 col-xs-offset-1'>
                            <div class="form-group">
                                <label>Nama </label>
                                <input type="text" class="form-control" id="nama" name="nama" value="{{$data->nama}}" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label>Email </label>
                                <input type="email" class="form-control" id="nama" name="email" value="{{$data->email}}" disabled>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label >Tempat Lahir</label>
                                <input type="text" class="form-control" name="tempat_lahir" value="{{$data->tempat_lahir}}" data-error='Data harus diisi' required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label >Tanggal Lahir</label>
                                <input type="date" class="form-control" name="tanggal_lahir" value="{{$data->tanggal_lahir}}" data-error='Data harus diisi' required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label>Alamat </label>
                                <textarea class="form-control" id="alamat" name="alamat"required>{{$data->alamat}}</textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label>No Telepon </label>
                                <input type="text" class="form-control" id="no_telepon" name="no_telepon" value="{{$data->no_telepon}}" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
                            <hr>
                        </div>
                    </div>
                </form>
                
              </div>
          <br>
        </div>
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
@stop
@section('js')
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('/js/bootstrap-editable.min.js') }}"></script>
<script src="{{ asset('/js/validator.js') }}"></script>
@stop