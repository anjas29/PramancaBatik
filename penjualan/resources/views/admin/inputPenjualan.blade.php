@extends('layouts.adminLayout')
@section('header')
  {{ Html::style('css/dataTables.bootstrap.css') }}
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Input Penjualan Offline
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Input Penjualan Offline</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-info'>
            <div class="box-header">
              <i class="fa fa-list"></i>
              <h3 class="box-title">Daftar Barang</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#inputBarang" id="input-cs">Input Barang <i class="fa fa-plus"></i></button>
              </div>
            </div>
            <div class="box-body">
            <form action="/administrator/update-penjualan-barang" method="POST">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Barang</th>
                    <th>Jumlah</th>
                    <th>Harga Satuan</th>
                    <th>Total Pembayaran</th>
                    <th>Operasi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->name }}</td>
                    <td>
                      <input type="number" name="qty[]" value="{{ $d->qty }}" class="form-control">
                    </td>
                    <td>{{ $d->price }}</td>
                    <td>{{ $d->subtotal }}</td>
                    <td>
                      <div class="btn-group"> 
                          <input type="hidden" value="{{$d->rowid}}" name="rowid[]">
                          <a type='button' class='delete-item' data-rowid="{{ $d->rowid }}" data-name="{{ $d->name }}"><i class="fa fa-trash-o"></i></a>
                      </div>
                   </td>
                  </tr>
                  <?php $c++; ?>
                  @endforeach
                </tbody>
              </table>
              <hr>
              <button type="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Perbarui jumlah barang</button>
              <a href="/administrator/final-penjualan" class="btn btn-primary btn-sm pull-right">Selesai</a>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!--Input Barang Modal-->
  <div class="modal fade" id="inputBarang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <form action="/administrator/add-penjualan-barang" method="post" class='form-horizontal' role='form' data-toggle='validator'>
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Input Barang</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Nama Barang</label>
            <div class="col-sm-9">
              <select id="barang" name="id_barang" class="form-control">
                <option>Pilih Barang</option>
                @foreach($barang as $d)
                  <option value="{{$d->id}}" data-harga='{{$d->harga}}'>{{$d->nama}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Harga Satuan</label>
            <div class="col-sm-4">
              <input disabled type="text" class="form-control" id="harga" name='harga'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Jumlah</label>
            <div class="col-sm-2">
              <input type="number" class="form-control" id="jumlah" name='jumlah' value="0" required>
              <div class="help-block with-errors"></div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </div>
        </form>
      </div>
    </div>
  </div>
@stop
@section('js')
  <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>

<script>
  $( "#barang" ).change(function() {
    var selected = $(this).find('option:selected');
    var extra = selected.data('harga');
    $('#harga').val(extra);
  });

  $('.delete-item').click(function() {
    var rowid = $(this).data('rowid');
    var barang = $(this).data('name');

    bootbox.confirm("Anda yakin akan menghapus barang <b>"+barang+"</b> ?", function(result) {
      if (result) {
        toastr.options.timeOut = 0;
        toastr.options.extendedTimeOut = 0;
        toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Sedang menghapus...');
        toastr.options.timeOut = 5000;
        toastr.options.extendedTimeOut = 1000;
        $.post("/administrator/delete-penjualan-barang", {rowid: rowid})
        .done(function(result) {
          window.location.replace("/administrator/input-penjualan");
        })
        .fail(function(result) {
          toastr.clear();
          toastr.error('Kesalahan server! Silahkan reload halaman dan coba lagi');
        });
      };
    }); 
  });
</script>
@stop