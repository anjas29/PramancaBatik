@extends('layouts.adminLayout')
@section('header')
  {{ Html::style('css/dataTables.bootstrap.css') }}
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pembayaran
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pemabayaran</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-info'>
            <div class="box-header">
              <i class="fa fa-list"></i>
              <h3 class="box-title">Daftar Pembayaran</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#inputPembayaran" id="tambah-kategori">Input Pembayaran <i class="fa fa-plus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Pesanan</th>
                    <th>Nama Pelanggan</th>
                    <th>Total Pembayaran</th>
                    <th>Uang Muka</th>
                    <th>Sisa Pembayaran</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->pesanan->kode_pesanan }}</td>
                    <td>{{ $d->pesanan->pelanggan->nama }}</td>
                    <td>{{ $d->pesanan->total_pembayaran }}</td>
                    <td>{{ $d->uang_muka }}</td>
                    <td>{{ $d->sisa_pembayaran }}</td>
                    <td>{{ $d->status_pembayaran }}</td>
                  </tr>
                  <?php $c++; ?>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Input Kategori Modal -->
  <div class="modal fade" id="inputPembayaran" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      {!! Form::open(array('url' => 'administrator/tambah-pembayaran', 'class' => 'form-horizontal', 'role' => 'form')) !!}
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Input Pembayaran</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Kode Pesanan</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name='kode_pesanan'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Tipe Pembayaran</label>
            <div class="col-sm-9">
              <select class="form-control" name="keterangan">
                <option value="Tunai">Tunai</option>
                <option value="Uang Muka">Uang Muka</option>
                <option value="Cicilan">Pelunasan Kredit</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Jumlah Pembayaran</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name='pembayaran'>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@stop
@section('js')
  <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
<script>
  $(document).ready(function() {
    $('.dataTable').dataTable();
  });
</script>
@stop