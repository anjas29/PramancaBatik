@extends('layouts.adminLayout')
@section('header')
  {{ Html::style('css/dataTables.bootstrap.css') }}
  {{ Html::style('css/bootstrap-editable.css') }}
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Detail Pesanan
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Detail Pesanan Pesanan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-info'>
            <div class="box-header">
              <i class="fa fa-list"></i>
              <h3 class="box-title">
                Daftar Pesanan
              </h3>
              <a href="/administrator/print-pesanan/{{$data->id}}" target="_blank" class="btn btn-sm btn-danger pull-right"><strong>Cetak</strong></a>
            </div>
            <div class="box-body">
                <div class="row">
                  <div class="col-sm-12">
                   <h3>Pesanan {{$data->kode_pesanan}}
                      @if($data->verifikasi == 'Diterima')
                          <span class="label label-success pull-right">Diterima</span>
                      @elseif($data->verifikasi == 'Konfirmasi Pembayaran')
                          <span class="label label-warning pull-right">Konfirmasi Pembayaran</span>
                      @elseif($data->verifikasi == 'Lunas Uang Muka')
                          <span class="label label-warning pull-right">Lunas Uang Muka</span>
                      @elseif($data->verifikasi == 'Dalam Pengiriman')
                          <div class="label label-info pull-right">Dalam Pengiriman</div>
                      @endif
                   </h3>
                   <h4>Detail Pelanggan</h4>
                  <table class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <td>{{$data->nama_pelanggan}}</td>
                      </tr>
                      @if(!is_null($data->pelanggan))
                      <tr>
                        <th>Email</th>
                        <td>{{$data->pelanggan->email}}</td>
                      </tr>
                      @endif
                      <tr>
                        <th>No Telepon</th>
                        <td>{{$data->no_telepon}}</td>
                      </tr>
                      @if(!is_null($data->alamat))
                      <tr>
                        <th>Tujuan</th>
                        <td>{{$data->alamat}}</td>
                      </tr>
                      @endif
                    </thead>
                  </table>
                      
                  </div>
                    
                </div>
                <hr>
                <h4>Detail Barang </h4>
              <table class="table table-striped table-bordered" id='dataTable'>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>
                    <th>Harga Satuan</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; ?>
                  @foreach($data->detail_pesanan as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->barang->nama }}</td>
                    <td>{{ $d->jumlah }}</td>
                    <td>Rp {{ number_format($d->harga, 0, ',','.') }}</td>
                    <?php $subtotal = $d->harga * $d->jumlah; ?>
                    <td>Rp {{ number_format($subtotal, 0, ',','.') }}</td>
                  </tr>
                  <?php $c++; ?>
                  @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="4" class="text-right">Harga total</th>
                        <th>Rp {{ number_format($data->total_pembayaran, 0, ',','.') }}</th>
                    </tr>
                    <tr>
                        <th colspan="4" class="text-right">Beban</th>
                        <th>Rp {{ number_format($data->beban->harga_beban, 0, ',','.') }}</th>
                    </tr>
                    <tr>
                        <th colspan="4" class="text-right">Total</th>
                        <?php $total = $data->total_pembayaran + $data->beban->harga_beban; ?>
                        <th>Rp {{ number_format($total, 0, ',','.') }}</th>
                    </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop
@section('js')
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('/js/bootstrap-editable.min.js') }}"></script>
@stop