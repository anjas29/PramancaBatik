@extends('layouts.adminLayout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active">Laporan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="box box-warning">
            <div class="box-header">
            <i class="fa fa-list"></i>
              <h3 class="box-title">Laporan</h3>
            </div>
            <div class="box-body">
              <div class="row">
              <form action="/administrator/laporan" method="POST">
                  <div class="form-group col-md-12">
                    <label for="nmprdk" class="col-sm-3 control-label">Jenis Laporan</label>
                    <div class="col-sm-9">
                      <select class="validate[required] form-control" name="jenis" id="jenis">
                        <option value="">-- Pilih Jenis Laporan --</option>
                        <option value="penjualan_semua">Penjualan Semua</option>
                        <option value="penjualan_tunai">Penjualan Tunai</option>
                        <option value="penjualan_kredit">Penjualan Kredit</option>
                        <option value="beban">Beban</option>
                        <option value="penerimaan">Penerimaan</option>
                        <option value="piutang">Piutang</option>
                        <option value="persediaan">Persediaan</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <label for="nmprdk" class="col-sm-3 control-label">Jenis Penjualan</label>
                    <div class="col-sm-9">
                      <select class="validate[required] form-control" name="penjualan" id="penjualan" disabled="false">
                        <option value="">-- Pilih Jenis Penjualan --</option>
                        <option value="semua">Semua</option>
                        <option value="offline">Offline</option>
                        <option value="online">Online</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <label for="nmprdk" class="col-sm-3 control-label">Tahun</label>
                    <div class="col-sm-9">
                      <input type="text" class="validate[required] form-control" name="tahun" id="tahun" disabled="false">
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <label for="nmprdk" class="col-sm-3 control-label">Bulan</label>
                    <div class="col-sm-9">
                      <select class="validate[required] form-control" name="bulan" id="bulan" disabled="false">
                        <option value="">-- Pilih Bulan --</option>
                        <option value="01">Januari</option>
                        <option value="02">Februari</option>
                        <option value="03">Maret</option>
                        <option value="04">April</option>
                        <option value="05">Mei</option>
                        <option value="06">Juni</option>
                        <option value="07">Juli</option>
                        <option value="08">Agustus</option>
                        <option value="09">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                  </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection
  @section('js')
    <script type="text/javascript">
      $('#jenis').on('change', function() {
          var jenis = this.value;

          switch(jenis){
            case 'penjualan_semua':
              $('#penjualan').removeAttr('disabled');
              $('#tahun').removeAttr('disabled');
              $('#bulan').removeAttr('disabled');
              break;
            case 'penjualan_kredit':
              $('#penjualan').removeAttr('disabled');
              $('#tahun').removeAttr('disabled');
              $('#bulan').removeAttr('disabled');
              break;
            case 'penjualan_tunai':
              $('#penjualan').removeAttr('disabled');
              $('#tahun').removeAttr('disabled');
              $('#bulan').removeAttr('disabled');
              break;
            case 'beban':
              $('#penjualan').attr('disabled', true);
              $('#tahun').removeAttr('disabled');
              $('#bulan').removeAttr('disabled');
              break;
            case 'penerimaan':
              $('#penjualan').removeAttr('disabled');
              $('#tahun').removeAttr('disabled');
              $('#bulan').removeAttr('disabled');
              break;
            case 'piutang':
              $('#penjualan').attr('disabled', true);
              $('#tahun').attr('disabled', true);
              $('#bulan').attr('disabled', true);
              break;
            case 'persediaan':
              $('#penjualan').attr('disabled', true);
              $('#tahun').removeAttr('disabled');
              $('#bulan').removeAttr('disabled');
              break;
          }
      });
    </script>
  @endsection