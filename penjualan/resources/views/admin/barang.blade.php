@extends('layouts.adminLayout')
@section('header')
  {{ Html::style('css/dataTables.bootstrap.css') }}
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Barang
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-list"></i>
              <h3 class="box-title">Daftar Kota</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#inputKategori" id="tambah-kategori">Input Kota <i class="fa fa-plus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Operasi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($kategori as $key => $ktg)
                  <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $ktg->nama }}</td>
                    <td>
                      <div class="btn-group">
                        <button type="button" class="btn btn-success btn-sm edit-kategori" data-toggle="modal" data-target="#editKategori" data-nama="{{ $ktg->nama }}" data-id="{{ $ktg->id }}" data-keterangan='{{ $ktg->keterangan }}' datatitle="Edit"><i class="fa fa-edit fa-fw"></i> Edit</button>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row-->

      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-info'>
            <div class="box-header">
              <i class="fa fa-list"></i>
              <h3 class="box-title">Daftar Barang</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#inputBarang">Input Barang <i class="fa fa-plus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Harga</th>  
                    <th>Harga Pokok</th>
                    <th>Stok</th>
                    <th>Kategori</th>
                    <th>Operasi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <th>{{ $d->kode_barang}}</th>
                    <td>{{ $d->nama }}</td>
                    <td>Rp {{ number_format($d->harga, 0, ',','.') }}</td>
                    <td>Rp {{ number_format($d->harga_grosir, 0, ',','.') }}</td>
                    <td>Rp {{ number_format($d->harga_pokok, 0, ',','.') }}</td>
                    <td>{{ $d->stok }}</td>
                    <td>{{ $d->kategori->nama }}</td>
                    <td>
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-primary detail-barang" data-id="{{ $d->id }}" data-nama="{{ $d->nama }}" data-harga="{{ $d->harga}}" data-harga-pokok='{{$d->harga_pokok}}' data-stok="{{ $d->stok}}" data-keterangan='{{$d->keterangan}}' data-toggle="modal" data-bahan='{{$d->bahan}}' data-harga-grosir='{{$d->harga_grosir}}' data-berat='{{$d->berat}}' data-filosofi='{{$d->filosofi}}' data-ukuran='{{$d->ukuran}}' data-target="#barangDetail" title="Detail"><i class="fa fa-pencil"></i></button>
                      </div>
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-success detail-stok" data-id="{{ $d->id }}"  data-stok="{{ $d->stok}}" data-toggle="modal" data-target="#tambahStok" title="Detail"><i class="fa fa-plus"></i></button>
                      </div>
                   </td>
                  </tr>
                  <?php $c++; ?>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!--Input Barang Modal-->
  <div class="modal fade" id="inputBarang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      {!! Form::open(array('url' => 'administrator/create-barang', 'class' => 'form-horizontal', 'role' => 'form', 'files'=>'true')) !!}
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Input Barang</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="input_nama" name='nama'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Caption</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="input_keterangan" name='keterangan'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Harga</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="input_harga" name='harga'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Harga Pokok</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="input_stok" name='harga_pokok'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Harga Grosir</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="input_stok" name='harga_grosir'>
            </div>
          </div>
          <div class="form-group">
          <label for="nmprdk" class="col-sm-2 control-label">Berat</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name='berat'>
            </div>
          </div>
          <div class="form-group">
          <label for="nmprdk" class="col-sm-2 control-label">Bahan</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="input_stok" name='bahan'>
            </div>
          </div>
          <div class="form-group">
          <label for="nmprdk" class="col-sm-2 control-label">Ukuran</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="input_stok" name='ukuran'>
            </div>
          </div>
          <div class="form-group">
          <label for="nmprdk" class="col-sm-2 control-label">Filosofi</label>
            <div class="col-sm-10">
              <textarea class="form-control" name="filosofi" id="input_stok"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Stok</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="input_stok" name='stok'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Kategori</label>
            <div class="col-sm-10">
              <select class="validate[required] form-control" name="kategori_id" id="kategori">
                <option value="">-- Pilih Kategori --</option>
                @foreach($kategori as $dk)
                  <option value="{{ $dk->id }}">{{ $dk->nama }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Foto Barang</label>
            <div class="col-sm-10">
              <input type="file" class="form-control" id="input_file" name='image'>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
  <!--Edit Barang Modal-->
 <div class="modal fade" id="detailBarang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      {!! Form::open(array('url' => 'administrator/edit-barang', 'class' => 'form-horizontal', 'role' => 'form', 'files'=>'true')) !!}
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Detail Barang</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="detail_nama" name='nama'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Caption</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="detail_keterangan" name='keterangan'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Harga</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="detail_harga" name='harga'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Harga Pokok</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="detail_harga_pokok" name='harga_pokok'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Harga Grosir</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="detail_harga_grosir" name='harga_grosir'>
            </div>
          </div>
          <div class="form-group">
          <label for="nmprdk" class="col-sm-2 control-label">Berat</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name='berat' id="detail_berat">
            </div>
          </div>
          <div class="form-group">
          <label for="nmprdk" class="col-sm-2 control-label">Bahan</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="detail_bahan" name='bahan'>
            </div>
          </div>
          <div class="form-group">
          <label for="nmprdk" class="col-sm-2 control-label">Ukuran</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="detail_ukuran" name='ukuran'>
            </div>
          </div>
          <div class="form-group">
          <label for="nmprdk" class="col-sm-2 control-label">Filosofi</label>
            <div class="col-sm-10">
              <textarea class="form-control" name="filosofi" id="detail_filosofi"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Foto Barang</label>
            <div class="col-sm-10">
              <input type="file" class="form-control" id="detail_file" name='image'>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type='hidden' name='id' id='detail_id'>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>

  <!-- Input Kategori Modal -->
  <div class="modal fade" id="inputKategori" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      {!! Form::open(array('url' => 'administrator/create-kategori', 'class' => 'form-horizontal', 'role' => 'form')) !!}
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Input Kategori</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="tambah_kategori_nama" name='nama'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Keterangan</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="tambah_kategori_keterangan" name='keterangan'>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>

  <!-- Edit Kategori Modal -->
  <div class="modal fade" id="editKategori" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      {!! Form::open(array('url' => 'administrator/edit-kategori', 'class' => 'form-horizontal', 'role' => 'form')) !!}
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit Kategori</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="edit_kategori_nama" name='nama'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Keterangan</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="edit_kategori_keterangan" name='keterangan'>
            </div>
          </div>
        </div>
        <div class="modal-footer">
        <input type='hidden' name='id' id='edit_kategori_id'>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
  <div class="modal fade" id="tambahStok" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      {!! Form::open(array('url' => 'administrator/tambah-stok', 'class' => 'form-horizontal', 'role' => 'form')) !!}
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Tambah Stok Barang</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Tambah Stok</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="stok_data" name='stok'>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type='hidden' name='id' id='stok_id'>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@stop
@section('js')
  <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
  <script>
    $('.detail-barang').click(function() {
      $('#detail_nama').val($(this).data('nama'));
      $('#detail_harga').val($(this).data('harga'));
      $('#detail_stok').val($(this).data('stok'));
      $('#detail_id').val($(this).data('id'));
      $('#detail_keterangan').val($(this).data('keterangan'));
      $('#detail_harga_pokok').val($(this).data('harga-pokok'));
      $('#detail_harga_grosir').val($(this).data('harga-grosir'));
      $('#detail_filosofi').val($(this).data('filosofi'));
      $('#detail_ukuran').val($(this).data('ukuran'));
      $('#detail_berat').val($(this).data('berat'));
      $('#detail_bahan').val($(this).data('bahan'));
      $('#detailBarang').modal('show');
    });
    $('.detail-stok').click(function() {
      $('#stok_data').val($(this).data('stok'));
      $('#stok_id').val($(this).data('id'));
    });
    $('#tambah-kategori').click(function() {
      $('#tambah_kategori_nama').val("");
      $('#tambah_kategori_keterangan').val("");
    });
    $('.edit-kategori').click(function() {
      $('#edit_kategori_nama').val($(this).data('nama'));
      $('#edit_kategori_keterangan').val($(this).data('keterangan'));
      $('#edit_kategori_id').val($(this).data('id'));
    });
  </script>
  <script>
    $(document).ready(function() {
      $('.dataTable').dataTable();
    });
  </script>
@stop