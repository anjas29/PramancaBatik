@extends('layouts.adminLayout')
@section('header')
  {{ Html::style('css/dataTables.bootstrap.css') }}
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Laporan Beban</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-info'>
            <div class="box-header">
              <i class="fa fa-list"></i>
              <h3 class="box-title">{{$judul}}</h3>
              <form action="/administrator/laporan-download" method="POST">
                <input  type="hidden" value="{{$form['jenis']}}" name="jenis">
                <input  type="hidden" value="{{$form['penjualan']}}" name="penjualan">
                <input  type="hidden" value="{{$form['tahun']}}" name="tahun">
                <input  type="hidden" value="{{$form['bulan']}}" name="bulan">
                <button type='submit' class="btn btn-sm btn-info pull-right">Download Laporan</i></button>
              </form>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Kode Pesanan</th>
                    <th>Jumlah Barang</th>
                    <th>Beban</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->Tanggal}}</td>
                    <td>{{ $d->Kode_Pesanan}}</td>
                    <td>{{ $d->Jumlah_Barang}}</td>
                    <td>{{ $d->Beban}}</td>
                  </tr>
                  <?php $c++; ?>
                  @endforeach
                </tbody>
                <tfoot>
                  <td colspan='4'><strong>Total</strong></td>
                  <td ><strong>{{ $total->Total }}</strong></td>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop
@section('js')


  <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
  <script>
    $('.detail-barang').click(function() {
      $('#detail_nama').val($(this).data('nama'));
      $('#detail_harga').val($(this).data('harga'));
      $('#detail_stok').val($(this).data('stok'));
      $('#detail_id').val($(this).data('id'));
      $('#detail_keterangan').val($(this).data('keterangan'));
      $('#detailBarang').modal('show');
    });
    $('.detail-stok').click(function() {
      $('#stok_data').val($(this).data('stok'));
      $('#stok_id').val($(this).data('id'));
    });
    $('#tambah-kategori').click(function() {
      $('#tambah_kategori_nama').val("");
    });
    $('.edit-kategori').click(function() {
      $('#edit_kategori_nama').val($(this).data('nama'));
      $('#edit_kategori_id').val($(this).data('id'));
    });
  </script>
<script>
</script>
@stop