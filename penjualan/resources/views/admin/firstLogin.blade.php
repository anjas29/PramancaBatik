<!DOCTYPE html>
<html>
    <head>
        <title>Batik Pramanca</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ asset('/admin/bootstrap/css/bootstrap.min.css') }}">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #FFFFF;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Aktifasi Akun <br><strong>Owner Batik Pramanca</strong></div>
                <a href="/firstLogin" class="btn btn-sm btn-primary"><strong>Aktifasi Akun</strong></a>
            </div>
        </div>
    </body>
</html>
