@extends('layouts.header')
@section('style')
	
@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="/index">Home</a>
                </li>
                <li>Log in</li>
            </ul>

        </div>

        <div class="col-md-6 col-md-offset-3">
            <div class="box">
                <h3>Login Administrator</h3>
                <form action="/loginAdmin" method="post" data-toggle='validator'>
                    @if(Session::has('loginFailed'))
                    <div class="alert alert-danger alert-dismissable" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Login Gagal!</strong> Email atau password salah.
                    </div>
                    @endif
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" required data-error='Email tidak valid'>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in"></i> Log in</button>
                    </div>
                </form>
            </div>
        </div>


    </div>
    <!-- /.container -->
</div>
<!-- /#content -->
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
<script src="{{ asset('/js/validator.js') }}"></script>
@stop