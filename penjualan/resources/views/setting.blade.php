@extends('layouts.header')
@section('style')

@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="/index">Beranda</a>
                </li>
                <li>
                    <a href="/profil">Profil</a>
                </li>
                <li>Setting</li>
            </ul>

        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU ***
_________________________________________________________ -->
            <div class="panel panel-default sidebar-menu">

                <div class="panel-heading">
                    <h3 class="panel-title">Customer section</h3>
                </div>

                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked">
                        <li>
                            <a href="/order"><i class="fa fa-list"></i> Pesanan</a>
                        </li>
                        <li>
                            <a href="/tagihan"><i class="fa fa-heart"></i> Tagihan</a>
                        </li>
                        <li class="active">
                            <a href="/profil"><i class="fa fa-user"></i> Profil</a>
                        </li>
                        <li>
                            <a href="/logout"><i class="fa fa-sign-out"></i> Logout</a>
                        </li>
                    </ul>
                </div>

            </div>
            <!-- /.col-md-3 -->

            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-9">
            <div class="box">
                <h1>Pengaturan Profil</h1>
                <p class="lead">Data diri</p>

                {!! Form::open(array('url' => '/settings', 'class' => 'form-horizontal', 'role' => 'form', 'files' => true)) !!}
                    <div class="row">
                        <div class='col-sm-4'>
                            <div>
                                <label>Foto </label><br>
                                @if(is_null($data->foto))
                                <img src="/imgUser/default.jpg" alt="" class="img-responsive">
                                @else
                                <img src="/imgUser/{{ $data->foto }}" alt="" class="img-responsive">
                                @endif
                                <hr>
                                <input type="file" class="form-control" id="image" name="file">
                            </div>
                        </div>
                        <div class='col-sm-6 col-sm-offset-1'>
                            <div class="form-group">
                                <label>Nama </label>
                                <input type="text" class="form-control" id="nama" name="nama" value="{{$data->nama}}" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label>Email </label>
                                <input type="email" class="form-control" id="nama" name="email" value="{{$data->email}}" disabled>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label >Tempat Lahir</label>
                                <input type="text" class="form-control" name="tempat_lahir" value="{{$data->tempat_lahir}}" data-error='Data harus diisi' required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label >Tanggal Lahir</label>
                                <input type="date" class="form-control" name="tanggal_lahir" value="{{$data->tanggal_lahir}}" data-error='Data harus diisi' required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label>Kota </label>
                                <select name="kota" class="form-control" required>
                                    @foreach($kota as $d)
                                        @if($data->kota_id == $d->city_id)
                                            <option value="{{$d->city_id}}-{{$d->city_name}}" selected='true'>{{$d->city_name}}</option>
                                        @else
                                            <option value="{{$d->city_id}}-{{$d->city_name}}">{{$d->city_name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label >Kode Pos</label>
                                <input type="text" class="form-control" name="kode_pos" value="{{$data->kode_pos}}" data-error='Data harus diisi' required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label>Alamat </label>
                                <input type="text" class="form-control" id="alamat" name="alamat" value="{{$data->alamat}}" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label>No Telepon </label>
                                <input type="text" class="form-control" id="no_telepon" name="no_telepon" value="{{$data->no_telepon}}" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </div>
                {!! Form::close() !!}
                <form action="/settingPassword" method="post" data-toggle='validator'>
                <hr>
                    <div class="row">
                        <div class="col-sm-6">
                        <p class="lead"> Password</p>
                            <div class="form-group">
                                <label for="password_old">Password Lama</label>
                                <input type="password" class="form-control" id="password_old" name="old_password" required data-error="Harus diisi">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="password_1">Password Baru</label>
                                <input type="password" class="form-control" id="password_1" name="new_password" data-minlength='8' required data-error="Minimal 8 huruf">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="password_2">Ulangi Password Baru</label>
                                <input type="password" class="form-control" id="password_2" name="renew_password" data-match="#password_1" required data-error="Password tidak sama">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan password</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
<!-- /#content -->
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
<script src="{{ asset('/js/validator.js') }}"></script>
@stop