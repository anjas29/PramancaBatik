@extends('layouts.header')
@section('style')
	<meta name="_token" content="{{ csrf_token() }}">
@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="/index">Beranda</a>
                </li>
                <li>Pembayaran - Ringkasan</li>
            </ul>
        </div>

        <div class="col-md-9" id="checkout">

            <div class="box">
                <form method="post" action="/payment/step3">
                    <h1>Pembayaran</h1>
                    <ul class="nav nav-pills nav-justified">
                        <li><a href="/payment/step1"><i class="fa fa-map-marker"></i><br>Alamat</a>
                        </li>
                        <li><a href="/payment/step2"><i class="fa fa-truck"></i><br>Cara Pembayaran</a>
                        </li>
                        <li class="active"><a href="#"><i class="fa fa-money"></i><br>Ringkasan</a>
                        </li>
                    </ul>

                    <div class="content">                        
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Data Pelanggan</h4>
                                <strong>Nama</strong>
                                <p class="lead">{{Session::get('temp_nama')}}</p>
                                <strong>Kota</strong>
                                <p class="lead">{{Session::get('temp_kota')}}</p>
                                <strong>Kode Pos</strong>
                                <p class="lead">{{Session::get('temp_kode_pos')}}</p>
                                <strong>Alamat</strong>
                                <p class="lead">{{Session::get('temp_alamat')}}</p>
                            </div>
                        </div>
                        <hr>
                        <h4>Data Pembelian</h4>
                        <div class="table-responsive">
                            <table class="table">
                            @if ($data['count'] != 0)
                                <thead>
                                    <tr>
                                        <th colspan="2">Barang</th>
                                        <th>Jumlah</th>
                                        <th>Harga Satuan</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                    <tbody>
                                    @foreach($data['details'] as $d)
                                        <tr>
                                            <td>
                                                <a href="/detail/{{ $d->id }}">
                                                    <img src="/imgProduct/{{ $d->options->foto }}" alt="Foto Barang">
                                                </a>
                                            </td>
                                            <td>
                                                <a href="/detail/{{ $d->id }}">{{ $d->name }}</a>
                                            </td>
                                            <td>{{ $d->qty }}</td>
                                            <td>Rp {{ number_format($d->price, 0, ',','.') }}</td>
                                            <td>Rp {{ number_format($d->subtotal, 0, ',','.') }}</td>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="4"><br>Sub Total</th>
                                            <th><br>Rp {{ number_format($data['total'], 0, ',','.') }}</th>
                                        </tr>
                                        <tr>
                                            <th colspan="4">Biaya Ekspedisi</th>
                                            <th> Rp {{ number_format($data['beban'], 0, ',','.') }}</th>
                                        </tr>
                                        <tr>
                                            <th colspan="4">Biaya Unik</th>
                                            <th> Rp {{ number_format($data['kode_unik'], 0, ',','.') }} </th>
                                        </tr>
                                        <tr>
                                            <th colspan="4">Total</th>
                                            <?php $total = $data['total'] + $data['beban'] + $data['kode_unik']; ?>
                                            <th> Rp {{ number_format($total, 0, ',','.') }} </th>
                                        </tr>
                                    </tfoot>
                                @else
                                    <thead>
                                        <tr>
                                            <th> Kosong </th>
                                        </tr>
                                    </thead>
                                @endif
                            </table>
                        </div>
                    </div>

                    <div class="box-footer">
                        <div class="pull-left">
                            <a href="/payment/step2" class="btn btn-default"><i class="fa fa-chevron-left"></i>Cara Pembayaran</a>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary">Selesai<i class="fa fa-chevron-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box -->


        </div>
        <!-- /.col-md-9 -->

        <div class="col-md-3">

            <div class="box" id="order-summary">
                <div class="box-header">
                    <h3>Ringkasan</h3>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>Total Belanja</td>
                                <?php $total = Cart::total(); ?>
                                <th>{{ $total }}</th>
                            </tr>
                            <tr>
                                <td>Biaya Ekspedisi</td>
                                <th>{{ Session::get('temp_beban') }}</th>
                            </tr>
                            <tr>
                                <td>Biaya Kode Unik</td>
                                <th>{{ Session::get('temp_kode_unik') }}</th>
                            </tr>
                            <tr class="total">
                                <td>Total</td>
                                <th>{{ $total + Session::get('temp_beban') + Session::get('temp_kode_unik')}}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>

        </div>
        <!-- /.col-md-3 -->

    </div>
</div>
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
@stop