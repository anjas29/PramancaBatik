@extends('layouts.header')
@section('style')
<style type="text/css">
.thumb {
  position: relative;
  width: 100%;
  height: 300px;
  overflow: hidden;
}
.thumb img {
  position: absolute;
  left: 50%;
  top: 50%;
  height: 100%;
  width: auto;
  -webkit-transform: translate(-50%,-50%);
      -ms-transform: translate(-50%,-50%);
          transform: translate(-50%,-50%);
}
.thumb img.portrait {
  width: 100%;
  height: auto;
}

.thumbSmall {
  position: relative;
  width: 100%;
  height: 200px;
  overflow: hidden;
}
.thumbSmall img {
  position: absolute;
  left: 50%;
  top: 50%;
  height: 100%;
  width: auto;
  -webkit-transform: translate(-50%,-50%);
      -ms-transform: translate(-50%,-50%);
          transform: translate(-50%,-50%);
}
.thumbSmall img.portrait {
  width: 100%;
  height: auto;
}
</style>
@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12">
            <ul class="breadcrumb">
                <li>
                    <a href="/index">Beranda</a>
                </li>
                <li>{{ $select }}</li>
            </ul>
        </div>

        <div class="col-md-3">
            <!-- *** MENUS AND FILTERS ***
_________________________________________________________ -->
            <div class="panel panel-default sidebar-menu">

                <div class="panel-heading">
                    <h3 class="panel-title">Kategori</h3>
                </div>

                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked category-menu">
                        @if ($select == 'Semua')
                            <li class="active">
                                <a href="/category">Semua  </a>
                            </li>
                        @else
                            <li>
                                <a href="/category">Semua  </a>
                            </li>
                        @endif
                        <hr>
                        @foreach($category as $c)
                            @if (strcasecmp($c->nama,$select) == 0)
                                <li class="active">
                                    <a href="/category/{{ $c->nama }}">{{ $c->nama }}  <span class="badge pull-right">{{ $c->barang->count() }}</span></a>
                                </li>
                            @else
                                <li>
                                    <a href="/category/{{ $c->nama }}">{{ $c->nama }}  <span class="badge pull-right">{{ $c->barang->count() }}</span></a>
                                </li>
                            @endif
                        @endforeach
                    </ul>

                </div>
            </div>
            <!-- *** MENUS AND FILTERS END *** -->
        </div>

        <div class="col-md-9">
            <div class="box info-bar">
                <div class="row">
                    <div class="col-sm-12 col-md-4 products-showing">
                        Menampilkan <strong>{{ $data->count() }}</strong> dari <strong>{{ $count }}</strong> produk
                    </div>

                    <div class="col-sm-12 col-md-8  products-number-sort">
                        <div class="row">
                            <form class="form-inline">
                                <div class="col-md-6 col-sm-6">
                                    <div class="products-number">
                                        <strong>Tampilkan</strong>  
                                        @if(Input::get('limit') == 12 || is_null(Input::get('limit')))
                                        <a href="?limit=12" class="btn btn-default btn-sm btn-primary">12</a>  
                                        @else
                                        <a href="?limit=12" class="btn btn-default btn-sm ">12</a>  
                                        @endif
                                        
                                        @if(Input::get('limit') == 24)
                                        <a href="?limit=24" class="btn btn-default btn-sm btn-primary">24</a>  
                                        @else
                                        <a href="?limit=24" class="btn btn-default btn-sm ">24</a>  
                                        @endif

                                        @if(Input::get('limit') == 48)
                                        <a href="?limit=48" class="btn btn-default btn-sm btn-primary">48</a>  produk
                                        @else
                                        <a href="?limit=48" class="btn btn-default btn-sm ">48</a>  produk
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="products-sort-by">
                                        <strong>Filter</strong>
                                        <select name="sort-by" class="form-control" id="sort-by">
                                            @if(Input::get('sort') == 'nama' || is_null(Input::get('sort')))
                                            <option selected value="nama">Nama</option>
                                            @else
                                            <option value="nama">Nama</option>
                                            @endif

                                            @if(Input::get('sort') == 'harga')
                                            <option selected value="nama">Harga</option>
                                            @else
                                            <option value="harga">Harga</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row products">
                @foreach($data as $d)
                <div class="col-md-4 col-sm-6">
                    <div class="product">
                        <div class="thumb">
                            <a href="/detail/{{ $d->id }}">
                                <img src="/imgProduct/{{ $d->foto }}" alt="" class="img-responsive">
                            </a>
                        </div>
                        <div class="text">
                            <h3>
                                <a href="detail.html">{{ $d->nama }}</a>
                                <p class="price">Rp {{ number_format($d->harga, 0, ',','.') }}</p>
                            </h3>
                            
                            <p class="buttons">
                                <a href="/detail/{{ $d->id }}" class="btn btn-default">Lihat Detail</a>
                            </p>
                        </div>
                        <!-- /.text -->
                    </div>
                    <!-- /.product -->
                </div>
                @endforeach
                <!-- /.col-md-4 -->
            </div>
            <!-- /.products -->

            <div class="pages">
                {{ $data->links() }}
            </div>


        </div>
        <!-- /.col-md-9 -->
    </div>
    <!-- /.container -->
</div>
@endsection
@section('js')
<script type="text/javascript">
  $('#sort-by').on('change', function(){
    var url = '/category?sort=' + $(this).val();
    
    window.location.replace (url);
  });
</script>
@endsection