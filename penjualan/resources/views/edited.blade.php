@extends('layouts.header')
@section('style')
<style type="text/css">
.thumbe {
  position: relative;
  width: 100%;
  height: 300px;
  overflow: hidden;
}
.thumbe img {
  position: absolute;
  left: 50%;
  top: 50%;
  height: 100%;
  width: auto;
  -webkit-transform: translate(-50%,-50%);
      -ms-transform: translate(-50%,-50%);
          transform: translate(-50%,-50%);
}
.thumbe img.portrait {
  width: 100%;
  height: auto;
}

.thumb1 {
  position: relative;
  width: 100%;
  height: 563px;
  overflow: hidden;
}
.thumb1 img {
  position: absolute;
  left: 50%;
  top: 50%;
  height: 100%;
  width: auto;
  -webkit-transform: translate(-50%,-50%);
      -ms-transform: translate(-50%,-50%);
          transform: translate(-50%,-50%);
}
.thumb1 img.portrait {
  width: 100%;
  height: auto;
}

.thumbSmall {
  position: relative;
  width: 100%;
  height: 200px;
  overflow: hidden;
}
.thumbSmall img {
  position: absolute;
  left: 50%;
  top: 50%;
  height: 100%;
  width: auto;
  -webkit-transform: translate(-50%,-50%);
      -ms-transform: translate(-50%,-50%);
          transform: translate(-50%,-50%);
}
.thumbSmall img.portrait {
  width: 100%;
  height: auto;
}

</style>
@stop
@section('content')
<div id="content">
<div class="container">
    <div class="col-md-12">
        <div id="main-slider">
            <div class="item thumb1">
                <img src="/img/slider1.jpg" alt="" class="img-responsive potrait">
            </div>
            <div class="item thumb1">
                <img src="/img/slider2.jpg" alt="" class="img-responsive potrait">
            </div>
            <div class="item thumb1">
                <img src="/img/slider3.jpg" alt="" class="img-responsive potrait">
            </div>
            <div class="item thumb1">
                <img src="/img/slider4.jpg" alt="" class="img-responsive potrait">
            </div>
            <div class="item thumb1">
                <img src="/img/slider5.jpg" alt="" class="img-responsive potrait">
            </div>
        </div>
        <!-- /#main-slider -->
    </div>
</div>

<!-- *** Category HOMEPAGE ***
_________________________________________________________ -->
<div id="advantages">

    <div id='hot' class="box">
        <div class="container">
            <div class="col-md-12">
                <h2>Kategori</h2>
            </div>
        </div>
    </div>

    <div class="container" data-animate='fadeInUp'>
        <div class="same-height-row">
        @foreach($category as $c)
            <div class="col-sm-4">
                <div class="box same-height clickable">
                    <div class="icon"><i class="fa fa-heart"></i>
                    </div>

                    <h3><a href="/category/{{ $c->nama }}">{{ $c->nama }}</a></h3>
                    @if(is_null($c->keterangan))
                        <p>Keterangan</p>
                    @else
                        <p>{{ $c->keterangan}}</p>
                    @endif
                </div>
            </div>
        @endforeach
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>
<!-- /#cateogory -->

<!-- *** ADVANTAGES END *** -->

<!-- *** HOT PRODUCT SLIDESHOW ***
_________________________________________________________ -->
<div id="hot" data-animate="fadeInUp">
    <div class="box">
        <div class="container">
            <div class="col-md-12">
                <h2>Hot this week</h2>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="product-slider">
            @foreach($latest as $l)
            <div class="item">
                <div class="product">
                   <div class='thumbSmall'>
                    <a href="/detail/{{ $l->id }}">
                        <img src="/imgProduct/{{ $l->foto }}" alt="" class="potrait">
                    </a>
                </div>
                    <div class="text">
                        <h3><a href="/detail/{{ $l->id }}">{{ $l->nama }}</a>
                        <p class="price">Rp {{ number_format($l->harga, 0, ',','.') }}</p></h3>
                        
                    </div>
                    <!-- /.text -->
                </div>
                <!-- /.product -->
            </div>
            @endforeach
        </div>
        <!-- /.product-slider -->
    </div>
    <!-- /.container -->
</div>
<!-- /#hot -->

<!-- *** HOT END *** -->

<!--all Barang here -->
<div class="box slideshow">
    <h3>Batik Pramanca</h3>
    <p class="lead">All what we have</p>
</div>
<div class="container">
    <div class="row products">
        @foreach($data as $d)
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="product">
                <div class='thumbe'>
                    <a href="/detail/{{ $d->id }}">
                        <img src="/imgProduct/{{ $d->foto }}" alt="" class="potrait">
                    </a>
                </div>
                <div class="text">
                    <h3>
                        <a href="/detail/{{ $d->id }}"> {{ $d->nama }}</a>
                        <p class="price">Rp {{ number_format($d->harga, 0, ',','.') }}</p>
                    </h3>
                    
                    <p class="buttons">
                        <a href="/detail/{{ $d->id }}" class="btn btn-default">Detail</a>
                    </p>
                </div>
                <!-- /.text -->
                <a href="/category/{{ $d->kategori->nama }}">
                    <div class="ribbon sale">
                        <div class="theribbon">{{ $d->kategori->nama}}</div>
                        <div class="ribbon-background"></div>
                    </div>
                </a>
            </div>
            <!-- /.product -->
        </div>
        @endforeach
        <!-- /.col-md-4 -->
        <div class='pages'>
            <p class="loadMore">
                <a href="/category" class="btn btn-primary btn-lg">Selengkapnya</a>
            </p>    
        </div>
    </div>
</div>
@stop