@extends('layouts.header')
@section('style')
<style type="text/css">
.thumbe {
  position: relative;
  width: 100%;
  height: 100px;
  overflow: hidden;
}
.thumbe img {
  position: absolute;
  left: 50%;
  top: 50%;
  height: 100%;
  width: auto;
  -webkit-transform: translate(-50%,-50%);
      -ms-transform: translate(-50%,-50%);
          transform: translate(-50%,-50%);
}
.thumbe img.portrait {
  width: 100%;
  height: auto;
}
</style>
@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="/">Beranda</a>
                </li>
                <li>Checkout - Cara Pembayaran</li>
            </ul>
        </div>

        <div class="col-md-10 col-md-offset-1" id="checkout">

            <div class="box">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Berhasil</h2>
                        <p class="muted">
                            <strong>Terimakasih</strong>, Anda telah berhasil melakukan checkout pemesanan dengan memilih pembayaran transfer manual.
                        </p>
                        <div class="row">
                            <div class='btn btn-danger col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3'>
                                <strong>Rp  <?php echo number_format($total, 0, ',', '.') ?>,-</strong>
                            </div>
                        </div>
                        <hr>
                        <p class="muted">
                            1. Transfer sejumlah Rp {{$total}} ke salah satu nomor rekening di bawah ini:
                            <br>
                            <div class="row">
                                <div class="col-md-5 col-md-offset-1">
                                    <div class="thumbe">
                                        <img src="/img/mandiri.png" class="img-responsive potrait">
                                    </div>
                                    <p>
                                        <br>
                                        <strong>Mandiri</strong>
                                        <br><b>13700 100 21638</b>
                                        <br><strong>A. N. Hastha Dewa Putranta</strong>
                                    </p>
                                </div>
                                <div class="col-md-4 col-md-offset-1">
                                    
                                </div>
                            </div>
                            2. Setelah melakukan transfer, segera konfirmasi Pembayaran. Pembayaran tanpa melakukan konfirmasi tidak dapat kami proses lebih lanjut. 
                            <br>3. Unggah bukti pembayaran/transfer pada halaman <a href="/order"><strong style="color: #009688;">Pesanan</strong></a>. Pastikan data yang diunggah benar dan valid.
                            <br>4. Pesanan akan otomatis <strong>dibatalkan</strong> dalam waktu 1x24jam jika Anda tidak melakukan pembayaran dan konfirmasi pembayaran.
                            <br>5. Cek Kode Pesanan menggunakan kode berikut : <strong style="color: #009688;">{{$kode}}</strong>
                        </p>
                        <a href="/order" class='btn btn-primary col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-6 col-xs-offset-3'>
                                Lihat Pesanan Selengkapnya
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.box -->


        </div>
        <!-- /.col-md-9 -->
    </div>
</div>
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
@stop