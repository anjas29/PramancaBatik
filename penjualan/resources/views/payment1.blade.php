@extends('layouts.header')
@section('style')

@stop
@section('content')
<?php $total = Cart::total(); ?>
<div id="content">
    <div class="container">

        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="/">Beranda</a>
                </li>
                <li>Pembayaran - Alamat</li>
            </ul>
        </div>

        <div class="col-md-9" id="checkout">

            <div class="box">
                <form action="/payment/step1" method="post" id='formAlamat' data-toggle='validator'>
                    <h1>Pembayaran</h1>
                    <ul class="nav nav-pills nav-justified">
                        <li class="active"><a href="#"><i class="fa fa-map-marker"></i><br>Alamat</a>
                        </li>
                        <li class="disabled"><a href="#"><i class="fa fa-truck"></i><br>Cara Pembayaran</a>
                        </li>
                        <li class="disabled"><a href="#"><i class="fa fa-money"></i><br>Ringkasan</a>
                        </li>
                    </ul>

                    <div class="content">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <h4>
                                    Data Alamat Pelanggan
                                    <button id='editButton' type="button" class="btn btn-warning btn-sm pull-right">Gunakan Alamat Lain?</button>
                                </h4>
                                
                                    <div class="form-group col-md-6">
                                        <label><strong>Kota</strong></label>
                                        <select class="form-control" id="nama" name="kota" data-error='Data harus diisi' required disabled="true">
                                            @foreach($data['kota'] as $d)
                                                @if($d->city_id == $data['kota_id'])
                                                    <option value="{{$d->city_id}}-{{$d->city_name}}" selected>{{$d->city_name}}</option>
                                                @else
                                                    <option value="{{$d->city_id}}-{{$d->city_name}}">{{$d->city_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label><strong>Kode Pos</strong></label>
                                        <input type="text" class="form-control" id="kode_pos" name="kode_pos" data-error='Data harus diisi' required disabled="true" value="{{$data['kode_pos']}}">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label><strong>Alamat</strong></label>
                                        <textarea class="form-control" id="alamat" name="alamat" data-error='Data harus diisi' required disabled="true">{{$data['alamat']}}</textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <div class="pull-left">
                            <a href="/cart" class="btn btn-default"><i class="fa fa-chevron-left"></i>Keranjang Belanja</a>
                        </div>
                        <div class="pull-right">
                            <button type="submit" id='submitAlamat' class="btn btn-primary">Cara Pembayaran<i class="fa fa-chevron-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-md-9 -->

        <div class="col-md-3">

            <div class="box" id="order-summary">
                <div class="box-header">
                    <h3>Ringkasan</h3>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>Total Belanja</td>
                                <th>{{ Cart::total() }}</th>
                            </tr>
                            <tr>
                                <td>Biaya Ekspedisi</td>
                                <th>0</th>
                            </tr>
                            <tr>
                                <td>Biaya Unik</td>
                                <th>0</th>
                            </tr>
                            <tr class="total">
                                <td>Total</td>
                                <th>{{ $total }}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>

        </div>
        <!-- /.col-md-3 -->

    </div>
</div>
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
<script type="text/javascript">
    var edit = false;
    $(document).ready(function() {
        $("#editButton").click(function(){
            if(edit){
                $("#nama").attr("disabled", true);
                $("#kode_pos").attr("disabled", true);
                $("#alamat").attr("disabled", true);
                $("#editButton").html('Ubah Alamat');
                $("#editButton").addClass("btn-warning");
                $("#editButton").removeClass("btn-primary");
                edit = false;
            }else{
                $("#nama").removeAttr("disabled");    
                $("#kode_pos").removeAttr("disabled");    
                $("#alamat").removeAttr("disabled");    
                $("#editButton").html('Simpan');
                $("#editButton").removeClass("btn-warning");
                $("#editButton").addClass("btn-primary");
                edit = true;
            }
        }); 

        $('#submitAlamat').click(function(){
            $("#nama").removeAttr("disabled");
            $("#kode_pos").removeAttr("disabled");
            $("#alamat").removeAttr("disabled");
        });
    });
</script>
<script src="{{ asset('/js/validator.js') }}"></script>
@stop