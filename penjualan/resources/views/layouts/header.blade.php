<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Batik Pramanca | Pramanca Batik">
    <link rel="stylesheet" href="{{ asset('/css/toastr.min.css') }}">
    <link rel='shortcut icon' href='/img/bp-logo.png' type='image/x-icon'/ >

    <title>
        Batik Pramanca
    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="{{asset('/template/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('/template/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('/template/css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('/template/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('/template/css/owl.theme.css')}}" rel="stylesheet">
    

    <!-- <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet"> -->

    <!-- theme stylesheet -->
    <link href="{{ asset('/template/css/style.default.css') }}" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="{{ asset('/template/css/custom.css') }}" rel="stylesheet">

    <script src="{{ asset('/template/js/respond.min.js') }}"></script>

    <link rel="shortcut icon" href="favicon.png">
    @yield('style')
</head>

<body>

    <!-- *** TOPBAR ***
 _________________________________________________________ -->
    <div id="top">
        <div class="container">
            <div class="col-md-6 offer" data-animate="fadeInDown">
                
            </div>
            <div class="col-md-6" data-animate="fadeInDown">
                <ul class="menu">
                @if(auth('pelanggan')->check())
                    <li><a href="/profil">{{ auth('pelanggan')->user()->nama }}</a>
                    </li>
                    <li><a href="/logout">Logout</a>
                    </li>
                @else
                    <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a>
                    </li>
                    <li><a href="/register">Register</a>
                    </li>
                @endif
                </ul>
            </div>
        </div>
        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
            <div class="modal-dialog modal-sm">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="Login">Login</h4>
                    </div>
                    <div class="modal-body">
                        <form action="/login" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" id="email-modal" placeholder="Email" name="email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password-modal" placeholder="Password" name="password">
                            </div>

                            <p class="text-center">
                                <button class="btn btn-primary"><i class="fa fa-sign-in"></i> Log in</button>
                            </p>

                        </form>

                        <p class="text-center text-muted">Belum punya akun? <a href="/register">daftar sekarang</a></p>
                        <p class="text-center text-muted">Login sebagai Administrator? <a href="/loginAdmin">Login Administrator</a></p>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- *** TOP BAR END *** -->

    <!-- *** NAVBAR ***
 _________________________________________________________ -->

    <div class="navbar navbar-default yamm" role="navigation" id="navbar">
        <div class="container">
            <div class="navbar-header">

                <a class="navbar-brand home" href="index.html" data-animate-hover="bounce">
                    <img src="/img/logo.png" alt="Obaju logo" class="hidden-xs">
                    <img src="/img/logo.png" alt="Obaju logo" class="visible-xs"><span class="sr-only">Batik Pramanca</span>
                </a>
                <div class="navbar-buttons">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-align-justify"></i>
                    </button>
                    <a class="btn btn-default navbar-toggle" href='/cart'>
                        <i class="fa fa-shopping-cart"></i>  <span class="hidden-xs">{{ Cart::count() }}</span>
                    </a>
                </div>
            </div>
            <!--/.navbar-header -->

            <div class="navbar-collapse collapse" id="navigation">

                <ul class="nav navbar-nav navbar-left">
                    <li><a href="/">Home</a></li>
                    <li><a href="/profil">Profil</a></li>
                    <li><a href="/category">Belanja</a></li>
                    <li><a href="/checkPesanan">Cek Pesanan</a></li>
                </ul>

            </div>
            <!--/.nav-collapse -->

            <div class="navbar-buttons">

                <div class="navbar-collapse collapse right" id="basket-overview">
                    <a href="/cart" class="btn btn-primary navbar-btn"><i class="fa fa-shopping-cart"></i><span class="hidden-sm"> {{ Cart::count() }} barang</span></a>
                </div>
                <!--/.nav-collapse -->

                <!-- <div class="navbar-collapse collapse right" id="search-not-mobile">
                    <button type="button" class="btn navbar-btn btn-primary" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
                    </button>
                </div> -->

            </div>

            <div class="collapse clearfix" id="search">

                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">
			                 <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>
            </div>
            <!--/.nav-collapse -->

        </div>
        <!-- /.container -->
    </div>
    <!-- /#navbar -->

    <!-- *** NAVBAR END *** -->

    <div id="all">

    @yield('content')
        <!-- *** FOOTER *** _________________________________________________________ -->
        <div id="footer" data-animate="fadeInUp">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-1">

                        <h4><a href="#">Lokasi</a></h4>

                        <p><strong>Batik Pramanca</strong>
                            <br>Gamping Lor,
                            <br>No. 14, RT. 02, RW. 14,
                            <br>Ambar Ketawang, Gamping
                            <br>
                            <strong>Daerah Istimewa Yogyakarta</strong>
                        </p>

                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- /.col-md-3 -->
                    <div class="col-md-4 col-md-offset-1">

                        <h4><a href="#">Media</a></h4>

                        <p class="social">
                            <a href="https://www.facebook.com/public/Pramanca-Batik" target="_blank" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.instagram.com/pramanca_batik/" target="_blank" class="instagram external" data-animate-hover="shake"><i class="fa fa-instagram"></i></a>
                        </p>


                    </div>
                    <!-- /.col-md-3 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->




        <!-- *** COPYRIGHT ***
 _________________________________________________________ -->
        <div id="copyright">
            <div class="container">
                <div class="col-md-6">
                    <p class="pull-left">© 2016 Pramanca Batik.</p>
                </div>
                <div class="col-md-6">
                    <p class="pull-right">Template by <a href="http://www.bootstrapious.com">Responsive Templates</a> with support from <a href="http://kakusei.cz">Designové předměty</a> 
                        <!-- Not removing these links is part of the licence conditions of the template. Thanks for understanding :) -->
                    </p>
                </div>
            </div>
        </div>
        <!-- *** COPYRIGHT END *** -->
    </div>
    <!-- /#all -->
    <!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
    <script src="{{ asset('/template/js/jquery-1.11.0.min.js') }}"></script>
    <script src="{{ asset('/template/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/template/js/jquery.cookie.js') }}"></script>
    <script src="{{ asset('/template/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('/template/js/modernizr.js') }}"></script>
    <script src="{{ asset('/template/js/bootstrap-hover-dropdown.js') }}"></script>
    <script src="{{ asset('/template/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('/template/js/front.js') }}"></script>
    <script src="{{ asset('/js/toastr.min.js') }}"></script>
    <script src="{{ asset('/js/validator.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function() {
    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-bottom-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "3000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }

      @if(Session::has('message'))
        toastr.success("{{ Session::get('message') }}");
      @elseif(Session::has('messageError'))
        toastr.error("{{ Session::get('messageError') }}");
      @endif
    });
  </script>

    <!-- <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/bootstrap-hover-dropdown.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/front.js"></script> -->

    @yield('js')
</body>

</html>