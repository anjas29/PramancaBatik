@extends('layouts.header')
@section('style')
	
@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="/index">Beranda</a>
                </li>
                <li>Keranjang Belanja</li>
            </ul>
        </div>

        <div class="col-md-9" id="basket">

            <div class="box">

                <form method="post" action="/cart/update">

                    <h1>Keranjang Belanja</h1>
                    <div class="table-responsive">
                        <table class="table">
                        @if ($data['count'] != 0)
                            <thead>
                                <tr>
                                    <th colspan="2">Barang</th>
                                    <th>Jumlah</th>
                                    <th></th>
                                    <th>Harga Satuan</th>
                                    <th colspan="2">Total</th>
                                </tr>
                            </thead>
	                            <tbody>
	                            @foreach($data['details'] as $d)
	                                <tr>
	                                    <td>
	                                        <a href="/detail/{{$d->id}}">
	                                            <img src="/imgProduct/{{ $d->options->foto }}" alt="White Blouse Armani">
	                                        </a>
	                                    </td>
	                                    <td>
                                            <a href="/detail/{{$d->id}}" class=""><strong> {{ $d->name }}</strong></a>
	                                    </td>
	                                    <td>
	                                        <input type="number" name='qty[]' value="{{ $d->qty }}" class="form-control" colspan='2'>
	                                    </td>
	                                    <td>Rp {{ number_format($d->price, 0, ',','.') }}</td>
	                                    <td>Rp {{ number_format($d->subtotal, 0, ',','.') }}</td>
	                                    <td>
	                                    	<input type="hidden" name='rowid[]' value="{{ $d->rowid }}">
	                                    	<a type='button' class='delete-item' data-rowid="{{ $d->rowid }}" data-name="{{ $d->name }}"><i class="fa fa-trash-o"></i></a>
	                                    </td>
	                                </tr>
	                            @endforeach
	                            </tbody>
	                            <tfoot>
	                                <tr>
	                                    <th colspan="4">Total</th>
										<th colspan="2"> Rp {{ number_format($data['total'], 0, ',','.') }}</th>
	                                </tr>
	                            </tfoot>
	                        @else
	                       		<thead>
	                                <tr>
	                                    <th> <h3>Kosong</h3> </th>
	                                </tr>
	                            </thead>
	                        @endif
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                    <div class="box-footer">
                        <div class="pull-left">
                            <a href="/category" class="btn btn-default"><i class="fa fa-chevron-left"></i> Kembali Belanja</a>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Perbarui jumlah barang</button>
                            <a href="/payment/step1" class="btn btn-primary">Lanjutkan Pembayaran <i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>

                </form>

            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-md-9 -->

        <div class="col-md-3">
            <div class="box" id="order-summary">
                <div class="box-header">
                    <h3>Ringkasan</h3>
                </div>

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>Total Belanja</td>
                                <th>{{{ $data['total'] }}}</th>
                            </tr>
                            <tr>
                                <td>Biaya Ekspedisi</td>
                                <th>0</th>
                            </tr>
                            <tr>
                                <td>Biaya Unik</td>
                                <th>0</th>
                            </tr>
                            <tr class="total">
                                <td>Total</td>
                                <th>{{{ $data['total'] }}}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!-- /.col-md-3 -->

    </div>
    <!-- /.container -->
</div>
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
<script type="text/javascript" charset="utf-8">

$('.delete-item').click(function() {
	var rowid = $(this).data('rowid');
	var barang = $(this).data('name');

	bootbox.confirm("Anda yakin akan menghapus barang <b>"+barang+"</b> ?", function(result) {
		if (result) {
			toastr.options.timeOut = 0;
			toastr.options.extendedTimeOut = 0;
			toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Sedang menghapus...');
			toastr.options.timeOut = 5000;
			toastr.options.extendedTimeOut = 1000;
			$.post("/cart/delete", {rowid: rowid})
			.done(function(result) {
				window.location.replace("/cart");
			})
			.fail(function(result) {
				toastr.clear();
				toastr.error('Kesalahan server! Silahkan reload halaman dan coba lagi');
			});
		};
	}); 
});
</script>
@stop