@extends('layouts.header')
@section('style')
	<meta name="_token" content="{{ csrf_token() }}">
@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="/index">Home</a>
                </li>
                <li>Log in</li>
            </ul>

        </div>

        <div class="col-md-6 col-md-offset-3">
            <div class="box">
                <h3>Data Tamu</h3>

                <form action="/guest" method="post">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" id="email" name="nama">
                    </div>
                    <div class="form-group">
                        <label>No Telepon</label>
                        <input type="text" class="form-control" id="no_telepon" name="no_telepon">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-user-md"></i> Lanjutkan</button>
                    </div>
                </form>
                <br>
                <p class="lead" align="center">Buat akun sekarang? <a href="/register">Daftar disini</a></p>
            </div>
        </div>


    </div>
    <!-- /.container -->
</div>
<!-- /#content -->
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
@stop