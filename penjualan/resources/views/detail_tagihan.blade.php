@extends('layouts.header')
@section('style')

@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="/index">Beranda</a>
                </li>
                <li><a href="#">Detail Tagihan</a>
                </li>
                <li>{{ $data->pesanan->kode_pesanan }}</li>
            </ul>

        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU ***
_________________________________________________________ -->
            <div class="panel panel-default sidebar-menu">

                <div class="panel-heading">
                    <h3 class="panel-title">Customer </h3>
                </div>

                <div class="panel-body">

                    <ul class="nav nav-pills nav-stacked">
                        <li >
                            <a href="/order"><i class="fa fa-list"></i> Pesanan</a>
                        </li class="active">
                        <li>
                            <a href="/tagihan"><i class="fa fa-heart"></i> Tagihan</a>
                        </li>
                        <li>
                            <a href="/profil"><i class="fa fa-user"></i> Profil</a>
                        </li>
                        <li>
                            <a href="/logout"><i class="fa fa-sign-out"></i> Logout</a>
                        </li>
                    </ul>
                </div>

            </div>
            <!-- /.col-md-3 -->
        </div>

        <div class="col-md-9" id="customer-order">
            <div class="box">
                <div class="row ">
                    <div class="col-md-8 col-md-offset-2">
                    <h3>Tagihan</h3>
                        <hr>
                        <p><strong>Kepada</strong><br> Nama <strong>{{$data->pesanan->pelanggan->nama}}</strong>
                            <br>Email <strong>{{$data->pesanan->pelanggan->email}}</strong>
                            </p>
                        <p>Kepada Pelanggan diharap segera menyelesaikan transaksi pesanan dengan kode pesanan <a href="/order/{{$data->pesanan->id}}"><strong>{{$data->pesanan->kode_pesanan}}</strong></a>
                        sebesar <strong>Rp {{ number_format($data->sisa_pembayaran, 0, ',','.') }}</strong></p>
                        <p class="pull-right"><strong>Terimakasih</strong><br><br><br><br><strong>Batik Pramanca</strong></p>
                        
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
@stop