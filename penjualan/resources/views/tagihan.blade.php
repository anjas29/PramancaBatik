@extends('layouts.header')
@section('style')

@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href='/index'>Beranda</a>
                </li>
                <li>Tagihan</li>
            </ul>

        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU ***
_________________________________________________________ -->
            <div class="panel panel-default sidebar-menu">

                <div class="panel-heading">
                    <h3 class="panel-title">Customer</h3>
                </div>

                <div class="panel-body">

                    <ul class="nav nav-pills nav-stacked">
                        <li >
                            <a href="/order"><i class="fa fa-list"></i> Pesanan</a>
                        </li>
                        <li class="active">
                            <a href="/tagihan"><i class="fa fa-heart"></i> Tagihan</a>
                        </li>
                        <li>
                            <a href="/profil"><i class="fa fa-user"></i> Profil</a>
                        </li>
                        <li>
                            <a href="/logout"><i class="fa fa-sign-out"></i> Logout</a>
                        </li>
                    </ul>
                </div>

            </div>
            <!-- /.col-md-3 -->

            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-9" id="customer-orders">
            <div class="box">
                <h1>Tagihan</h1>
                <hr>
                @if(count($data) != 0)
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Kode Pesanan</th>
                                <th>Tanggal</th>
                                <th>Total</th>
                                <th>Sisa Pembayaran</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $d)
                            <tr>
                                <th>{{ $d->pesanan->kode_pesanan }}</th>
                                <td>{{ $d->pesanan->tanggal_pesanan }}</td>
                                <td>Rp {{ number_format($d->pesanan->total_pembayaran, 0, ',','.') }}</td>
                                <td>Rp {{ number_format($d->sisa_pembayaran, 0, ',','.') }}</td>
                                <td><span class="label label-info">Pelunasan Kredit</span></td>
                                <td>
                                    <a href="/tagihan/{{ $d->id }}" class="btn btn-primary btn-sm">Lihat</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
                 @else
                    <h4>Tidak ada tagihan</h4>
                @endif
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
@stop