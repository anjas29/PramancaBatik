<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCustomerService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('customer_service', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alamat');
            $table->string('no_telepon');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
        Schema::table('log_aktivitas', function (Blueprint $table){
            $table->foreign('customer_service_id')
                    ->references('id')
                    ->on('customer_service')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::table('log_aktivitas', function (Blueprint $table){
            $table->dropForeign('log_aktivitas_customer_service_id_foreign');
        });

        Schema::table('customer_service', function (Blueprint $table){
            $table->dropForeign('customer_service_user_id_foreign');
        });

        Schema::drop('customer_service');
    }
}
