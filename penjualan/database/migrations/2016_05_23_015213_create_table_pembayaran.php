<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayaran', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_pembayaran');
            $table->integer('pesanan_id')->unsigned();
            $table->string('metode_pembayaran');
            $table->integer('uang_muka');
            $table->integer('cicilan_perbulan');
            $table->integer('sisa_pembayaran');
            $table->timestamp('tanggal_jatuh_tempo');
            $table->string('status_pembayaran');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pembayaran');
    }
}
