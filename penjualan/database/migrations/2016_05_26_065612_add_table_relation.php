<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('beban', function (Blueprint $table) {
            $table->foreign('pesanan_id')->references('id')
                    ->on('pesanan')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
        Schema::table('pesanan', function (Blueprint $table) {
            $table->foreign('pelanggan_id')->references('id')
                    ->on('pelanggan')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
        Schema::table('detail_pesanan', function (Blueprint $table) {
            $table->foreign('pesanan_id')->references('id')
                    ->on('pesanan')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
        Schema::table('pembayaran', function (Blueprint $table) {
            $table->foreign('pesanan_id')->references('id')
                    ->on('pesanan')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
        Schema::table('detail_pembayaran', function (Blueprint $table) {
            $table->foreign('pembayaran_id')->references('id')
                    ->on('pembayaran')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
        Schema::table('pelanggan', function (Blueprint $table){
            $table->foreign('user_id')->references('id')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }  

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('beban', function (Blueprint $table){
            $table->dropForeign('beban_pesanan_id_foreign');
        });

        Schema::table('pesanan', function (Blueprint $table){
            $table->dropForeign('pesanan_pelanggan_id_foreign');
        });

        Schema::table('detail_pesanan', function (Blueprint $table){
            $table->dropForeign('detail_pesanan_pesanan_id_foreign');
        });

        Schema::table('pembayaran', function (Blueprint $table){
            $table->dropForeign('pembayaran_pesanan_id_foreign');
        });

        Schema::table('detail_pembayaran', function (Blueprint $table){
            $table->dropForeign('detail_pembayaran_pembayaran_id_foreign');
        });

        Schema::table('pelanggan', function (Blueprint $table){
            $table->dropForeign('pelanggan_user_id_foreign');
        });
    }
}
