<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPesanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pesanan', function($table){
            $table->boolean('verifikasi');
        });

        Schema::table('detail_pembayaran', function($table){
            $table->boolean('verifikasi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pesanan', function ($table) {
            $table->dropColumn(['verifikasi']);
        });

        Schema::table('detail_pembayaran', function ($table) {
            $table->dropColumn(['verifikasi']);
        });
    }
}
