<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetailBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::create('detail_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barang_id')->unsigned();
            $table->string('size');
            $table->integer('stok');
            $table->timestamps();
            $table->foreign('barang_id')
                    ->references('id')
                    ->on('barang')
                    ->onupdate('cascade')
                    ->ondelete('cascade');
        });*/
        Schema::table('detail_pesanan', function (Blueprint $table){
            $table->foreign('barang_id')->references('id')
                    ->on('barang')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::table('detail_barang', function(Blueprint $table){
            $table->dropForeign('detail_barang_barang_id_foreign');
        });*/

        Schema::table('detail_pesanan', function(Blueprint $table){
            $table->dropForeign('detail_pesanan_barang_id_foreign');
        });
        // Schema::drop('detail_barang');
    }
}
