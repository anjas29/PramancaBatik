<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKategori extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategori', function (Blueprint $table){
            $table->increments('id');
            $table->string('nama');
            $table->string('keterangan');
        });

        Schema::table('barang', function (Blueprint $table){
            $table->foreign('kategori_id')->references('id')->on('kategori')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang', function (Blueprint $table){
            $table->dropForeign('barang_kategori_id_foreign');
        });

        Schema::drop('kategori');
    }
}
