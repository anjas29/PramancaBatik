<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('barang')->delete();

        $data = [
        	[
        		'nama'=>'Barang 1',
        		'kategori_id'=>1,
        		'harga'=>45000,
        		'stok'=>10,
                'foto'=>'product.jpg',
        		'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
        		'updated_at'=>Carbon::now()->format('Y-m-d H:i:s')
        	],
        	[
        		'nama'=>'Barang 2',
        		'kategori_id'=>1,
        		'harga'=>5000,
        		'stok'=>10,
                'foto'=>'product.jpg',
        		'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
        		'updated_at'=>Carbon::now()->format('Y-m-d H:i:s')
        	],
        	[
        		'nama'=>'Barang 3',
        		'kategori_id'=>2,
        		'harga'=>45000,
        		'stok'=>10,
                'foto'=>'product.jpg',
        		'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
        		'updated_at'=>Carbon::now()->format('Y-m-d H:i:s')
        	],
        	[
        		'nama'=>'Barang 4',
        		'kategori_id'=>2,
        		'harga'=>25000,
        		'stok'=>10,
                'foto'=>'product.jpg',
        		'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
        		'updated_at'=>Carbon::now()->format('Y-m-d H:i:s')
        	],
        	[
        		'nama'=>'Barang 5',
        		'kategori_id'=>3,
        		'harga'=>19000,
        		'stok'=>10,
                'foto'=>'product.jpg',
        		'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
        		'updated_at'=>Carbon::now()->format('Y-m-d H:i:s')
        	],
        	[
        		'nama'=>'Barang 6',
        		'kategori_id'=>3,
        		'harga'=>20000,
        		'stock'=>10,
                'foto'=>'product.jpg',
        		'created_at'=>Carbon::now()->format('Y-m-d H:i:s'),
        		'updated_at'=>Carbon::now()->format('Y-m-d H:i:s')
        	],

        ];

        DB::table('barang')->insert($data);
    }
}
