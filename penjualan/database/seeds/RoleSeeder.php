<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role')->delete();

        $data = [
        	[
        		'name'=>'Admin'
        	],
        	[
        		'name'=>'Pelanggan'
        	],
        ];

        DB::table('role')->insert($data);
    }
}
