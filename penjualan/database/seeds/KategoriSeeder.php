<?php

use Illuminate\Database\Seeder;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kategori')->delete();

        $data = [
        	[
        		'nama'=>'Pria',
        	],
        	[
        		'nama'=>'Wanita',
        	],
            [
        		'nama'=>'Anak-anak'
        	]
        ];

        DB::table('kategori')->insert($data);
    }
}
